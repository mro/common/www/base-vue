FROM node:lts-slim

WORKDIR /app
ADD . /app
RUN apt-get update && \
    apt-get install git python3 build-essential --yes && \
    npm install && \
    npm install pm2 && mkdir .pm2 && chmod 777 .pm2 && \
    apt-get autoremove -y git python3 build-essential && \
    rm -rf /var/lib/apt/lists/* && apt-get clean
ENV PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:node_modules/.bin
ENV HOME=/app

CMD pm2-docker -n app --no-autorestart ./example/server/index.js
EXPOSE 8080/tcp
