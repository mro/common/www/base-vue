# EN-SMM group Vue.js base widgets

Base widgets for [Vue.js](https://vuejs.org/) based applications.

In addition to [Vue.js](https://vuejs.org/) components, this project provides
[fonts](https://fontawesome.com/icons?d=gallery) and
[animations](https://greensock.com).

This library is developped according to our [guidelines](https://mro-dev.web.cern.ch/docs/drafts/current/en-smm-apc-web-guidelines.html).

# Documentation

A [live demo](https://mro-dev.web.cern.ch/base-vue) of this library with
additional comments and boilerplates is available.

# Build

To build the example application (assuming that Node.js is installed on your machine):
```bash
# Run webpack and bundle things in /dist
npm run build

# Serve pages on port 8080
npm run serve
```

# Usage

To use this library as a dependency:
```bash
# Install it
npm install "git+https://gitlab.cern.ch/mro/common/www/base-vue.git"
```

Then components can be imported (using ES6 imports):
```js
import {
  BaseParamInput as Param,
  BaseCollapsible as Collapsible } from '@cern/base-vue';
```

## WebPack

Some WebPack configuration is required to properly compile
[Vue.js](https://vuejs.org/) components, [SASS](https://sass-lang.com) and
embed icon-fonts.

The [WebPack configuration file](/webpack.config.js) found in this project can
be used as a reference.

# Deploy

To deploy the example application, assuming that oc is configured and connected
on the proper project:
```bash
oc apply -f deploy/openshift-dc.yaml
```
