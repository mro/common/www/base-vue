
import "./public-path";

import Vue from "vue";
import { default as BaseVue, createStore } from "../src";
import App from "./App";
import VueRouter from "vue-router";

Vue.use(BaseVue);

/**
 * @typedef {import('vue-router').Route} Route
 * @typedef {{ x: number, y : number }} Position
 */

/**
 * @param  {Route} to
 * @param  {Route} from
 * @param  {Position} [savedPosition]
 */
function scrollBehavior(to, from, savedPosition) {
  return savedPosition;
}
const router = new VueRouter({ scrollBehavior });

function getNotFixed(elt) {
  while (elt) {
    const style = window.getComputedStyle(elt);
    if (style && style.position !== "fixed") {
      return elt;
    }
    elt = elt.parentElement;
  }
  return elt;
}

let scrollTimer;
router.beforeEach(function(to, from, next) {
  if (scrollTimer) {
    clearTimeout(scrollTimer);
  }
  if (to.query && to.query.anchor) {
    const anchor = to.query.anchor;
    scrollTimer = setTimeout(() => {
      scrollTimer = null;

      const elt = getNotFixed(document.getElementById(anchor));
      if (elt) { elt.scrollIntoView({ behavior: "smooth" }); }
    }, 300);
  }
  next();
});

export default new Vue({
  el: "#app",
  store: createStore(router),
  router,
  components: { App },
  template: "<App/>"
});
