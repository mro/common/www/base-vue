
const
  path = require("path"),
  express = require("express"),
  pug = require("pug"),
  bodyParser = require("body-parser"),
  sse = require("./express-sse"),
  serveStatic = require("express-static-gzip");

var config;
try {
  /* $FlowIgnore */
  config = require("/etc/app/config"); /* eslint-disable-line global-require */
}
catch (e) {
  config = { basePath: "" };
}

var app = express();
var router = express.Router();

app.use(bodyParser.text({ type: "text/plain" }));

app.set("view engine", "pug");
app.set("views", __dirname);
app.set("etag", true);

const dist = path.join(__dirname, "..", "..", "dist");
router.use("/dist", serveStatic(dist, { enableBrotli: true }));

router.get("/", (req, res) => res.render("index", config));

router.post("/pugRender", (req, res) => {
  res.setHeader("content-type", "text/plain");
  const ret = pug.render(req.body, { pretty: (req.query.pretty === "1") });

  res.send(ret);
});

router.get("/error_url", ({ next }) => {
  next({ status: 500, message: "Something went really wrong !" });
});

router.get("/sse", sse, (req, res) => {
  if (res.sse) {
    var counter = 0;
    const interval = setInterval(() => res.sse((++counter).toString()), 1000);
    res.on("close", () => clearInterval(interval));
  }
  else {
    res.send();
  }
});

app.use(config.basePath, router);

// error handler
app.use(function(
  /** @type {any} */ err,
  /** @type {Request} */ req,
  /** @type {Response} */res,
  /** @type {NextFunction} */ next) { /* eslint-disable-line */ /* jshint ignore:line */
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};
  res.locals.status = err.status || 500;
  res.status(err.status || 500);
  res.render("error", { baseUrl: config.basePath });
});

app.listen(process.env.PORT || 8080,
  () => console.log("Server listening on http://localhost:" + (process.env.PORT || 8080)));
