// @ts-check
/* eslint-disable max-lines, prefer-rest-params */

import { forEach, get, has, isFunction, last, noop } from "lodash";
import { select } from "d3-selection";
import { easeBackIn, easeBackOut, easeBounceOut, easeCubicOut } from "d3-ease";
import { interpolateNumber, interpolateString, interpolateTransformCss } from "d3-interpolate";
import { active } from "d3-transition";

/**
 * @typedef {import('d3-selection').Selection<Element, any, any, undefined>} D3Selection
 * @typedef {import('d3-transition').Transition<Element, any, any, undefined>} D3Transition
 * @typedef {(this: Element, t: number) => string} D3Interpolator
 * @typedef {{
 *  name?: string,
 *  duration?: number
 * }} AnimOptions
 * @typedef {(
 *   ((elt: Vue|Vue.VNode|Element, options?: AnimOptions|null|Function, done?: Function) => void)
 * )} AnimFunction
 * @typedef {{ in: AnimFunction, out: AnimFunction }} Anim
 */

/**
 * @brief get the very last argument as a callback, manages an optional parameters
 * @param  {IArguments} args
 * @return {Function}
 */
function getCallback(args) {
  var done = last(args);
  return isFunction(done) ? done : noop;
}

/**
 * @param  {Element} elt
 */ // eslint-disable-next-line
function getStyle(elt) { // jshint ignore:line
  return window.getComputedStyle(elt, null);
}

/**
 * @brief retrieve HTMLElement from various objects
 * @param  {Vue|Vue.VNode|Element} elt
 * @return {Element}
 */
function getDom(elt) {
  if (has(elt, "elm")) {
    // @ts-ignore
    return /** @type {Vue.VNode} */ (elt).elm;
  }
  else if (has(elt, "$el")) {
    /* VueComponent */
    return /** @type {Vue} */ (elt).$el;
  }
  return /** @type {Element} */ (elt);
}

/**
 * @param  {number} max
 * @param  {number} min
 * @return {number}
 */
function getRandom(max, min) {
  return Math.floor((Math.random() * (1 + max - min)) + min);
}

/**
 * @param  {D3Selection} selection
 * @param  {string} name
 * @param  {AnimOptions|null|undefined} options
 * @return {D3Transition}
 */
function getAnim(selection, name, options) {
  const trans = selection.property("anim-" + name);
  if (trans && active(selection.node(), name)) {
    if (get(options, "cancel", false)) {
      selection.interrupt(name);
      return selection.transition(name);
    }
    return trans.transition();
  }
  return selection.transition(name);
}

/**
 * @param  {D3Selection} selection
 * @param  {string} name
 * @param  {D3Transition} transition
 */
function setAnim(selection, name, transition) {
  selection.property("anim-" + name, transition);
  return transition;
}

/**
 * @param  {string|number|undefined} style
 * @param  {string|number} defaultValue
 * @return {string|number}
 */
function defaultStyle(style, defaultValue) {
  return (!style || style === "none") ? defaultValue : style;
}

/**
 * @param  {string|number} from
 * @param  {string|number} to
 * @param  {Function} [interpolator=interpolateNumber]
 * @return {() => D3Interpolator}
 */ // eslint-disable-next-line
function styleFactory(from, to, interpolator = interpolateNumber) { // jshint ignore:line
  return function() {
    return interpolator(from, to);
  };
}

/**
 * @details move from current value to provided one
 * @param {string} name
 * @param {string|number} from
 * @param {string|number} to
 * @param {Function} [interpolator=interpolateNumber]
 * @return {() => D3Interpolator}
 */ // eslint-disable-next-line
function toStyleFactory(name, from, to, interpolator = interpolateNumber) { // jshint ignore:line
  /** @this {Element} */
  return function() {
    return interpolator(defaultStyle(select(this).style(name), from), to);
  };
}

/**
 * @details move from provided value to current one
 * @param {string} name
 * @param {string|number} from
 * @param {string|number} to
 * @param {Function} [interpolator=interpolateNumber]
 * @return {() => D3Interpolator}
 */
function fromStyleFactory(name, from, to, interpolator = interpolateNumber) {
  /** @this {Element} */
  return function() {
    return interpolator(from, defaultStyle(select(this).style(name), to));
  };
}

/**
 * @brief direct setSyles plugin for d3-transition
 * @param {D3Transition} transition
 * @param {{ [name: string]: string|number|null }} styles
 */
function setStyles(transition, styles) {
  transition.on("start", () => {
    const sel = transition.selection();
    // @ts-ignore
    forEach(styles, (value, name) => sel.style(name, value));
  });
}

var BaseAnimation = {
  default: "fade",
  animationSpeed: 1,
  /**
   * @param {Element} elt
   * @param {number} opacity
   */
  setOpacity(elt, opacity) { /* not an animation, just an helper */
    return select(elt).style("opacity", opacity);
  },
  getDom: getDom,
  /**
   * @param  {Vue|Vue.VNode|Element} elt
   * @param  {AnimOptions} options
   */
  randomTranslateIn(elt, options) {
    const done = getCallback(arguments);
    const name = get(options, "name", "base-anim");
    const sel = select(getDom(elt));

    setAnim(sel, name,
      getAnim(sel, name, options)
      .call((transition) => {
        transition.on("start", () => transition.selection()
        .style("position", "absolute")
        .style("x", getRandom(300, -300))
        .style("y", getRandom(300, -300)));
      })
      .transition()
      .style("x", 0)
      .style("y", 0)
      .duration(get(options, "duration", 1200) / BaseAnimation.animationSpeed)
      .ease(easeBounceOut)
    )
    .end().then(() => done(), () => done());
  },
  /** @type {{ in: AnimFunction, out: AnimFunction }} */
  rot90: {
    in(elt, options) {
      const done = getCallback(arguments);
      const name = get(options, "name", "base-anim");
      const sel = select(getDom(elt));

      setAnim(sel, name,
        getAnim(sel, name, options)
        .duration(get(options, "duration", 300) / BaseAnimation.animationSpeed)
        .ease(easeBounceOut)
        .style("transform", "rotate(90deg)")
      )
      .end().then(() => done(), () => done());
    },
    out(elt, options) {
      const done = getCallback(arguments);
      const name = get(options, "name", "base-anim");
      const sel = select(getDom(elt));

      setAnim(sel, name,
        getAnim(sel, name, options)
        .duration(get(options, "duration", 300) / BaseAnimation.animationSpeed)
        .ease(easeBounceOut)
        .style("transform", "rotate(0deg)")
      )
      .end().then(() => done(), () => done());
    }
  },
  /**
    @type {Anim &
      { inInterpolator: D3Interpolator, outInterpolator: D3Interpolator }}
   */
  flip: {
    inInterpolator: interpolateTransformCss("rotateX(-90deg)", "rotateX(0deg)"),
    in(elt, options) {
      const done = getCallback(arguments);
      const name = get(options, "name", "base-anim");
      const sel = select(getDom(elt));
      sel.attr("data-anim", "d3");

      setAnim(sel, name,
        getAnim(sel, name, options)
        .duration(get(options, "duration", 500) / BaseAnimation.animationSpeed)
        .ease(easeBounceOut)
        .style("opacity", 1)
        .styleTween("transform", () => BaseAnimation.flip.inInterpolator)
      )
      .end().then(() => done(), () => done());
    },
    outInterpolator: interpolateTransformCss("rotateX(0deg)", "rotateX(-90deg)"),
    out(elt, options) {
      const done = getCallback(arguments);
      const name = get(options, "name", "base-anim");
      const sel = select(getDom(elt));
      sel.attr("data-anim", "d3");

      setAnim(sel, name,
        getAnim(sel, name, options)
        .duration(get(options, "duration", 500) / BaseAnimation.animationSpeed)
        .ease(easeBounceOut)
        .style("opacity", 0)
        .styleTween("transform", () => BaseAnimation.flip.outInterpolator)
      )
      .end().then(() => done(), () => done());
    }
  },
  /** @type {Anim} */
  fade: {
    in(elt, options) {
      const done = getCallback(arguments);
      const name = get(options, "name", "base-anim");
      const sel = select(getDom(elt));
      sel.attr("data-anim", "d3");

      setAnim(sel, name,
        getAnim(sel, name, options)
        .duration(get(options, "duration", 300) / BaseAnimation.animationSpeed)
        .style("opacity", 1)
        .ease(easeCubicOut)
      )
      .end().then(() => done(), () => done());
    },
    out(elt, options) {
      const done = getCallback(arguments);
      const name = get(options, "name", "base-anim");
      const sel = select(getDom(elt));
      sel.attr("data-anim", "d3");

      setAnim(sel, name,
        getAnim(sel, name, options)
        .duration(get(options, "duration", 300) / BaseAnimation.animationSpeed)
        .style("opacity", 0)
        .ease(easeCubicOut)
      )
      .end().then(() => done(), () => done());
    }
  },
  /**
    @type {Anim &
      { inScaleInterpolator: D3Interpolator, outScaleInterpolator: D3Interpolator }}
   */
  pop: {
    inScaleInterpolator: interpolateTransformCss("scale(0.001)", "scale(1)"),
    in(elt, options) {
      const done = getCallback(arguments);
      const name = get(options, "name", "base-anim");
      const sel = select(getDom(elt));
      sel.attr("data-anim", "d3");

      setAnim(sel, name,
        getAnim(sel, name, options)
        .duration(get(options, "duration", 300) / BaseAnimation.animationSpeed)
        .style("visibility", "visible")
        .styleTween("padding-top", fromStyleFactory("padding-top", "0px", "0px", interpolateString))
        .styleTween("padding-bottom", fromStyleFactory("padding-bottom", "0px", "0px", interpolateString))
        .styleTween("height", fromStyleFactory("height", "0px", "0px", interpolateString))
        .styleTween("transform", () => BaseAnimation.pop.inScaleInterpolator)
        .ease(easeBackOut)
        .transition()
        .call(setStyles, {
          display: null, visibility: "visible",
          "padding-top": null, "padding-bottom": null, height: null
        })
      )
      .end().then(() => done(), () => done());
    },
    outScaleInterpolator: interpolateTransformCss("scale(1)", "scale(0.001)"),
    out(elt, options) {
      const done = getCallback(arguments);
      const name = get(options, "name", "base-anim");
      const sel = select(getDom(elt));
      sel.attr("data-anim", "d3");

      setAnim(sel, name,
        getAnim(sel, name, options)
        .duration(get(options, "duration", 300) / BaseAnimation.animationSpeed)
        .style("padding-top", "0px")
        .style("padding-bottom", "0px")
        .style("height", "0px")
        .styleTween("transform", () => BaseAnimation.pop.outScaleInterpolator)
        .ease(easeBackIn)
        .transition()
        .call(setStyles, {
          display: "none", visibility: "hidden",
          "padding-top": null, "padding-bottom": null, height: null
        })
      )
      .end().then(() => done(), () => done());
    }
  },
  /** @type {Anim} */
  height: {
    in(elt, options) {
      const done = getCallback(arguments);
      const name = get(options, "name", "base-anim");
      const sel = select(getDom(elt));
      sel.attr("data-anim", "d3");

      setAnim(sel, name,
        getAnim(sel, name, options)
        .duration(get(options, "duration", 300) / BaseAnimation.animationSpeed)
        .style("visibility", "visible")
        .styleTween("padding-top", fromStyleFactory("padding-top", "0px", "0px", interpolateString))
        .styleTween("padding-bottom", fromStyleFactory("padding-bottom", "0px", "0px", interpolateString))
        .styleTween("height", fromStyleFactory("height", "0px", "0px", interpolateString))
        .ease(easeBackOut)
        .transition()
        .call(setStyles, {
          display: null, visibility: "visible",
          "padding-top": null, "padding-bottom": null, height: null
        })
      )
      .end().then(() => done(), () => done());
    },
    out(elt, options) {
      const done = getCallback(arguments);
      const name = get(options, "name", "base-anim");
      const sel = select(getDom(elt));
      sel.attr("data-anim", "d3");

      setAnim(sel, name,
        getAnim(sel, name, options)
        .duration(get(options, "duration", 300) / BaseAnimation.animationSpeed)
        .style("padding-top", "0px")
        .style("padding-bottom", "0px")
        .style("height", "0px")
        .style("visibility", "visible")
        .ease(easeBackIn)
        .transition()
        .call(setStyles, {
          display: "none", visibility: "hidden",
          "padding-top": null, "padding-bottom": null, height: null
        })
      )
      .end().then(() => done(), () => done());
    }
  },
  /** @type {Anim} */
  width: {
    in(elt, options) {
      const done = getCallback(arguments);
      const name = get(options, "name", "base-anim");
      const sel = select(getDom(elt));
      sel.attr("data-anim", "d3");

      setAnim(sel, name,
        getAnim(sel, name, options)
        .duration(get(options, "duration", 300) / BaseAnimation.animationSpeed)
        .style("visibility", "visible")
        .styleTween("padding-left", fromStyleFactory("padding-left", "0px", "0px", interpolateString))
        .styleTween("padding-right", fromStyleFactory("padding-right", "0px", "0px", interpolateString))
        .styleTween("width", fromStyleFactory("width", "0px", "0px", interpolateString))
        .ease(easeBackOut)
        .transition()
        .call(setStyles, {
          display: null, visibility: "visible",
          "padding-left": null, "padding-right": null, width: null
        })
      )
      .end().then(() => done(), () => done());
    },
    out(elt, options) {
      const done = getCallback(arguments);
      const name = get(options, "name", "base-anim");
      const sel = select(getDom(elt));
      sel.attr("data-anim", "d3");

      setAnim(sel, name,
        getAnim(sel, name, options)
        .duration(get(options, "duration", 300) / BaseAnimation.animationSpeed)
        .style("padding-left", "0px")
        .style("padding-right", "0px")
        .style("width", "0px")
        .style("visibility", "visible")
        .ease(easeBackIn)
        .transition()
        .call(setStyles, {
          display: "none", visibility: "hidden",
          "padding-left": null, "padding-right": null, width: null
        })
      )
      .end().then(() => done(), () => done());
    }
  },
  /** @type {Anim} */
  delay: {
    in(elt, options) {
      const done = getCallback(arguments);
      const name = get(options, "name", "base-anim");
      const sel = select(getDom(elt));
      sel.attr("data-anim", "d3");

      setAnim(sel, name,
        getAnim(sel, name, options)
        .delay(get(options, "inDelay", 0) / BaseAnimation.animationSpeed)
      )
      .end().then(() => done(), () => done());
    },
    out(elt, options) {
      const done = getCallback(arguments);
      const name = get(options, "name", "base-anim");
      const sel = select(getDom(elt));
      sel.attr("data-anim", "d3");

      setAnim(sel, name,
        getAnim(sel, name, options)
        .delay(get(options, "outDelay", 0) / BaseAnimation.animationSpeed)
      )
      .end().then(() => done(), () => done());
    }
  },
  none: {
    /*
     * a noop animation, must exist to be referenced
     * main use is to disable animations on specific elements, ex:
     * ```pug
     * BaseAnimationGroup(anim="height")
     *   BaseModalLoader(:isLoading="isXXX" anim="none")
     *   div(v-if="isYYY")
     * ```
     */
  }
};

export default BaseAnimation;
