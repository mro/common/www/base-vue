// @ts-check
import _ from "lodash";
import Anim from "./BaseAnimation";
import Vue from "vue";

/*
 * CSS animations are easier to achieve in an inline-block that has no
 * margins/padding, which is what this components does
 */
const component = /** @type {V.Constructor<any, any>} */(Vue).extend({
  name: "BaseAnimationBlock",
  props: {
    appear: { type: Boolean, default: true },
    anim: { type: String, default: Anim.default },
    mode: { type: String, default: "out-in" }, /* set to empty for simultaneous*/
    options: { type: Object, default: null }
  },
  computed: {
    /**
     * @returns {{in?: function, out?: function}}
     */
    // @ts-ignore maybe we should limit anim props with an array of values
    animation() { return Anim[this.anim] || {}; },
    /**
     * @returns {{appear: boolean} & any}
     */
    animOptions() { return _.assign({ appear: this.appear }, this.options); }
  },
  methods: {
    /**
     * @param {Vue.VNode | Vue | Element } elt
     * @param {function} done
     */
    onEnter(elt, done) {
      this.$emit("onEnter", elt);
      if (this.animation.in) {
        this.animation.in(elt, this.animOptions, done);
      }
      else {
        done();
      }
    },
    /**
     * @param {Vue.VNode | Vue | Element} elt
     * @param {function} done
     */
    onLeave(elt, done) {
      this.$emit("onLeave", elt);
      if (this.animation.out) {
        this.animation.out(elt, this.animOptions, done);
      }
      else {
        done();
      }
    }
  }
});
export default component;
