// @ts-check

import _ from "lodash";
import Anim from "./BaseAnimation";
import Vue from "vue";

/*
 * CSS animations are easier to achieve in an inline-block that has no
 * margins/padding, which is what this components does
 */

const component = /** @type {V.Constructor<any, any>} */(Vue).extend({
  name: "BaseAnimationGroup",
  props: {
    tag: { type: String, default: "span" },
    appear: { type: Boolean, default: true },
    anim: { type: String, default: Anim.default }
  },
  computed: {
    /**
     * @returns {{in?: function, out?: function}}
     */
    // @ts-ignore maybe we should limit anim props with an array of values
    animation() { return Anim[this.anim] || {}; },
    /**
     * @returns {{appear: boolean}}
     */
    options() { return { appear: this.appear }; }
  },
  methods: {
    /**
     * @param {Vue.VNode | Vue | Element } elt
     * @param {function} done
     */
    onEnter(elt /*: any */, done /*: () => any */) {
      /** @type Element */
      const eltDom = Anim.getDom(elt);
      /** @type {{in?: function, out?: function}}  */
      const anim = _.get(Anim, eltDom.getAttribute("anim") || "", this.animation);
      if (anim.in) {
        anim.in(eltDom, this.options, done);
      }
      else {
        done();
      }
    },
    /**
     * @param {Vue.VNode | Vue | Element} elt
     * @param {function} done
     */
    onLeave(elt /*: any */, done /*: () => any */) {
      /** @type Element */
      const eltDom = Anim.getDom(elt);
      /** @type {{in?: function, out?: function}} */
      const anim = _.get(Anim, eltDom.getAttribute("anim") || "", this.animation);
      if (anim.out) {
        anim.out(eltDom, this.options, done);
      }
      else {
        done();
      }
    }
  }
});
export default component;
