// @ts-check
import Vue from "vue";
import Anim from "./BaseAnimation";
import { first } from "lodash";

/*
 * CSS animations are easier to achieve in an inline-block that has no
 * margins/padding, which is what this components does
 */
/**
 * @typedef {{
 *   loader: HTMLDivElement
 * }} Refs
 */
const component = /** @type {V.Constructor<any, Refs>} */ (Vue).extend({
  name: "BaseAnimationLoader",
  props: {
    loading: Boolean,
    anim: { type: String, default: "default" }
  },
  computed: {
    /**
     * @returns {{in?: function, out?: function}}
     */
    // @ts-ignore maybe we should limit anim props with an array of values
    animation() { return Anim[this.anim] || {}; }
  },
  methods: {
    /**
     * @param {Vue|Vue.VNode|Element|undefined} elt
     * @param {function} done
     */
    onEnter(elt, done) {
      if (elt && (elt === first(this.$slots.loader) ||
          elt === this.$refs.loader)) {
        Anim.fade.in(elt, null, done);
      }
      else if (this.animation.in) {
        this.animation.in(elt, null, done);
      }
      else {
        done();
      }
    },
    /**
     * @param {Vue|Vue.VNode|Element|undefined} elt
     * @param {function} done
     */
    onLeave(elt, done) {
      if (elt && (elt === first(this.$slots.loader) ||
          elt === this.$refs.loader)) {
        Anim.fade.out(elt, null, done);
      }
      else if (this.animation.out) {
        this.animation.out(elt, null, done);
      }
      else {
        done();
      }
    }
  }
});
export default component;
