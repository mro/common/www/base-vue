// @ts-check

import Vue from "vue";

/**
 * @typedef {typeof BaseVue.BaseModal} BaseModal
 * @typedef {{ modal: V.Instance<BaseModal> }} Refs
 */

const component = /** @type {V.Constructor<any, Refs>} */(Vue).extend({
  name: "BaseAbout",
  props: {
    title: { type: String, default: "About ..." },
    components: { type: Object, default: null },
    contributors: { type: Array, default: null },
    support: { type: Object, default: null }
  },
  methods: {
    toggle() {
      this.$refs.modal?.toggle();
    }
  }
});

export default component;
