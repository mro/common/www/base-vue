// @ts-check

import Vue from "vue";

import KeyboardEventMixin from "../mixins/BaseKeyboardEventMixin";
import { BaseStoreMixin } from "../store/utils";

/**
 * @typedef {typeof BaseVue.BaseModal} BaseModal
 * @typedef {{ sidebar: V.Instance<BaseModal> }} Refs
 */

const component = /** @type {V.Constructor<any, Refs>} */(Vue).extend({
  name: "BaseWebApp",
  mixins: [ KeyboardEventMixin({ local: false }), BaseStoreMixin ],
  props: {
    title: { type: String, default: "" },
    version: { type: String, default: "" },
    showRoutes: { type: Boolean, default: true },
    showThemeSwitcher: { type: Boolean, default: false },
    showAbout: { type: Boolean, default: false }
  },
  computed: {
    /** @return {boolean} */
    hasRouter() { return this.$route !== undefined; },
    /** @return {boolean} */
    showKeyHints() { return this.getBaseState([ "ui", "showKeyHints" ]); }
  },
  beforeMount() {
    this.dispatchBase("ui/applyTheme");
  },
  mounted() {
    this.onKey("ctrl-m-keydown", this.toggleSideBar);
  },
  methods: {
    /**
     * @param  {Event} event
     */
    toggleSideBar(event) {
      if (event) { event.preventDefault(); }
      this.$refs.sidebar.toggle();
    }
  }
});

export default component;
