// @ts-check

import Vue from "vue";

import { BaseStoreMixin } from "../store/utils";

const component = /** @type {V.Constructor<any, any>} */(Vue).extend({
  name: "BaseWebSite",
  mixins: [ BaseStoreMixin ],
  props: {
    title: { type: String, default: "" },
    version: { type: String, default: "" },
    showRoutes: { type: Boolean, default: true },
    showThemeSwitcher: { type: Boolean, default: false },
    showAbout: { type: Boolean, default: false }
  },
  computed: {
    hasRouter() { return this.$route !== undefined; }
  },
  beforeMount() {
    this.dispatchBase("ui/applyTheme");
  }
});

export default component;
