// @ts-check

import { filter, get, isFunction } from "lodash";
import $ from "jquery";
import Vue from "vue";

import logger from "../Logger/BaseLogger";
import KeyboardEventMixin from "../mixins/BaseKeyboardEventMixin";
import { BaseStoreMixin } from "../store/utils";
import ThemeSwitcher from "./ThemeSwitcher.vue";

/**
 * @typedef {{
 *  toggler: HTMLElement,
 *  nav: HTMLElement
 * }} Refs
 */

const component = /** @type {V.Constructor<any, Refs> } */ (Vue).extend({
  name: "BaseNavBar",
  components: { ThemeSwitcher },
  mixins: [ KeyboardEventMixin({ local: false }), BaseStoreMixin ],
  props: {
    title: { type: String, default: "" },
    version: { type: String, default: "" },
    showRoutes: { type: Boolean, default: true },
    showThemeSwitcher: { type: Boolean, default: false },
    showAbout: { type: Boolean, default: false }
  },
  /**
   * @return {{ errorsExist: boolean }}
   */
  data() {
    return { errorsExist: false };
  },
  computed: {
    // FIXME: add RouteUtilsMixin in base-vue
    /** @return {boolean} */
    isAdmin() { return get(this.$route, [ "meta", "isAdmin" ], false); },
    /**
     * @return {boolean}
     */
    showKeyHints() { return this.getBaseState([ "ui", "showKeyHints" ]); },
    /**
     * @return {boolean}
     */
    darkMode() { return this.getBaseState([ "ui", "darkMode" ]); },
    /**
     * @return {boolean}
     */
    username() { return this.getBaseGetter([ "username" ]); },
    /** @return {any[]} */
    navRoutes() {
      if (!this.showRoutes) { return []; }
      else {
        return filter(get(this.$router, [ "options", "routes" ]),
          (r) => {
            const nav = get(r, "meta.navbar", true);
            return (isFunction(nav)) ? nav(this) : nav;
          });
      }
    }
  },
  watch: {
    "$route.name": function() {
      /* hide nav if toggler is visible and displayed */
      if (this.$refs.nav && this.$refs.nav.classList.contains("show") &&
          (get(window.getComputedStyle(this.$refs.toggler, null), "display") !== "none")) {
        this.$refs.toggler.click();
      }
    }
  },
  mounted() {
    // @ts-ignore
    $('[data-toggle="tooltip"]').tooltip();
    this.onKey("ctrl-e-keydown", this.toggleErrors);
    this.onKey("ctrl-k-keydown", this.toggleKeyboardShortcuts);
    this.onKey("f1-keydown", this.openKeyboardShortcutsModal);
    logger.on("error", this.setErrorExist);
  },
  destroyed() {
    logger.removeListener("error", this.setErrorExist);
  },
  methods: {
    setErrorExist() {
      this.errorsExist = true;
    },
    /**
     * @param {Event} event
     */
    toggleErrors(event) {
      if (event) {
        event.preventDefault();
      }
      this.$emit("toggle-errors");
    },
    /**
     * @param {Event} event
     */
    openKeyboardShortcutsModal(event) {
      if (event) {
        event.preventDefault();
      }
      this.$emit("toggle-keyboard-modal");
    },
    /**
     * @param {Event} event
     */
    toggleKeyboardShortcuts(event) {
      if (event) {
        event.preventDefault();
      }
      this.commitBase("ui/update", { showKeyHints: !this.showKeyHints });
    },
    /**
     * @param {string} name
     */
    hasRoute(name) {
      return get(this.$router.resolve(name),
        [ "resolved", "matched", "length" ], 0) > 0;
    }
  }
});
export default component;
