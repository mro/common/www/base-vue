// @ts-check

import Vue from "vue";
import { startsWith } from "lodash";

const component = /** @type {V.Constructor<any, any>} */(Vue).extend({
  name: "BaseNavBarItem",
  props: {
    path: { type: String, default: "" },
    isDisabled: { type: Boolean, default: false }
  },
  computed: {
    active: function() {
      if (this.isDisabled) {
        return false;
      }
      else if (this.path === "/") {
        return this.$route.path === this.path;
      }
      return startsWith(this.$route.path, this.path);
    }
  }
});
export default component;
