// @ts-check

import Vue from "vue";
import { filter, get, isFunction } from "lodash";
import MatchMedia from "../BaseMatchMedia";
import { BaseStoreMixin } from "../store/utils";

const component = /** @type {V.Constructor<any, any> } */ (Vue).extend({
  name: "BaseSideBar",
  mixins: [ BaseStoreMixin ],
  props: {
    showRoutes: { type: Boolean, default: true },
    showAbout: { type: Boolean, default: false }
  },
  /**
   * @returns {{
   *   media?: ?MatchMedia,
   *   hidden: boolean,
   *   absolute: boolean
   *  }}
   */
  data() { return { absolute: true, hidden: false }; },
  computed: {
    /**
     * @return {boolean}
     */
    darkMode() { return this.getBaseState([ "ui", "darkMode" ]); },
    /** @return {boolean} */
    isAdmin() { return get(this.$route, [ "meta", "isAdmin" ], false); },
    /** @return {any[]} */
    navRoutes() {
      if (!this.showRoutes) { return []; }
      else {
        return filter(get(this.$router, [ "options", "routes" ]),
          (r) => {
            const nav = get(r, "meta.navbar", true);
            return (isFunction(nav)) ? nav(this) : nav;
          });
      }
    }
  },
  watch: {
    "$route.name": function() {
      /* hide nav if toggler is visible and displayed */
      if (this.absolute && !this.hidden) {
        this.hidden = true;
      }
    }
  },
  mounted() {
    this.media = new MatchMedia(MatchMedia.LG, (/** @type {boolean} */ value) => {
      this.absolute = !value;
      if (this.absolute) {
        this.hidden = true;
      }
    });
  },
  beforeDestroy() {
    /** @type {MatchMedia} */ (this.media).close();
  },
  methods: {
    toggle() {
      this.hidden = !this.hidden;
    }
  }
});
export default component;
