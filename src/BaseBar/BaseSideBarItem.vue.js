// @ts-check

import Vue from "vue";
import { isEmpty, startsWith } from "lodash";

const component = /** @type {V.Constructor<any, any>} */(Vue).extend({
  name: "BaseSideBarItem",
  props: {
    path: { type: String, default: "" },
    isActive: { type: Boolean, default: false },
    isDisabled: { type: Boolean, default: false }
  },
  computed: {
    active: function() {
      if (this.isDisabled) {
        return false;
      }
      else if (this.isActive || isEmpty(this.path)) {
        return this.isActive;
      }
      else if (this.path === "/") {
        return this.$route.path === this.path;
      }
      return startsWith(this.$route.path, this.path);
    }
  }
});
export default component;
