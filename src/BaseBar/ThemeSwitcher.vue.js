// @ts-check

import Vue from "vue";
import { BaseStoreMixin } from "../store/utils";

const component = /** @type {V.Constructor<any, any> } */ (Vue).extend({
  name: "ThemeSwitcher",
  mixins: [ BaseStoreMixin ],
  computed: {
    /**
     * @return {string}
     */
    darkMode() { return this.getBaseState([ "ui", "darkMode" ]); },
    /**
     * @return {string|null}
     */
    routerDarkMode() {
      return this.$store?.state?.route?.query?.darkMode ?? null;
    }
  },
  watch: {
    routerDarkMode: {
      immediate: true,
      handler() {
        if (this.routerDarkMode && !this.darkMode) {
          this.toggleTheme();
        }
      }
    }
  },
  methods: {
    toggleTheme() {
      this.dispatchBase("ui/toggleTheme");
    }
  }
});
export default component;
