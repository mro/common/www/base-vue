// @ts-check
import Vue from "vue";
import AnimGroup from "./BaseAnimation/BaseAnimationGroup.vue";
import AnimBlock from "./BaseAnimation/BaseAnimationBlock.vue";
import HasSlotMixin from "./mixins/BaseHasSlotMixin";

const component = /** @type {V.Constructor<any, any> } */ (Vue).extend({
  name: "BaseCard",
  components: { AnimGroup, AnimBlock },
  mixins: [ HasSlotMixin ],
  props: {
    title: { type: String, default: "" }
  }
});
export default component;
