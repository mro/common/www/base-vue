// @ts-check
import AnimBlock from "./BaseAnimation/BaseAnimationBlock.vue";
import Anim from "./BaseAnimation/BaseAnimation";
import Vue from "vue";
// FIXME add animations support

/**
 * @typedef {{ caret: Element }} Refs
 */

const component = /** @type {V.Constructor<any, Refs>} */ (Vue).extend({
  name: "BaseCollapsible",
  components: { AnimBlock },
  props: {
    title: { type: String, default: "" },
    expand: { type: Boolean, default: false },
    counter: { type: Number, default: null }
  },
  /**
   * @return {{ isExpanded: boolean }}
   */
  data() { return { isExpanded: this.expand }; },
  watch: {
    /**
     * @param {boolean} value
     */
    expand(value) {
      this.isExpanded = value;
    },
    /**
     * @param {boolean} value
     * @return {void}
     */
    isExpanded: function(value) {
      if (value) {
        Anim.rot90.in(this.$refs.caret);
      }
      else {
        Anim.rot90.out(this.$refs.caret);
      }
      // v3 compatible v-model
      this.$emit("update:expand", this.isExpanded);
    }
  },
  mounted() {
    if (this.isExpanded) {
      Anim.rot90.in(this.$refs.caret);
    }
  },
  methods: {
    toggle() {
      this.isExpanded = !this.isExpanded;
    },
    show(value = true) {
      this.isExpanded = value;
    }
  }
});
export default component;
