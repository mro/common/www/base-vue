// @ts-check

import Modal from "./BaseModal.vue";
import Vue from "vue";
import { makeDeferred } from "@cern/prom";
import KeyboardEventMixin from "./mixins/BaseKeyboardEventMixin";

/**
 * @typedef {{ modal: V.Instance<typeof Modal> }} Refs
 */

const options = {
  /** @type  {prom.Deferred<any> | null} */
  _request: null
};

const component = /** @type {V.Constructor<typeof options, Refs>} */ (Vue).extend({
  name: "BaseDialog",
  ...options,
  components: { Modal },
  mixins: [ KeyboardEventMixin({ local: true }) ],
  props: {
    title: { type: String, default: "" }
  },
  /**
   * @return {{ text: string, visible: boolean }}
   */
  data() {
    return { text: "", visible: false };
  },
  mounted() {
    if (!this.$scopedSlots["footer"]) {
      this.onKey("esc", () => this.resolve(false));
      this.onKey("enter", () => this.resolve(true));
    }
  },
  beforeDestroy() {
    this.reject(new Error("destroyed"));
  },
  methods: {
    /**
     * @param  {boolean} [hidden]
     */
    onHiddenEvent(hidden) {
      this.$emit("hidden", hidden);
      if (hidden) {
        this.reject(new Error("hidden"));
      }
    },
    /**
     * @param  {any} [value] promise output value
     */
    resolve(value) {
      if (this.$options._request) {
        this.visible = false;
        this.$refs.modal.show(false);
        this.$options._request.resolve(value);
        this.$options._request = null;
      }
    },
    /**
     * @param  {any} err promise rejection error
     */
    reject(err) {
      if (this.$options._request) {
        this.visible = false;
        this.$refs.modal.show(false);
        this.$options._request.reject(err);
        this.$options._request = null;
      }
    },
    /**
     * @template T=any
     * @return {Promise<T>}
     */
    async request() {
      this.reject(new Error("aborted"));
      this.visible = true;
      this.$refs.modal.show(true);
      this.$options._request = makeDeferred();
      return this.$options._request.promise;
    }
  }
});
export default component;
