// @ts-check
import { select } from "d3-selection";
import "d3-transition"; // load the plugin
import Vue from "vue";

/**
 * @typedef {import('d3-selection').BaseType} BaseType
 * @typedef {import('d3-selection').Selection<BaseType, BaseType, HTMLElement, any>} Selection
 */

const component = /** @type {V.Constructor<any, any> } */ (Vue).extend({
  name: "BaseExpiryIcon",
  props: {
    expiry: { type: Number, default: NaN },
    color: { type: String, default: "#545b62" }
  },
  /**
   * @return {{
   *   start: number,
   *   selection?: Selection
   * }}
   */
  data() { return { start: Date.now(), selection: undefined }; },
  computed: {
    /**
     * @return {string}
     */
    tooltip() {
      return (Number.isNaN(this.expiry)) ? "never" : (new Date(this.expiry)).toISOString();
    }
  },
  watch: {
    expiry() {
      this.restart();
    }
  },
  mounted() {
    // @ts-ignore I do not get this. select wants a string. What are we doing?
    this.selection = select(this.$refs.circle);
    this.restart();
  },
  beforeDestroy() {
    if (this.selection) {
      this.selection.interrupt();
    }
  },
  methods: {
    restart() {
      this.start = Date.now();
      this.update();
    },
    update() {
      const now = Date.now();
      if (this.selection && Number.isNaN(this.expiry)) {
        this.selection.interrupt().attr("stroke-dashoffset", 0);
      }
      else if (this.selection && now > this.expiry + 200) { // take rollback animation in account
        this.selection.interrupt().attr("stroke-dashoffset", 805);
      }
      else {
        let remain = this.expiry - now;
        const range = (this.expiry - this.start);
        const position = ((range - remain) / range) * 804;
        if (this.selection) {
          this.selection.interrupt()
          .transition()
          .call((transition) => {
            const current = (transition.selection().attr("stroke-dashoffset"));
            // @ts-ignore they're numbers here
            if ((current - position) > 10) {
              transition.duration(200)
              .attr("stroke-dashoffset", position);
              transition = transition.transition();
              remain -= 200;
            }
            transition.duration(remain)
            .attr("stroke-dashoffset", 805);
          });
        }
      }
    }
  }
});
export default component;
