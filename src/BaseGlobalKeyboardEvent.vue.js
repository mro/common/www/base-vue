// @ts-check
import _ from "lodash";
import BaseKeyboardEventMixin from "./mixins/BaseKeyboardEventMixin";
import Vue from "vue";

/**
  * @typedef {{ render: any }} Options
  */
const component = /** @type {V.Constructor<any, any>} */(Vue).extend({
  name: "BaseGlobalKeyboardEvent",
  mixins: [ BaseKeyboardEventMixin({ local: false }) ],
  props: {
    inputCheck: { type: Boolean, default: true }
  },
  mounted() {
    this.setCheckOnInput(this.inputCheck);
    _.forEach(this.$listeners, (cb, key) => {
      this.onKeyEmit(key);
    });
  },
  render(h) {
    return h(); // Remove missing template error
  }
});
export default component;
