// @ts-check
import Anim from "./BaseAnimation/BaseAnimation";
import Vue from "vue";

const component = /** @type {V.Constructor<any, any>} */ (Vue).extend({
  name: "BaseImage",
  props: {
    src: { type: String, default: null },
    href: { type: String, default: null }
  },
  /**
   * @return {{
   *   error: boolean,
   *   loading: boolean
   * }}
   */
  data() {
    return { error: false, loading: true };
  },
  watch: {
    loading() {
      Anim.fade.in(this.$el);
    }
  }
});
export default component;
