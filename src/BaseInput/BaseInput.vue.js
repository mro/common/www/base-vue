// @ts-check
import { has, invoke, isEmpty, isNil, toString } from "lodash";
import Vue from "vue";

import InputError from "./InputError.vue";
import InputErrorMixin from "./InputErrorMixin";
import BaseKeepFocusMixin from "../mixins/BaseKeepFocusMixin";
import BaseFocusInOutMixin from "../mixins/BaseFocusInOutMixin";

const component = /** @type {V.Constructor<any, any>} */(Vue).extend({
  name: "BaseInput",
  components: { InputError },
  mixins: [ BaseKeepFocusMixin, BaseFocusInOutMixin, InputErrorMixin ],
  model: { prop: "value", event: "edit" },
  props: {
    value: { type: [ String, Number ], default: undefined },
    inEdit: { type: Boolean, default: false },
    noEditIcon: { type: Boolean, default: false }
  },
  /**
   * @return {{ editValue: string | null, hasEditRequest: boolean }}
   */
  data() { return { editValue: null, hasEditRequest: false }; },
  computed: {
    /**
     * @return {boolean}
     */
    hasValueSlot() {
      return has(this.$scopedSlots, "value");
    },
    /**
     * @return {boolean}
     */
    showEditIcon() {
      return this.hasFocus && !this.hasValueSlot && this.hasEditRequest;
    }
  },
  watch: {
    /**
     * @param {string|number|null} v
     * @param {string|number|null} old
     */
    value(v /*: ?(string|number) */, old /*: ?(string|number) */) {
      if (isEmpty(this.editValue) || (this.editValue === toString(old))) {
        this.editValue = toString(v);
      }
    },
    /**
     * @param {boolean} v
     */
    inEdit(v /*: boolean */) {
      if (v) {
        const editValue = toString(this.value);
        if (editValue === this.editValue) {
          // Force emit the event when we enter edit mode
          this.$emit("edit", this.editValue);
        }
        else {
          this.editValue = editValue; /* clear old editValue when entering edit */
        }
      }
      this.keepFocusOnNextTick("input");
    },
    /**
     * @return {void}
    */
    editValue: function() {
      this.$emit("edit", this.editValue);
    }
  },
  mounted() {
    if (!isNil(this.value)) {
      this.editValue = toString(this.value);
    }
    this.hasEditRequest = has(this.$listeners, "edit-request");
  },
  methods: {
    /**
     * @param  {boolean} value
     */
    emitEditRequest(value) {
      invoke(this.$refs, [ "input", "focus" ]);
      this.keepFocusOnNextTick("input");
      this.$emit("edit-request", value);
    }
  }
});
export default component;
