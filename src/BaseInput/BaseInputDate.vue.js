// @ts-check
import { has, invoke, isEmpty, isNil } from "lodash";

import InputError from "./InputError.vue";
import InputErrorMixin from "./InputErrorMixin";
import TDPicker from "../plugins/TempusDominusDatePicker.vue";
import BaseKeepFocusMixin from "../mixins/BaseKeepFocusMixin";
import BaseFocusInOutMixin from "../mixins/BaseFocusInOutMixin";
import Vue from "vue";

const component = /** @type {V.Constructor<any, any>} */(Vue).extend({
  name: "BaseInputDate",
  components: { InputError, TDPicker },
  mixins: [ BaseKeepFocusMixin, BaseFocusInOutMixin, InputErrorMixin ],
  model: { prop: "timestamp", event: "edit" },
  props: {
    timestamp: { type: Number, default: null },
    inEdit: { type: Boolean, default: false },
    noEditIcon: { type: Boolean, default: false }
  },
  /**
   * @return {{ editTimestamp: number | null, hasEditRequest: boolean }}
   */
  data() {
    return { editTimestamp: null, hasEditRequest: false };
  },
  computed: {
    /**
     * @return {boolean}
     */
    showEditIcon() {
      return this.hasFocus && this.hasEditRequest;
    }
  },
  watch: {
    /**
     * @param {number?} v
     * @param {number?} old
     */
    timestamp(v, old) {
      if (isEmpty(this.editTimestamp) || (this.editTimestamp === old)) {
        this.editTimestamp = v;
      }
    },
    /**
     * @param {boolean} v
     */
    inEdit(v) {
      if (v) {
        this.editTimestamp = this.timestamp; /* clear old editTimestamp when entering edit */
        this.$emit("edit", this.editTimestamp);
      }
      this.keepFocusOnNextTick("input");
    },
    /**
     * @param {number} value
     */ // @ts-ignore
    editTimestamp(value) {
      this.$emit("edit", value);
    }
  },
  mounted() {
    if (!isNil(this.timestamp)) {
      this.editTimestamp = this.timestamp;
    }
    this.hasEditRequest = has(this.$listeners, "edit-request");
  },
  methods: {
    /**
     * @param {number} timestamp
     */
    onChange(timestamp) {
      this.editTimestamp = timestamp;
    },
    /**
     * @param  {boolean} value
     */
    emitEditRequest(value) {
      invoke(this.$refs, [ "input", "focus" ]);
      this.keepFocusOnNextTick("input");
      this.$emit("edit-request", value);
    }
  }
});
export default component;
