// @ts-check
import { get, has, invoke } from "lodash";

import InputError from "./InputError.vue";
import InputErrorMixin from "./InputErrorMixin";
import BaseKeepFocusMixin from "../mixins/BaseKeepFocusMixin";
import BaseFocusInOutMixin from "../mixins/BaseFocusInOutMixin";
import Vue from "vue";

const component = /** @type {V.Constructor<any, any>} */(Vue).extend({
  name: "BaseInputFile",
  components: { InputError },
  mixins: [ BaseKeepFocusMixin, BaseFocusInOutMixin, InputErrorMixin ],
  props: {
    value: { type: String, default: undefined },
    inEdit: { type: Boolean, default: false },
    noEditIcon: { type: Boolean, default: false }
  },
  /**
   * @return {{ editValue: File|null, hasEditRequest: boolean }}
   */
  data() { return { editValue: null, hasEditRequest: false }; },
  computed: {
    /**
     * @returns {string}
     */
    editName() {
      return get(this.editValue, [ "name" ], "");
    },
    /**
     * @return {boolean}
     */
    showEditIcon() {
      return this.hasFocus && this.hasEditRequest;
    }
  },
  watch: {
    /**
     * @param {boolean} v
     */
    inEdit(v /*: boolean */) {
      if (v) {
        if (this.editValue) {
          // Force emit the event when we enter edit mode
          this.$emit("edit", this.editValue);
        }
        else {
          this.editValue = null; /* clear old editValue when entering edit */
        }
      }
      this.keepFocusOnNextTick("input");
    },
    /**
     * @return {void}
    */
    editValue: function() {
      this.$emit("edit", this.editValue);
    }
  },
  mounted() {
    this.hasEditRequest = has(this.$listeners, "edit-request");
  },
  methods: {
    onFileChange() {
      this.editValue = get(this.$refs, [ "input", "files", 0 ], null);
    },
    /**
     * @param  {boolean} value
     */
    emitEditRequest(value) {
      invoke(this.$refs, [ "input", "focus" ]);
      this.keepFocusOnNextTick("input");
      this.$emit("edit-request", value);
    }
  }
});
export default component;
