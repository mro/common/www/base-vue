// @ts-check
import { get, isEmpty, isNil, omit, toString } from "lodash";

import InputError from "./InputError.vue";
import InputErrorMixin from "./InputErrorMixin";
import Vue from "vue";

/**
 * @typedef {{
 *  hiddenInput: HTMLInputElement
 * }} Refs
 */
const component = /** @type {V.Constructor<any, Refs>} */ (Vue).extend({
  name: "BaseInputReadonly",
  components: { InputError },
  mixins: [ InputErrorMixin ],
  model: { prop: "value", event: "edit" },
  props: {
    value: { type: [ String, Number ], default: undefined },
    inEdit: { type: Boolean, default: false }
  },
  /**
   * @return {{
   *   editValue?: string | null
   * }}
   */
  data() { return { editValue: null }; },
  watch: {
    /**
     * @param {string|number|null} v
     * @param {string|number|null} old
     */
    value(v /*: ?(string|number) */, old /*: ?(string|number) */) {
      if (isEmpty(this.editValue) || (this.editValue === toString(old))) {
        this.editValue = toString(v);
      }
    },
    /**
     * @param {boolean} v
     */
    inEdit(v /*: boolean */) {
      if (v) {
        this.editValue = toString(this.value); /* clear old editValue when entering edit */
      }
    },
    /**
     * @return {void}
    */
    editValue: function() {
      this.$emit("edit", this.editValue);
    }
  },
  mounted() {
    if (!isNil(this.value)) {
      this.editValue = toString(this.value);
    }
  },
  methods: {
    /**
     * @returns {Record<string, string>}
     */
    filterAttrs() {
      return omit(this.$attrs, [ "readonly" ]);
    },
    /**
     * @return {Promise<boolean>}
     */
    checkValidity() {
      return this.$nextTick()
      .then(() => {
        if (this.$refs["hiddenInput"].checkValidity()) {
          this.removeError("b-checkValidity");
          return true;
        }
        else {
          this.addError("b-checkValidity",
            get(this.$refs["hiddenInput"], "validationMessage", "invalid value"));
          return false;
        }
      });
    }
  }
});
export default component;
