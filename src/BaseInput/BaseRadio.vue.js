// @ts-check

import Vue from "vue";
import { forEach, indexOf } from "lodash";

import BaseToggle from "./BaseToggle.vue";


/**
 * @details Radio buttons act like a group
 * but no event is emitted when unchecked, keep track of radio buttons
 * @type {{ [name: string]: Instance[] }}
 */
const radioMap = {};

/**
 * @param {string} name
 * @param {any} elt
 */
function radioMapAdd(name, elt) {
  if (!name) { return; }
  let radios = radioMap[name];
  if (!radios) {
    radios = radioMap[name] = [];
  }
  radios.push(elt);
}

/**
 * @param {string} name
 * @param {any} elt
 */
function radioMapRemove(name, elt) {
  const radios = radioMap[name];
  const idx = indexOf(radios, elt);
  if (idx >= 0) {
    radios.splice(idx, 1);
    if (radios.length === 0) {
      delete radioMap[name];
    }
  }
}

/**
 * @typedef {V.Instance<typeof component> &
  *  V.Instance<typeof BaseToggle>} Instance
  * @typedef {{ input: HTMLInputElement }} Refs
  */
const component = /** @type {V.Constructor<any, Refs>} */(Vue).extend({
  name: "BaseRadio",
  extends: BaseToggle,
  props: {
    name: { type: String, required: true }
  },
  watch: {
    name: {
      immediate: true,
      handler: function(value, oldValue) {
        radioMapRemove(oldValue, this);
        radioMapAdd(value, this);
      }
    },
    editValue(value) {
      if (value) {
        this.setChecked();
      }
      this.$refs.input.checked = value;
    }
  },
  beforeDestroy() {
    radioMapRemove(this.name, this);
  },
  methods: {
    setChecked() {
      forEach(radioMap[this.name], (radio) => {
        radio.editValue = radio === this;
      });
    }
  }
});
export default component;
