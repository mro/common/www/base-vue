// @ts-check

import { find, get, has, invoke, isEqual, map, toString } from "lodash";
import BaseKeepFocusMixin from "../mixins/BaseKeepFocusMixin";
import BaseFocusInOutMixin from "../mixins/BaseFocusInOutMixin";
import InputError from "./InputError.vue";
import InputErrorMixin from "./InputErrorMixin";
import Vue from "vue";

/**
 * @typedef {{
 *   optList: HTMLSelectElement
 * }} Refs
 */

const component = /** @type {V.Constructor<any, Refs>} */ (Vue).extend({
  name: "BaseSelect",
  components: { InputError },
  mixins: [ BaseKeepFocusMixin, BaseFocusInOutMixin, InputErrorMixin ],
  model: { prop: "value", event: "edit" },
  props: {
    value: { type: [ String, Number ], default: undefined },
    options: { type: Array, default: undefined },
    inEdit: { type: Boolean, default: false },
    noEditIcon: { type: Boolean, default: false }
  },
  /**
    * @return {{
    *   editValue?: string | number | null,
    *   domOpts: { value: any; text: any; }[] | null,
    *   hasEditRequest: boolean
    *  }}
    */
  data() {
    return { editValue: null, domOpts: null, hasEditRequest: false };
  },
  computed: {
    /**
     * @return {boolean}
     */
    showEditIcon() {
      return this.hasFocus && this.hasEditRequest;
    },
    /**
     * @returns {string}
     */
    valueText() {
      const option = find(this.options || this.domOpts,
        (/** @type {{ value: any; text: any; }}*/opt) => {
          // eslint-disable-next-line eqeqeq
          return opt.value == this.value;
        });
      return get(option, "text") ?? toString(this.value);
    },
    /**
     * @returns {string}
     */
    editValueText() {
      const option = find(this.options || this.domOpts,
        (/** @type {{ value: any; text: any; }}*/opt) => {
          // eslint-disable-next-line eqeqeq
          return opt.value == this.editValue;
        });
      return get(option, "text") ?? toString(this.editValue);
    }
  },
  watch: {
    /**
     * @param {string | number | null} v
     * @param {string | number | null} old
     */
    value(v, old) {
      if (this.editValue === old) {
        this.editValue = v;
      }
    },
    /**
     * @param {boolean} v
     */
    inEdit(v) {
      if (v) {
        this.editValue = this.value; /* clear old editValue when entering edit */
      }
      this.keepFocusOnNextTick("input");
    },
    /**
     * @return {void}
    */
    editValue: function() {
      this.$emit("edit", this.editValue);
    }
  },
  mounted() {
    this.editValue = this.value;
    this.genDomOpts();
    this.hasEditRequest = has(this.$listeners, "edit-request");
  },
  updated() {
    this.genDomOpts();
  },
  methods: {
    genDomOpts() {
      if (!this.options && this.$refs.optList) {
        var opts = map(this.$refs.optList.querySelectorAll("option"),
          function(o) {
            return { value: o.getAttribute("value"), text: o.text };
          });
        if (!isEqual(opts, this.domOpts)) {
          this.domOpts = opts;
        }
      }
    },
    async checkValidity() { return true; },
    /**
     * @param  {boolean} value
     */
    emitEditRequest(value) {
      invoke(this.$refs, [ "input", "focus" ]);
      this.keepFocusOnNextTick("input");
      this.$emit("edit-request", value);
    }
  }
});
export default component;
