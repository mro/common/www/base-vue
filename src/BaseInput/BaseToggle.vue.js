// @ts-check

import { has, isNil } from "lodash";
import Vue from "vue";

import { genId } from "../utils";
import InputError from "./InputError.vue";
import InputErrorMixin from "./InputErrorMixin";
import BaseKeepFocusMixin from "../mixins/BaseKeepFocusMixin";
import BaseFocusInOutMixin from "../mixins/BaseFocusInOutMixin";

const component = /** @type {V.Constructor<any, any>} */(Vue).extend({
  name: "BaseToggle",
  components: { InputError },
  mixins: [ BaseKeepFocusMixin, BaseFocusInOutMixin, InputErrorMixin ],
  model: { prop: "value", event: "edit" },
  props: {
    value: { type: Boolean, default: false },
    label: { type: String, default: "" },
    disabled: { type: Boolean, default: false },
    inEdit: { type: Boolean, default: false }
  },
  /**
   * @returns {{
   *   editValue: boolean | null,
   *   id: string,
   *   hasEditRequest: boolean
   * }}
   */
  data() {
    return { editValue: null, id: genId(), hasEditRequest: false };
  },
  watch: {
    /**
     * @param {boolean} v
     */
    inEdit(v /*: boolean */) {
      if (v) {
        this.editValue = Boolean(this.value); /* clear old editValue when entering edit */
      }
      this.keepFocusOnNextTick("input");
    },
    /**
     * @param {boolean} v
     */
    value(v /*: boolean */) {
      // We force editValue if in edit mode
      if (this.inEdit) {
        this.editValue = v;
      }
    },
    /**
     * @return {void}
    */
    editValue: function() {
      this.$emit("edit", this.editValue);
    }
  },
  mounted() {
    if (!isNil(this.value)) {
      this.editValue = Boolean(this.value);
    }
    this.hasEditRequest = has(this.$listeners, "edit-request");
  },
  methods: {
    toggle() {
      this.editValue = !this.editValue;
      this.$emit("userEdit", this.editValue);
    }
  }
});
export default component;
