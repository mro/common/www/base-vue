// @ts-check

import Vue from "vue";

const component = /** @type {V.Constructor<any, any>} */(Vue).extend({
  name: "InputError",
  props: {
    errors: { type: Object, default: null },
    warnings: { type: Object, default: null },
    showErrors: { type: Boolean, default: true }
  }
});
export default component;
