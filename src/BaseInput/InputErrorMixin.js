// @ts-check

import { filter, first, get, invoke, isEmpty } from "lodash";
import { mixinMaker } from "../utils";


const component = mixinMaker({
  /**
   * @return {{
   *  warnings: { [id: string]: any },
   *  errors: { [id: string]: any }
   * }}
   */
  data: () => ({ warnings: {}, errors: {} }),
  computed: {
    /**
     * @return {boolean}
     */
    hasErrors() {
      return !(isEmpty(this.warnings) && isEmpty(this.errors));
    }
  },
  methods: {
    async checkValidity() {
      await this.$nextTick();
      const errors = filter(invoke(this.$el, "querySelectorAll", "input"),
        function(node) { return !node.checkValidity(); });
      if (isEmpty(errors)) {
        this.removeError("b-checkValidity");
        return true;
      }
      else {
        this.addError("b-checkValidity",
          get(first(errors), "validationMessage", "invalid value"));
        return false;
      }
    },
    /**
     * @param {string} id
     * @param {string} message
     */
    addError(id, message) {
      this.$set(this.errors, id, message);
    },
    /**
     * @param {string} id
     */
    removeError(id) {
      this.$delete(this.errors, id);
    },
    /**
     * @param {string} id
     * @param {string} message
     */
    addWarning(id, message) {
      this.$set(this.warnings, id, message);
    },
    /**
     * @param {string} id
     */
    removeWarning(id) {
      this.$delete(this.warnings, id);
    }
  }
});
export default component;
