// @ts-check

import Vue from "vue";
import { isBoolean, isString } from "lodash";


const component = /** @type {V.Constructor<any, any>} */(Vue).extend({
  name: "BaseLED",
  props: {
    blink: { type: Boolean, default: false },
    label: { type: String, default: undefined },
    value: { type: [ String, Boolean ], default: false }
  },
  computed: {
    /**
     * @returns {string}
     */
    actualValue() {
      if (isBoolean(this.value)) {
        return this.value ? "var(--yellow)" : "transparent";
      }
      else if (isString(this.value)) {
        return this.value;
      }
      else {
        return "transparent";
      }
    }
  }
});

export default component;
