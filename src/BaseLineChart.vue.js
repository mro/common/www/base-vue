// @ts-check
/* eslint-disable max-lines */
import Vue from "vue";
import {
  defaultTo, get, isArray, isNil, isNumber, isObject, map, toNumber,
  toString, wrap } from "lodash";
import { scaleLinear } from "d3-scale";
import { compositeChart, legend, lineChart } from "dc";
import { curveLinear, curveMonotoneX } from "d3-shape";
import crossfilter from "crossfilter2";
import { genId, getDateTime } from "./utils";
import d from "debug";
import { DateTime, Interval } from "luxon";

import BaseResizeObserverMixin from "./mixins/BaseResizeObserverMixin";

const debug = d("base:chart");

/**
 * @typedef {any} Datum
  */

const options = {
  /** @type {crossfilter.Crossfilter<any>|null} */
  _crossfilter: null,
  /** @type {ReturnType<compositeChart>|null} */
  _chart: null
};

/**
 *
 * @param {number[] | null} value
 * @returns
 */
function xAxisRangeValidator(value) {
  // null value is ok -> dynamic y range
  if (isNil(value)) { return true; }
  // Only 2 values
  if (value.length !== 2) { return false; }
  // Both are numbers
  if (!isNumber(value[0]) || !isNumber(value[1])) { return false; }
  // Range is correct
  if (value[0] >= value[1]) { return false; }
  return true;
}

/**
 * @param  {function} fun
 * @param  {number} load
 */
function loadThrottle(fun, load) {
  let lastCall = 0;
  let lastCallDuration = 0;
  /** @type {NodeJS.Timeout|null} */
  let timer = null;
  /** @type {any[]} */
  let lastArgs = [];

  const ret = function _loadThrottle(/** @type {any} */...args) {
    const now = Date.now();
    const duration = now - lastCall;

    if (timer) {
      debug("throttled");
      lastArgs = args;
    }
    else if (duration && (lastCallDuration / duration) >= load) {
      debug("throttled (lastCallDuration:%i)", lastCallDuration);
      lastArgs = args;
      timer = setTimeout(() => {
        timer = null;
        debug("rendering (throttled)");
        _loadThrottle(...lastArgs);
      }, (load ? (lastCallDuration / load) : 0) + 10);
    }
    else {
      fun(...args);
      lastCall = now;
      lastCallDuration = Date.now() - now;
    }
  };
  ret.cancel = function() {
    if (!timer) { return; }
    debug("throttled rendering cancelled");
    clearTimeout(timer);
    timer = null;
  };
  return ret;
}

const component = /** @type {V.Constructor<typeof options, any>} */ (Vue).extend({
  name: "BaseLineChart",
  ...options,
  mixins: [ BaseResizeObserverMixin ],
  props: {
    data: { type: Array, default: () => [] },
    xAxis: { type: String, default: null },
    yAxis: { type: [ String, Array, Object ], default: null },
    yAxisRange: { type: Array, default: null, validator: xAxisRangeValidator },
    xAxisLabel: { type: String, default: null },
    yAxisLabel: { type: String, default: null },
    maxData: { type: Number, default: 100 },
    smooth: { type: Boolean, default: true },
    isTimestamp: { type: Boolean, default: false },
    xyTips: { type: Boolean, default: null }
  },
  /**
   * @return {{
   *   id: string,
   *   xscale: any,
   *   yscale: any,
   *   timeFormat: string|null,
   *   showMenu: boolean,
   *   paused: boolean,
   *   documentVisible: boolean
   * }}
   */
  data() {
    return {
      id: genId(),
      xscale: scaleLinear(),
      yscale: scaleLinear(),
      timeFormat: null,
      showMenu: false,
      paused: false,
      documentVisible: true
    };
  },
  watch: {
    data: function() {
      this.__update();
    },
    paused: function(value) {
      if (value) { this.redraw(); }
    },
    isTimestamp: function() {
      this.__updateScale();
      this.redraw();
    },
    smooth: function(value) {
      const chart = this.$options._chart;
      if (chart) {
        chart.children().forEach(
          // @ts-ignore
          (/** @type {ReturnType<lineChart>} */ c) => c.curve(
            value ? curveMonotoneX : curveLinear));
        this.redraw();
      }
    }
  },
  mounted() {
    // This doesn't count for total rendering, only js part but should
    // be representative (~5%)
    this.redraw = loadThrottle(this.redraw, 0.01);
    this.render = loadThrottle(this.render, 0.01);

    this.__init();
    window.document
    .addEventListener("visibilitychange", this.onVisibilityChange);
  },
  beforeDestroy() {
    this.__release();
    window.document
    .removeEventListener("visibilitychange", this.onVisibilityChange);
    // @ts-ignore
    this.redraw.cancel?.();
    // @ts-ignore
    this.render.cancel?.();
  },
  methods: {
    onResize() {
      this.render();
    },
    /**
     * @brief prepare data for crossfilter
     * @param {any} data
     * @return {any[]}
     */ // eslint-disable-next-line complexity
    __prepareData(data) {
      if (isNil(data)) {
        data = [];
      }
      else if (!isArray(data)) {
        data = [ data ];
      }
      /* plain arrays support */
      if (!isObject(data[0])) {
        const indexKey = this.xAxis || "key";
        const valueKey = isArray(this.yAxis) ?
          this.yAxis[0] : (this.yAxis || "value");
        const startIndex = defaultTo(
          this.$options._chart ?
            (this.getXValue(this.$options._chart.dimension().top(1)[0]) + 1) :
            0,
          0);
        data = map(data,
          (d, index) => ({ [valueKey]: d, [indexKey]: index + startIndex }));
      }
      return data;
    },
    __init() {
      this.$options._crossfilter =
        crossfilter(this.__prepareData(this.data));
      const chart = this.$options._chart = compositeChart("#" + this.id);

      let axis = this.yAxis;
      if (!isObject(axis) && !isArray(this.yAxis)) {
        axis = [ axis ];
      }

      // Note: methods are already bound, we can directly give this.getYValue
      const dimension = this.$options._crossfilter.dimension(this.getXValue);
      chart
      .x(this.xscale)
      .y(this.yscale)
      .xAxisLabel(this.xAxisLabel).yAxisLabel(this.yAxisLabel)
      .brushOn(false)
      .elasticY(this.yAxisRange ? false : true)
      .elasticX(false)
      .transitionDuration((this.maxData > 100) ? 0 : 200)
      .clipPadding(20)
      .title(this.tooltip)
      .shareColors(true)
      .dimension(dimension);

      chart.margins().right = 0;

      if (!isArray(axis)) {
        chart.compose(map(axis,
          (name, key) => this.__makeChart(chart, key, name)));
        this.$options._chart.legend(legend().x(50).y(10).itemHeight(8));
      }
      else {
        chart.compose(map(axis,
          (key, idx) => this.__makeChart(chart, key, toString(idx))));
      }

      chart.xAxis().tickFormat(this.__formatValue);
      this.__updateScale();
      chart.render();
    },
    /**
     * @param  {ReturnType<compositeChart>} chart
     * @param  {string} key
     * @param  {string} name
     * @return {ReturnType<lineChart>}
     */
    __makeChart(chart, key, name) {
      const subChart = lineChart(chart)
      .dimension(chart.dimension())
      .xyTipsOn(this.xyTips ?? (this.maxData <= 100))
      .renderDataPoints({ radius: 2, fillOpacity: 0.4, strokeOpacity: 0.0 })
      // @ts-ignore
      .curve(this.smooth ? curveMonotoneX : curveLinear)
      .group(chart.dimension().group().reduceSum(
        (/** @type {Datum} */ d) => this.getYValue(key, d)), name);
      subChart._drawLine = wrap(subChart._drawLine,
        /** @this {ReturnType<lineChart>} */ function(fun, ...args) {
          const dur = this.transitionDuration();
          this.transitionDuration(0);
          const ret = fun.apply(this, args);
          this.transitionDuration(dur);
          return ret;
        });
      return subChart;
    },
    /**
     * @param  {any} v
     */
    __formatValue(v) {
      if (this.timeFormat) {
        if (v instanceof Date) {
          return (DateTime.fromJSDate(v)).toFormat(this.timeFormat);
        }
        else {
          return (getDateTime(v || 0)).toFormat(this.timeFormat);
        }
      }
      return v;
    },
    __release() {
      if (this.$options._chart) {
        this.$options._chart.children().forEach((c) => c.group().dispose());
        this.$options._chart.dimension().dispose();
      }
    },
    __update() {
      if (this.$options._crossfilter) {
        this.$options._crossfilter.remove(() => true);
        this.$options._crossfilter.add(this.__prepareData(this.data));
        this.__updateScale();
        this.redraw();
      }
    },
    /**
     * @param {any|any[]} value
     */
    add(value) {
      if (!this.$options._crossfilter || !this.$options._chart) { return; }
      debug("add: ", value);
      const cf = this.$options._crossfilter;
      cf.add(this.__prepareData(value));
      if (cf.size() > this.maxData) {
        const rem = cf.size() - this.maxData;
        // @ts-ignore
        cf.remove((d, /** @type {number} */i) => (i < rem));
      }
      this.__updateScale();
      this.redraw();
    },
    redraw() {
      if (!this.$options._chart || this.paused || !this.documentVisible) {
        return;
      }
      this.$options._chart.redraw();
    },
    render() {
      if (this.$options._chart) { this.$options._chart.render(); }
    },
    // eslint-disable-next-line complexity
    __updateScale() {
      if (this.$options._chart && this.$options._crossfilter) {
        const chart = this.$options._chart;

        if (this.yAxisRange) {
          this.yscale.domain(this.yAxisRange);
        }

        const xdomain = [
          this.getXValue(chart.dimension().bottom(1)[0]) || 0,
          this.getXValue(chart.dimension().top(1)[0]) ||
            this.$options._crossfilter.size()
        ];
        this.xscale.domain(xdomain);
        let duration;
        if (xdomain[0] instanceof Date) {
          duration = Interval.fromDateTimes(xdomain[0], xdomain[1]).toDuration("seconds");
        }
        else if (this.isTimestamp) {
          duration = Interval.fromDateTimes(
            getDateTime(xdomain[0]), getDateTime(xdomain[1])).toDuration("seconds");
        }

        if (duration) {
          this.timeFormat = (duration.seconds > 3600) ? "F" : "hh:mm:ss.SSS";
        }
        else {
          this.timeFormat = null;
        }
      }
    },
    /**
     * @param  {{ key: any, value: any }} d
     * @return {string}
     */
    tooltip(d) {
      return `${this.__formatValue(d.key)}: ${d.value}`;
    },
    /**
     * @param {string} prop
     * @param {Datum} d
     * @return {any}
     */
    getYValue(prop, d) {
      return toNumber(get(d, [ prop || "value" ], d));
    },
    /**
     * @param  {Datum} d
     * @return {any}
     */
    getXValue(d) {
      return get(d, [ this.xAxis || "key" ]);
    },
    onVisibilityChange() {
      this.documentVisible = !(window.document.visibilityState === "hidden");
      if (this.documentVisible) { this.redraw(); }
    }
  }
});

export default component;
