// @ts-check
import MarkdownViewer from "./plugins/MarkdownViewer.vue";
import Vue from "vue";

const component = /** @type {V.Constructor<any, any>} */(Vue).extend({
  name: "BaseMarkdownViewer",
  extends: MarkdownViewer
});
export default component;
