// @ts-check
import { invoke, isEmpty, isNil, toString } from "lodash";

import AnimGroup from "./BaseAnimation/BaseAnimationGroup.vue";
import CodeMirrorEditor from "./plugins/CodeMirrorEditor.vue";
import MarkdownViewer from "./plugins/MarkdownViewer.vue";
import BaseRibbon from "./BaseRibbon.vue";
import BaseKeyboardEventMixin from "./mixins/BaseKeyboardEventMixin";
import Vue from "vue";

/**
 * @typedef {{
 *  cmEditor: V.Instance<typeof CodeMirrorEditor>
 * }} Refs
 */


const component = /** @type {V.Constructor<any, Refs>} */ (Vue).extend({
  name: "BaseMarkdownWidget",
  components: { CodeMirrorEditor, MarkdownViewer, BaseRibbon, AnimGroup },
  mixins: [ BaseKeyboardEventMixin({ local: true }) ],
  props: {
    value: { type: [ String ], default: "" },
    inEdit: { type: Boolean, default: false },
    maxLength: { type: Number, default: -1 }
  },
  /**
   * @returns {{
   *   editValue: string | null,
   *   showPreview: boolean,
   *   cmOptions: any,
   *   mdOptions: any
   * }}
   */
  data() {
    return {
      editValue: "",
      showPreview: false,
      cmOptions: {
        mode: "markdown",
        lineNumbers: true,
        line: true,
        tabSize: 2
      },
      mdOptions: {
        html: true,
        linkify: true,
        typographer: true
      }
    };
  },
  watch: {
    /**
     * @param {string|null} v
     * @param {string|null} old
     */
    value(v /*: ?(string) */, old /*: ?(string) */) {
      if (isEmpty(this.editValue) || (this.editValue === toString(old))) {
        this.editValue = toString(v);
      }
    },
    /**
     * @param {boolean} v
     */
    inEdit(v /*: boolean */) {
      if (v) {
        const editValue = toString(this.value);
        if (editValue === this.editValue) {
          // Force emit the event when we enter edit mode
          this.$emit("edit", this.editValue);
        }
        else {
          this.editValue = editValue; /* clear old editValue when entering edit */
        }
        this.refreshEditor();
      }
    },
    /**
     * @return {void}
    */
    editValue: function() {
      this.$emit("edit", this.editValue);
    }
  },
  mounted() {
    this.onKey("ctrl-enter-keydown", () => {
      if (!this.inEdit) { return; }
      this.togglePreview();
      // Manage Focus
      this.$nextTick(() => this.focus());
    });
    if (!isNil(this.value)) {
      this.editValue = toString(this.value);
    }
  },
  methods: {
    togglePreview() {
      this.showPreview = !this.showPreview;
    },
    focus() {
      if (this.showPreview) {
        invoke(this, [ "$el", "focus" ]);
      }
      else {
        invoke(this, [ "$refs", "cmEditor", "focus" ]);
      }
    },
    refreshEditor() {
      this.$nextTick(() => invoke(this, [ "$refs", "cmEditor", "refresh" ]));
    }
  }
});
export default component;
