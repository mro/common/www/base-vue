// @ts-check

export default class BaseMatchMedia {
  /**
   *
   * @param {string} query
   * @param {(match: boolean)=> any} cb
   */
  constructor(query, cb) {
    this.cb = (/** @type {MediaQueryListEvent} */ mm) => cb(mm.matches);
    if (typeof window.matchMedia !== "undefined") {
      /** @type {?MediaQueryList} */
      this.mqList = window.matchMedia(query);
      this.mqList.addListener(this.cb);
      return cb(this.mqList.matches);
    }
  }

  close() {
    if (this.mqList) {
      this.mqList.removeListener(this.cb);
      this.mqList = null;
    }
  }
}

BaseMatchMedia.SM = "(min-width: 576px)";
BaseMatchMedia.MD = "(min-width: 768px)";
BaseMatchMedia.LG = "(min-width: 992px)";
BaseMatchMedia.XL = "(min-width: 1200px)";
BaseMatchMedia.XXL = "(min-width: 1400px)";
