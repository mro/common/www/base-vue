// @ts-check

import $ from "jquery";
import Vue from "vue";
import HasSlotMixin from "./mixins/BaseHasSlotMixin";
import BaseKeyboardEventMixin from "./mixins/BaseKeyboardEventMixin";
import { BaseStoreMixin } from "./store/utils";

const options = {
  /** @type {JQuery<Element> | null} */
  _elt: null
};

const component = /** @type {V.Constructor<typeof options, any>} */ (Vue).extend({
  name: "BaseModal",
  ...options,
  mixins: [ HasSlotMixin, BaseStoreMixin,
            BaseKeyboardEventMixin({ local: true }) ],
  props: {
    title: { type: String, default: "" },
    visible: { type: Boolean, default: false }
  },
  computed: {
    /**
     * @return {boolean}
     */
    showKeyHints() {
      return this.getBaseState([ "ui", "showKeyHints" ]);
    }
  },
  watch: {
    /**
     * @param {boolean} value
     */
    visible(value) {
      this.show(value);
    }
  },
  mounted() {
    const elt = $(this.$el);

    /* enable closure by pressing 'Esc' key if slot 'title' is not overridden */
    if (!this.hasSlot("title")) { this.onKey("esc", () => elt.modal("hide")); }

    elt.data("b-destroyed", false);
    elt.data("b-hidden", true);
    this.$options._elt = elt;
    this.$options._elt.on("hidden.bs.modal", () => {
      elt.data("b-hidden", true);
      this.$emit("hidden", true);
      if (elt.data("b-destroyed")) {
        // @ts-ignore: modal is a bootstrap plugin
        elt.modal("dispose").off();
      }
    });
    this.$options._elt.on("show.bs.modal", () => {
      elt.data("b-hidden", false);
    });
    this.$options._elt.on("shown.bs.modal", () => {
      if (elt.data("b-destroyed")) {
        // @ts-ignore: modal is a bootstrap plugin
        elt.modal("hide");
      }
      else {
        this.$emit("hidden", false);
      }
    });
    this.show(this.visible);
  },
  beforeDestroy() {
    const elt = this.$options._elt;
    if (elt && elt.data("b-hidden")) {
      // @ts-ignore: modal is a bootstrap plugin
      elt.modal("dispose").off();
    }
    else if (elt) {
      elt.hide().data("b-destroyed", true);
    }
  },
  methods: {
    /**
     * @param {boolean} value
     */
    show(value = true) {
      if (!this.$options._elt) { return; }
      if (value) {
        // @ts-ignore: modal is a bootstrap plugin
        this.$options._elt.modal("show");
      }
      else {
        // @ts-ignore: modal is a bootstrap plugin
        this.$options._elt.modal("hide");
      }
    },
    toggle() {
      if (!this.$options._elt) { return; }
      // @ts-ignore: modal is a bootstrap plugin
      this.$options._elt.modal("toggle");
    }
  }
});
export default component;
