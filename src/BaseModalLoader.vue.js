// @ts-check

import AnimBlock from "./BaseAnimation/BaseAnimationBlock.vue";
import Vue from "vue";

const component = /** @type {V.Constructor<any, any>} */(Vue).extend({
  name: "BaseModalLoader",
  components: { AnimBlock },
  props: { isLoading: { type: Boolean, default: false } }
});
export default component;
