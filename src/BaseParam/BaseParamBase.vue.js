// @ts-check
import Vue from "vue";
import InputError from "../BaseInput/InputError.vue";
import ParamCard from "./ParamCard.vue";
import InputErrorMixin from "../BaseInput/InputErrorMixin";

// read-only parameter class
const component = /** @type {V.Constructor<any, any>} */(Vue).extend({
  name: "BaseParamBase",
  components: { InputError },
  extends: ParamCard,
  mixins: [ InputErrorMixin ],
  props: {
    value: { type: String, default: undefined },
    showErrors: { type: Boolean, default: true }
  }
});
export default component;
