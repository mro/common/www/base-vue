// @ts-check

import Vue from "vue";
import ParamCard from "./ParamCard.vue";
import BaseInputDate from "../BaseInput/BaseInputDate.vue";

const component = /** @type {V.Constructor<any, any>} */(Vue).extend({
  name: "BaseParamDate",
  components: { ParamCard },
  extends: BaseInputDate,
  props: {
    title: { type: String, default: undefined }
  }
});
export default component;
