// @ts-check

import Vue from "vue";
import ParamCard from "./ParamCard.vue";
import BaseInputFile from "../BaseInput/BaseInputFile.vue";

const component = /** @type {V.Constructor<any, any>} */(Vue).extend({
  name: "BaseParamFile",
  components: { ParamCard },
  extends: BaseInputFile,
  props: {
    title: { type: String, default: undefined }
  }
});
export default component;
