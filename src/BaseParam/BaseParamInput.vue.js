// @ts-check

import Vue from "vue";
import ParamCard from "./ParamCard.vue";
import BaseInput from "../BaseInput/BaseInput.vue";

const component = /** @type {V.Constructor<any, any>} */(Vue).extend({
  name: "BaseParamInput",
  components: { ParamCard },
  extends: BaseInput,
  props: {
    title: { type: String, default: undefined }
  }
});
export default component;
