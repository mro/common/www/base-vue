// @ts-check

import Vue from "vue";
import ParamCard from "./ParamCard.vue";
import BaseLED from "../BaseLED/BaseLED.vue";

const component = /** @type {V.Constructor<any, any>} */(Vue).extend({
  name: "BaseParamLED",
  components: { ParamCard },
  extends: BaseLED,
  props: {
    title: { type: String, default: undefined }
  }
});

export default component;
