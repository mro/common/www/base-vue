// @ts-check

import Vue from "vue";
import ParamCard from "./ParamCard.vue";
import BaseSelect from "../BaseInput/BaseSelect.vue";

const component = /** @type {V.Constructor<any, any>} */(Vue).extend({
  name: "BaseParamList",
  components: { ParamCard },
  extends: BaseSelect,
  props: {
    title: { type: String, default: undefined }
  }
});
export default component;
