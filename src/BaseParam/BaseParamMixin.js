// @ts-check
import { filter, first, get, isEmpty } from "lodash";
import Vue from "vue";

/**
 * @typedef {typeof import('./BaseParamBase.vue').default} BaseParam
 * @typedef {typeof import('../BaseInput/InputErrorMixin').default} InputErrorMixin
 * @typedef {{ base: V.Instance<BaseParam> & V.Instance<InputErrorMixin> }} Refs
 */

const component = /** @type {V.Constructor<any, Refs>} */ (Vue).extend({
  methods: {
    checkValidity() {
      return this.$nextTick()
      .then(() => {
        const errors = filter(this.$el.querySelectorAll("input"),
          function(node) { return !node.checkValidity(); });
        if (isEmpty(errors)) {
          this.$refs.base.removeError("b-checkValidity");
          return true;
        }
        else {
          this.$refs.base.addError("b-checkValidity",
            get(first(errors), "validationMessage", "invalid value"));
          return false;
        }
      });
    },
    /**
     * @param {string} id
     * @param {string} message
     */
    addError(id, message) { this.$refs.base.addError(id, message); },
    /**
     * @param {string} id
     */
    removeError(id) { this.$refs.base.removeError(id); },
    /**
     * @param {string} id
     * @param {string} message
     */
    addWarning(id, message) { this.$refs.base.addWarning(id, message); },
    /**
     * @param {string} id
     */
    removeWarning(id) { this.$refs.base.removeWarning(id); }
  }
});
export default component;
