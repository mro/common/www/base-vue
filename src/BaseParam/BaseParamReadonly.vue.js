// @ts-check

import Vue from "vue";
import ParamCard from "./ParamCard.vue";
import BaseInputReadonly from "../BaseInput/BaseInputReadonly.vue";

const component = /** @type {V.Constructor<any, any>} */(Vue).extend({
  name: "BaseParamReadonly",
  components: { ParamCard },
  extends: BaseInputReadonly,
  props: {
    title: { type: String, default: undefined }
  }
});
export default component;
