// @ts-check

import { invoke } from "lodash";
import Vue from "vue";

import ParamCard from "./ParamCard.vue";
import BaseToggle from "../BaseInput/BaseToggle.vue";

/**
 * @typedef {V.ExtVue<any,any> & V.Instance<BaseToggle>} Instance
 */

const component = /** @type {V.Constructor<any, any>} */(Vue).extend({
  name: "BaseParamToggle",
  components: { ParamCard },
  extends: BaseToggle,
  props: {
    title: { type: String, default: undefined }
  },
  computed: {
    /**
     * @this {Instance}
     * @return {boolean}
     */
    showEditIcon() {
      return this.hasFocus && this.hasEditRequest;
    }
  },
  methods: {
    /**
     * @this {Instance}
     * @param {boolean} value
     */
    emitEditRequest(value) {
      invoke(this.$refs, [ "input", "focus" ]);
      this.keepFocusOnNextTick("input");
      this.$emit("edit-request", value);
    }
  }
});
export default component;
