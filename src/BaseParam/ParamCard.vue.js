// @ts-check
import Vue from "vue";

// read-only parameter class
const component = /** @type {V.Constructor<any, any>} */(Vue).extend({
  name: "BaseParamCard",
  props: {
    title: { type: String, default: undefined },
    isPrimary: { type: Boolean, default: true }
  },
  computed: {
    /**
     * @return {string[]}
     */
    paramType() {
      return [ (this.isPrimary) ? "b-param-primary" : "b-param-secondary" ];
    }
  }
});
export default component;
