// @ts-check
import { indexOf } from "lodash";
import Vue from "vue";

const modes = [
  "top-left",
  "top-right",
  "bottom-left",
  "bottom-right"
];

const component = /** @type {V.Constructor<any, any>} */(Vue).extend({
  name: "BaseRibbon",
  props: {
    value: { type: String, default: "" },
    position: {
      type: String,
      validator: function(value) {
        // The value must match one of these strings
        return indexOf(modes, value) !== -1;
      },
      default: "top-right"
    }
  },
  data() {
    return {
      positionClass: "b-ribbon-" + this.position
    };
  }
});
export default component;
