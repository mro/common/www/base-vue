// @ts-check
import { first, get } from "lodash";
import Vue from "vue";
import { SSVGEngine } from "@cern/ssvg-engine";

import ErrorHandlerMixin from "./mixins/BaseErrorHandlerMixin";

/**
 * @typedef {{
 *  svg: HTMLElement
 * }} Refs
 * @typedef {{ engine: SSVGEngine|null }} Options
 */

const component = /** @type {V.Constructor<Options, Refs> } */ (Vue).extend({
  name: "BaseSSVG",
  mixins: [ ErrorHandlerMixin ],
  props: {
    iframe: { type: Boolean, default: false },
    src: { type: String, default: null },
    state: { type: Object, default: null },
    preserveAspectRatio: { type: String, default: null }
  },
  /**
   * @return {{ frameLoaded: boolean }}
   */
  data() {
    return { frameLoaded: false };
  },
  computed: {
    /** @return {boolean} */
    hasFrame() { return this.iframe || !!this.src; }
  },
  watch: {
    state() { this.updateState(this.state); }
  },
  mounted() {
    this.reload();
  },
  beforeDestroy() {
    this.release();
  },
  methods: {
    release() {
      if (this.$options.engine) {
        this.$options.engine.disconnect();
        this.$options.engine = null;
      }
    },
    reload() {
      this.release();
      try {
        if (!this.hasFrame) {
          this.$options.engine = new SSVGEngine(this.$refs.svg);
        }
        else if (this.frameLoaded) {
          const svg = this._injectFrame();
          if (svg) {
            this.$options.engine = new SSVGEngine(svg);
          }
        }
        if (this.state) {
          this.updateState(this.state);
        }
      }
      catch (e) {
        this.onError(e);
      }
    },
    /**
     * @param {any} state
     */
    updateState(state) {
      if (!this.$options.engine) { return; }
      this.$options.engine.updateState(state);
    },
    /**
     * @return {any}
     */
    getState() {
      if (!this.$options.engine) { return undefined; }
      return this.$options.engine.getState();
    },
    _injectFrame() {
      /** @type {HTMLElement} */
      const document = get(this.$refs.frame, [ "contentDocument" ]);
      if (!document) {
        console.warn("iframe unavailable");
        return null;
      }
      var svg = first(document.getElementsByTagName("svg"));
      if (!svg) {
        const body = get(document.getElementsByTagName("body"), 0, document);
        const slot = get(this.$slots, [ "default", 0, "elm" ]);
        if (!slot) {
          console.warn("default slot unavailable");
          return null;
        }
        body.appendChild(slot.cloneNode(true));
        body.setAttribute("style", "margin: 0;");
        svg = first(body.getElementsByTagName("svg"));
      }
      if (svg) {
        /* we won't be reactive on this, but it's better than nothing */
        if (this.preserveAspectRatio) {
          svg.setAttribute("preserveAspectRatio", this.preserveAspectRatio);
        }
        /* Note: set width on parent element */
        svg.setAttribute("width", "100%");
        svg.setAttribute("height", "100%");
      }
      return svg;
    },
    _onFrameLoaded() {
      this.frameLoaded = true;
      this.reload();
    }
  }
});
export default component;
