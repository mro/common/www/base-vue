// @ts-check

import { filter, first, noop, now, orderBy, some, sortedLastIndexBy,
  takeRight } from "lodash";
import Vue from "vue";
import logger from "./BaseLogger";
import { BaseStoreMixin } from "../store/utils";

const TIMER_EXTRA_TIME = 100;

const options = {
  /** @type  JQuery<Element> | null */
  _elt: null,
  /** @type {{(...args: any[]): void}} */
  _errorCB: noop,
  /** @type  NodeJS.Timeout | null */
  _timer: null
};

const component = /** @type {V.Constructor<typeof options, any>} */ (Vue).extend({
  name: "BaseErrorAlert",
  ...options,
  mixins: [ BaseStoreMixin ],
  props: {
    timeout: { type: Number, default: 5000 },
    maxErrors: { type: Number, default: 5 }
  },
  /**
   * @return {{
   *  errors: Array<BaseLogger.Error>,
   *  loggerErrors: Array<BaseLogger.Error>
   * }}
   */
  data() {
    return {
      errors: [],
      loggerErrors: []
    };
  },
  computed: {
    /**
     * @type {() => Array<BaseLogger.Error>}
     */
    visibleErrors: function() {
      /** @type Array<BaseLogger.Error> */
      const lastErrors = takeRight(this.errors, this.maxErrors);
      return orderBy(lastErrors, [ "timestamp" ], [ "desc" ]);
    },
    /**
     * @returns {number}
     */
    nbAlerts() {
      return this.loggerErrors?.length ?? 0;
    },
    muteBtnClass() {
      if (some(this.loggerErrors, [ "type", "error" ])) {
        return [ "btn-danger" ];
      }
      else if (some(this.loggerErrors, [ "type", "warning" ])) {
        return [ "btn-warning" ];
      }
      else {
        return [ "btn-secondary" ];
      }
    },
    /**
     * @return {boolean}
     */
    muteAlerts() { return this.getBaseState([ "ui", "muteAlerts" ]); }
  },
  mounted() {
    /**
     * @param {BaseLogger.Error} error
     */
    this.$options._errorCB = (error) => {
      this.loggerErrors = logger.getErrors();

      if (!this.$options._timer) {
        this.$options._timer = setTimeout(this.expireErrors.bind(this),
          this.timeout + TIMER_EXTRA_TIME);
      }
      this.errors.splice(sortedLastIndexBy(this.errors, error, "timestamp"),
        0, error);
    };
    logger.on("error", this.$options._errorCB);
  },
  destroyed() {
    logger.removeListener("error", this.$options._errorCB);
  },
  methods: {
    /**
     * @param {BaseLogger.Error} error
     * @returns {Array<string>}
     */
    alertClass(error) {
      if (error.type === "error") {
        return [ "alert-danger" ];
      }
      else if (error.type === "warning") {
        return [ "alert-warning" ];
      }
      else {
        return [ "alert-secondary" ];
      }
    },
    expireErrors() {
      const _now = now();
      this.errors = filter(this.errors,
        (error) => ((error.timestamp + this.timeout) > _now));

      const next = first(this.errors);
      if (next) {
        this.$options._timer = setTimeout(this.expireErrors.bind(this),
          Math.max(next.timestamp + this.timeout - _now, 100));
      }
      else {
        this.clearTimer();
      }
    },
    clearTimer() {
      if (this.$options._timer) {
        clearTimeout(this.$options._timer);
        this.$options._timer = null;
      }
    },
    toggleMuteAlerts() {
      this.commitBase("ui/update", { muteAlerts: !this.muteAlerts });
    }
  }
});
export default component;
