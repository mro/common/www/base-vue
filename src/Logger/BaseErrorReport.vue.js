// @ts-check

import Vue from "vue";
import { get, noop, startsWith, toString } from "lodash";
import logger from "./BaseLogger";
import $ from "jquery";
import Collapsible from "../BaseCollapsible.vue";
import MarkdownViewer from "../plugins/MarkdownViewer.vue";
import Modal from "../BaseModal.vue";
import { BaseStoreMixin } from "../store/utils";


const options = {
  /** @type  {JQuery<Element> | null} */
  _elt: null,
  /** @type {{(...args: any[]): void}} */
  _errorCB: noop
};

const component = /** @type {V.Constructor<typeof options, any>} */ (Vue).extend({
  name: "BaseErrorReport",
  ...options,
  components: { Collapsible, MarkdownViewer, Modal },
  mixins: [ BaseStoreMixin ],
  /**
   * @return {{
   *  errors: Array<BaseLogger.Error>
   * }}
   */
  data() {
    return { errors: [] };
  },
  computed: {
    /**
     * @return {boolean}
     */
    showKeyHints() { return this.getBaseState([ "ui", "showKeyHints" ]); },
    /**
     * @return {boolean}
     */
    muteAlerts() { return this.getBaseState([ "ui", "muteAlerts" ]); }
  },
  mounted() {
    this.$options._elt = $(this.$el);
    this.$options._errorCB = () => {
      this.errors = logger.getErrors();
    };
    logger.on("error", this.$options._errorCB);
    // Load errors already stored (localStorage)
    this.$options._errorCB();
  },
  destroyed() {
    if (this.$options._elt) {
      // @ts-ignore: modal is a plugin of bootstrap
      this.$options._elt.modal("dispose");
      logger.removeListener("error", this.$options._errorCB);
    }
  },
  methods: {
    /**
     * @param  {BaseLogger.Error} error
     * @return {Array<string>}
     */
    alertClass(error /*: Logger$Error */) {
      if (error.type === "error") {
        return [ "text-danger" ];
      }
      else if (error.type === "warning") {
        return [ "text-warning" ];
      }
      return [];
    },
    toggle() {
      // @ts-ignore: modal is a plugin of bootstrap
      this.$options._elt.modal("toggle");
    },
    /**
     * @param  {BaseLogger.Error} error
     * @return {string}
     */
    errorDetails(error) {
      try {
        return JSON.stringify(get(error, "error"), null, 2);
      }
      catch (e) {
        return "";
      }
    },
    clearErrors() {
      logger.clear();
      this.$options._errorCB(); // Reload
    },
    /**
     * @param  {BaseLogger.Error} error
     * @return {boolean}
     */
    hasBody(error) {
      const contentType = get(error,
        [ "error", "response", "headers", "content-type" ]);
      return startsWith(contentType, "text/html");
    },
    /**
     * @param  {BaseLogger.Error} error
     * @return {boolean}
     */
    errorBody(error) {
      // support axios and superagent
      return get(error, [ "error", "response", "data" ]) ||
        get(error, [ "error", "response", "body" ]);
    },
    /**
     * @param  {BaseLogger.Error} error
     * @return {string}
     */
    errorStack(error /*: Logger$Error */) {
      const stack = get(error, "error.stack");
      return stack ? toString(stack) : "";
    },
    /** @param  {BaseLogger.Error} error */
    errorConsole(error /*: Logger$Error */) {
      console.log(error);
    },
    toggleMuteAlerts() {
      this.commitBase("ui/update", { muteAlerts: !this.muteAlerts });
    }
  }
});
export default component;
