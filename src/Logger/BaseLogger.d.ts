
import { EventEmitter } from 'events'

export = BaseLogger;
export as namespace BaseLogger;

declare class BaseLoggerClass extends EventEmitter {
  error(error: any): void;
  warning(warning: any): void;
  getErrors(): Array<BaseLogger.Error>;
  clear(): void
}

declare const BaseLogger: BaseLoggerClass

declare namespace BaseLogger {
  interface Error {
    id: string,
    type: 'error' | 'warning',
    message: string,
    error: any,
    timestamp: number
  }
}
