// @ts-check
import { EventEmitter } from "events";
import { bindAll, get, hasIn, noop, now, sortedLastIndexBy, toString } from "lodash";
import { genId } from "../utils";
import Cookies from "js-cookie";

/**
 * @typedef {import('./BaseLogger').Error} LoggerError
 */

export const MAX_ERRORS_SIZE = 100;
export const LOCAL_STORAGE_FIELD = "baseLogger";

class Logger extends EventEmitter {
  constructor() {
    super();
    /*
    enable/disable auto-reload feature
    should be disabled in tests environements (where partial back-ends are used)
     */
    this.autoReload = true;

    /** @type {Array<LoggerError>} */
    this._errors = [];

    /** @type {string} */
    this._field = `${window.location.pathname}${LOCAL_STORAGE_FIELD}`;

    // Load errors from localstorage
    let errorFromLocalStorage;
    try {
      errorFromLocalStorage = JSON.parse(localStorage.getItem(this._field) || "[]");
      this._errors = errorFromLocalStorage;
    }
    catch { /** noop */ }

    /* error messages must be handled */
    this.on("error", noop);
    bindAll(this, [ "error", "warning" ]);
  }

  /**
   * @param {any} error
   */
  _wrapMessage(error) {
    if (hasIn(error, "message")) {
      return hasIn(error, "name") ?
        (error.name + ": " + error.message) : error.message;
    }
    return toString(error);
  }

  /**
   * @param {any} error
   */
  error(error) {
    /* axios network error */
    if (this.autoReload &&
        (get(error, "message") === "Network Error") &&
        !Cookies.get("auth-reload-cookie")) {
      console.log("Network error, force-reloading the page");
      Cookies.set("auth-reload-cookie", "1", { "max-age": "10" });
      // @ts-ignore (some browsers have an extra argument)
      window.location.reload(true);
      return;
    }

    /** @type {LoggerError} */
    const err = {
      id: genId(),
      message: this._wrapMessage(error),
      error: error,
      timestamp: now(),
      type: "error"
    };
    this._storeAndEmit(err);
  }

  /**
   * @param {any} warning
   */
  warning(warning) {
    /** @type {LoggerError} */
    const warn = {
      id: genId(),
      message: this._wrapMessage(warning),
      error: warning,
      timestamp: now(),
      type: "warning"
    };
    this._storeAndEmit(warn);
  }

  /**
   * @param {any} errOrWarn
   */
  _storeAndEmit(errOrWarn) {
    this._errors.splice(sortedLastIndexBy(this._errors, errOrWarn, "timestamp"), 0, errOrWarn);
    if (this._errors.length > MAX_ERRORS_SIZE) {
      this._errors.splice(0, this._errors.length - MAX_ERRORS_SIZE);
    }
    this.emit("error", errOrWarn);
    localStorage.setItem(this._field, JSON.stringify(this._errors));
  }

  getErrors() {
    return this._errors;
  }

  clear() {
    this._errors = [];
    localStorage.setItem(this._field, JSON.stringify(this._errors));
  }
}

const logger = new Logger();

export default logger;
