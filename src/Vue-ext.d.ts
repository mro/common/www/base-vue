
import { CombinedVueInstance, ExtendedVue, VueConstructor } from "vue/types/vue";
import {
  ComponentOptions,
  ThisTypedComponentOptionsWithArrayProps,
  ThisTypedComponentOptionsWithRecordProps } from "vue/types/options";
import { ComponentOptionsMixin, default as Vue } from "vue";
import { Module as VuexModule, Store as VuexStore, StoreOptions as VuexStoreOptions } from "vuex";


export = V
export as namespace V

declare namespace V {
  type ComponentProps<T> = T extends ExtendedVue<
    any, any, any, any, infer Props, any>
    ? Props
    : any;

  /** @brief small utility to test for "any" */
  type IfAny<T, Y, N> = 0 extends (1 & T) ? Y : N;
  type IfIsExact<T, R, Y, N> = T extends R ? R extends T ? Y : N : N;

  type Instance<T, VueType extends Vue = Vue> = T extends ExtendedVue<
    VueType, infer Data, infer Methods, infer Computed, infer Props,
    infer SetupBindings, infer Mixin, infer Extends>
    ?
    CombinedVueInstance<
      VueType & IfAny<Extends, VueType, Instance<Extends>>,
      IfAny<Data, NonNullable<unknown>, Data>,
      IfAny<Methods, NonNullable<unknown>, Methods>,
      IfAny<Computed, NonNullable<unknown>, Computed>,
      IfAny<Props, NonNullable<unknown>, Props>,
      IfAny<SetupBindings, NonNullable<unknown>, SetupBindings>,
      IfAny<Mixin, NonNullable<unknown>, Mixin>,
      IfAny<Extends, NonNullable<unknown>, Extends>
    > : VueType

  /**
   * @details used with Constructor<> to declare advanced types
   * replaces VueType in Instance
   * ex: Instance<typeof component, ExtVue<Opts, Refs>>
   */
  interface ExtVue<Opts, Refs> extends Vue {
    $options: ComponentOptions<Vue> & IfAny<Opts, ComponentOptions<Vue>, Opts>;
    $refs: { [key: string]: Vue | Element | Vue[] | Element[] } & IfAny<Refs, object, Refs>;
  }

  /**
   * @details used to invert extraction of computed fields
   */
  type ConstructComputedReturns<T> = {
    [key in keyof T]: () => T[key]
  }

  export function mixinMaker<
    V extends Vue,
    Data = NonNullable<unknown>,
    Methods = NonNullable<unknown>,
    Computed = NonNullable<unknown>,
    Props = NonNullable<unknown>,
    SetupBindings = NonNullable<unknown>,
    Mixin extends ComponentOptionsMixin = NonNullable<unknown>,
    Extends extends ComponentOptionsMixin = ComponentOptionsMixin>(
    mixin: ThisTypedComponentOptionsWithRecordProps<
      IfIsExact<Extends, ComponentOptionsMixin, V, Instance<Extends>> & V,
      Data,
      Methods,
      Computed,
      Props,
      SetupBindings,
      Mixin,
      Extends>):
    IfIsExact<Data & Methods & Computed & Mixin, NonNullable<unknown>,
      { data(): IfIsExact<Extends, ComponentOptionsMixin, object, ReturnType<Extends["data"]>> }, {
        data(): IfIsExact<Extends, ComponentOptionsMixin, Data, Data & ReturnType<Extends["data"]>>,
        methods: Methods,
        computed: ConstructComputedReturns<Computed>,
        mixins: [ Mixin ],
      }> & IfIsExact<Extends, ComponentOptionsMixin, object, Omit<Extends, "data">>

  /**
   * @details used to declare advanced constructors
   * Opts contains additional properties typings for the object
   * Refs contains a map of $refs types
   */
  interface Constructor<Opts, Refs, V extends Vue = ExtVue<Opts, Refs>> extends VueConstructor<ExtVue<Opts, Refs>> {
    /**
     * extend with object props
     */
    extend<
      Data = unknown, Methods = unknown, Computed = unknown, Props = unknown,
      SetupBindings = unknown,
      Mixin extends ComponentOptionsMixin = ComponentOptionsMixin,
      Extends extends ComponentOptionsMixin = ComponentOptionsMixin
    >(
      options?: ThisTypedComponentOptionsWithRecordProps<
        IfIsExact<Extends, ComponentOptionsMixin, V, Instance<Extends>> & V,
        Data, Methods, Computed, Props, SetupBindings, Mixin, Extends
      >
    ): ExtendedVue<
      IfIsExact<Extends, ComponentOptionsMixin, V, Instance<Extends>> & V,
      Data, Methods, Computed, Props, SetupBindings, Mixin, Extends
    >

    /**
     * extend with array props is not supported
     * NOTE: This overload of  'extend' method breaks typecheking when extending
     *  a component. Do NOT use array of props, rather list props as an object
     *  (see https://v2.vuejs.org/v2/guide/components-props#Prop-Types)
     */
  }

  interface Computed<Key, Type> { [Key]: Type }

  interface Shape<K> extends K {
    [any]: any
  }

  type StoreOptions<T> = VuexStoreOptions<T>;
  type Module<T, R=any> = VuexModule<T, R>;
  type Store<S> = VuexStore<S>;
  type ActionContext<S> = VuexActionContext<S>
}
