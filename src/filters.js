// @ts-check

import { assign, isFinite, isInteger, isNaN, isNil, isNumber, isString,
  pickBy, toString } from "lodash";
import { DateTime, Duration } from "luxon";
import { getDateTime } from "./utils";

/**
 * @param {string | number} bytes
 * @param {number} decimals
 * @returns {string}
 */
export function byteSize(bytes, decimals = 2) {
  // if bytes is a string that not represent a number, return as it is.
  /** @type {number} */
  let bytesFloat;
  if (isNumber(bytes)) {
    bytesFloat = bytes;
  }
  else {
    bytesFloat = parseFloat(bytes);
  }

  if (isNaN(bytesFloat) && isString(bytes)) {
    return bytes;
  }
  else if (isNaN(bytesFloat) || !isFinite(bytesFloat)) {
    return "#NaN";
  }
  else if (bytesFloat === 0) {
    return "0 Bytes";
  }
  const k = 1024; // Using by default Kibibyte
  const sizes = [ "Bytes", "KiB", "MiB", "GiB", "TiB", "PiB", "EiB", "ZiB", "YiB", "BiB" ]; // IEC notation

  const i = Math.floor(Math.log(bytesFloat) / Math.log(k));
  return parseFloat((bytesFloat / Math.pow(k, i)).toFixed(decimals)) + " " + sizes[i];
}

/**
 * @param {number} timestamp
 * @param {string} [format]
 * @param {luxon.DateTimeOptions} [options]
 * @returns {string|null}
 * @deprecated
 */
export function momentFilter(timestamp, format, options) {
  if (!Number.isFinite(timestamp)) {
    return "";
  }
  else if (isNil(format)) {
    return getDateTime(timestamp, options).set({ millisecond: 0 })
    .toISO({ suppressMilliseconds: true });
  }
  else {
    return getDateTime(timestamp, options).toFormat(format);
  }
}

/**
 * @details diplay the provided ms or seconds timestamp as a human readable date
 * @param {number} timestamp
 * @param {string} [format]
 * @param {luxon.DateTimeOptions} [options]
 * @return {string}
 */
export function dateTimeFilter(timestamp, format, options) {
  if (!Number.isFinite(timestamp)) {
    return "";
  }
  else if (isNil(format)) {
    return getDateTime(timestamp, options)
    .toLocaleString(DateTime.DATETIME_SHORT_WITH_SECONDS);
  }
  else {
    return getDateTime(timestamp, options).toFormat(format);
  }
}

/**
 * @param {number} timestamp
 * @return {string}
 */
export function relativeDateTimeFilter(timestamp) {
  if (!Number.isFinite(timestamp)) {
    return "";
  }
  return getDateTime(timestamp).toRelative() || "";
}

/**
 * @param  {number} value
 * @param  {string} suffix
 * @param  {{ precision?: number, expLimit?: number|false, fixed: boolean }} [options]
 * @return {string}
 * @details
 *  - expLimit: value at which to switch to exponent notation (default: 10000)
 *  - precision: number of decimals (default: 3)
 *  - fixed: fixed number of decimals (precision required)
 *
 */
// eslint-disable-next-line complexity
function bigNumber(value, suffix = "", options = undefined) {
  if (isNil(value) || isNaN(value)) {
    return "-";
  }
  if (isString(value)) {
    value = Number.parseFloat(value);
  }
  const abs = Math.abs(value);
  const precision = options?.precision ?? 3;
  const expLimit = options?.expLimit ?? 10000;
  const fixed = options?.fixed ?? false;
  if (fixed) {
    return value.toFixed(precision) + suffix;
  }
  else if (expLimit === false || (abs <= expLimit && abs >= 1)) {
    return toString(Number.isInteger(value) ?
      value : Number.parseFloat(value.toFixed(precision))) + suffix;
  }
  return (value === 0) ? "0" : (value.toExponential(precision) + suffix);
}

/**
 * @param {number} ms
 * @param {luxon.DurationUnit[]} [units]
 * @param {Partial<luxon.DurationOptions & luxon.ToHumanDurationOptions> &
 *  { hideNulls: boolean }} [options]
 * @return {string}
 * @details
 *  - hideNulls: hide null units in the returned string if true (default: true)
 */
export function durationFilter(ms, units = [ "milliseconds" ], options) {
  if (!isInteger(ms) || ms < 0) { return ""; }

  const opts = assign({ hideNulls: true }, options);

  const values = Duration.fromMillis(ms, opts).shiftTo(...units).toObject();
  return Duration.fromObject(
    opts.hideNulls ? pickBy(values, (v) => (v > 0)) : values)
  .toHuman(opts);
}

export default {
  /**
   * @param {Vue.VueConstructor<Vue>} Vue
   */
  install(Vue) {
    Vue.filter("b-byteSize", byteSize);
    Vue.filter("b-moment", momentFilter);
    Vue.filter("b-luxon", momentFilter);
    Vue.filter("b-dateTime", dateTimeFilter);
    Vue.filter("b-duration", durationFilter);
    Vue.filter("b-relativeDateTime", relativeDateTimeFilter);
    Vue.filter("b-bigNumber", bigNumber);
  }
};
