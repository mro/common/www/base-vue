import { default as Vue, ComponentOptionsMixin } from 'vue';
import { Store as VuexStore, Plugin as VuexPlugin } from 'vuex';
import Router from 'vue-router';
import { ExtendedVue } from 'vue/types/vue';
import { PluginObject } from 'vue/types/plugin';

import { Selection, BaseType } from 'd3-selection';
import MarkdownIt from 'markdown-it';
import q from "q";
import EventEmitter from 'events';


export as namespace BaseVue;

declare module 'vue/types/vue' {
  interface Vue {
    $butils: typeof butils
  }
}

declare module '*.yaml' {
    const data: any;
    export default data;
}

declare module '*.yml' {
    const data: any;
    export default data;
}

export interface BaseVueOptions {
    auth?: boolean
}

declare const BaseVue: PluginObject<BaseVueOptions>;

export function install(vue: Vue, options?: BaseVueOptions): void;

declare namespace Methods {
    export function hasSlot(name: string): boolean;
}

export const BaseAnimationGroup: ExtendedVue<Vue, {}, {
    onEnter(elt: Vue.VNode | Vue | Element, done: Function): void;
    onLeave(elt: Vue.VNode | Vue | Element, done: Function): void;
}, {
    animation: {
        in?: Function | undefined;
        out?: Function | undefined;
    };
    options: {
        appear: boolean;
    };
}, {
    tag: string;
    appear: boolean;
    anim: string;
}, {}>;

export const BaseAnimationBlock: ExtendedVue<Vue, {}, {
    onEnter(elt: Vue.VNode | Vue | Element, done: Function): void;
    onLeave(elt: Vue.VNode | Vue | Element, done: Function): void;
}, {
    animation: {
        in?: Function | undefined;
        out?: Function | undefined;
    };
    animOptions: any;
}, {
    appear: boolean;
    anim: string;
    mode: string;
    options: any;
}, {}>;

export const BaseAnimationLoader: ExtendedVue<Vue, {}, {
    onEnter(elt: Vue.VNode | Element | undefined, done: Function): void;
    onLeave(elt: Vue.VNode | Element | undefined, done: Function): void;
}, {
    animation: {
        in?: Function | undefined;
        out?: Function | undefined;
    };
}, {
    loading: boolean;
    anim: string;
}, {}>;

export interface BaseAnimationOptions {
    name?: string;
    duration?: number;
}

export function BaseAnimationFunction(elt: Vue|Vue.VNode|Element, options?: BaseAnimationOptions|null|Function, done?: Function): void

export const BaseAnimation: {
    default: string;
    animationSpeed: number;
    setOpacity(elt: Element, opacity: number): void;
    getDom(elt: Vue|Vue.VNode|Element): Element;
    rot90: { in: typeof BaseAnimationFunction, out: typeof BaseAnimationFunction };
    flip: { in: typeof BaseAnimationFunction, out: typeof BaseAnimationFunction };
    fade: { in: typeof BaseAnimationFunction, out: typeof BaseAnimationFunction };
    pop: { in: typeof BaseAnimationFunction, out: typeof BaseAnimationFunction };
    height: { in: typeof BaseAnimationFunction, out: typeof BaseAnimationFunction };
    width: { in: typeof BaseAnimationFunction, out: typeof BaseAnimationFunction };
    delay: { in: typeof BaseAnimationFunction, out: typeof BaseAnimationFunction };
};

export const BaseCard: ExtendedVue<Vue, {}, {
    hasSlot: typeof Methods.hasSlot;
}, {}, {
    title: string;
}, {}>;

export const BaseCollapsible: ExtendedVue<Vue, {
    isExpanded: boolean;
}, {
    toggle(): void;
}, {}, {
    title: string;
    expand: boolean;
}, {}>;

export const BaseExpiryIcon: ExtendedVue<Vue, {
    start: number;
    selection?: Selection<BaseType, BaseType, HTMLElement, any> | undefined;
}, {
    restart(): void;
    update(): void;
}, {
    tooltip: string;
}, {
    expiry: number;
    color: string;
}, {}>;

export const BaseKeyboardEventMixinOptions: {
    local?: boolean | undefined;
    checkOnInputs?: boolean | undefined;
};

export function BaseKeyboardEventMixin(options: typeof BaseKeyboardEventMixinOptions): {
    methods: {
        onKey(key: string | string[], cb: Function): void;
        onKeyEmit(key: string, signal?: string | null): void;
        removeKey(key: string, cb?: Function | null): void;
        setCheckOnInput(value: boolean): void;
    }
};

export const BaseGlobalKeyboardEvent: ExtendedVue<Vue, {}, {}, {}, {
    inputCheck: boolean;
}, {}>;


export const BaseChangeReloadMixinOptions: {
    reloadInterval?: number,
};

export function BaseChangeReloadMixin(options: typeof BaseChangeReloadMixinOptions): ComponentOptionsMixin;

export const BaseImage: ExtendedVue<Vue, {
    error: boolean;
    loading: boolean;
}, {}, {}, {
    src: string;
    href: string;
}, {}>;

export const CodeMirrorEditor: ExtendedVue<Vue, {
    content: string;
    codemirror: CodeMirror.EditorFromTextArea | null;
    cminstance: CodeMirror.EditorFromTextArea | null;
    cmOptions: any;
}, {
    initialize(): void;
    refresh(): void;
    focus(): void;
    get(): CodeMirror.Editor | null;
    handlerCodeChange(newVal: string): void;
    getForwardSelection(): CodeMirror.Range | undefined;
    heading(): void;
    bold(): void;
    italic(): void;
    strikethrough(): void;
    quote(): void;
    code(): void;
    link(): void;
    list(): void;
    taskList(): void;
    table(): void;
    numberedList(): void;
    indent(): void;
    outdent(): void;
    indentLines(mode: "add" | "subtract"): void;
    undo(): void;
    redo(): void;
    wrapSelectionWith(elementStart: string, elementEnd?: (string | null) | undefined): void;
    makeListWith(elementList: string, withSpace?: boolean): void;
}, {
    showMarkdownTools: boolean;
}, {
    value: string;
    name: string;
    placeholder: string;
    markdownTools: boolean;
    options: any;
    events: unknown[];
}, {}>;

export const BaseMarkdownViewer: ExtendedVue<Vue, {
    mdOptions: MarkdownIt.Options;
    mdinstance: MarkdownIt;
}, {}, {}, {
    value: string;
    options: any;
}, {}>;

export const BaseRibbon: ExtendedVue<Vue, {
    positionClass: string;
}, {}, {}, {
    value: string;
    position: string;
}, {}>;

export const BaseMarkdownWidget: ExtendedVue<Vue, {
    editValue: string | null;
    showPreview: boolean;
    cmOptions: any;
    mdOptions: any;
}, {
    togglePreview(): void;
    refreshEditor(): void;
}, {}, {
    value: string;
    inEdit: boolean;
}, {}>;

export const BaseSSVG: ExtendedVue<Vue, {
    frameLoaded: boolean;
}, {
    release(): void;
    reload(): void;
    updateState(state: any): void;
    getState(): any;
}, {
    hasFrame: boolean
}, {
    iframe: boolean;
    src: string|null;
    state: any;
    preserveAspectRatio: string|null;
}, {}>;

export const BaseEventSource: ExtendedVue<Vue, {
    message: any;
}, {
    release(): void;
    connect(): void;
}, {}, {
    src: string,
    json: boolean
}, {}>;

export const BaseModalLoader: ExtendedVue<Vue, {}, {}, {}, {
    isLoading: boolean;
}, {}>;

export const BaseModal: ExtendedVue<Vue, any, {
    show(value: boolean): void;
    toggle(): void;
}, any, {
    title: string;
    visible: boolean;
}, {}>

export const BaseDialog: ExtendedVue<Vue, {
    text: string
}, {
    resolve<T>(value: T): void;
    reject(err: Error|any): void;
    request<T=any>(): Promise<T>;
}, any, {
    title: string;
}, {}>

export const BaseKeepFocusMixin: {
    methods: {
        keepFocusOnNextTick(ref?: string, bubble?: boolean): void;
        setFocusOnNextTick(ref?: string, bubble?: boolean): void;
        saveFocusState(ref: string): void;
        restoreFocusState(ref: string): void;
    }
}

export const BaseFocusInOutMixin: {
    data(): { hasFocus: boolean },
    methods: {
        setHasFocus(value: boolean): void;
    }
}


export const BaseHasSlotMixin: {
    methods: {
        hasSlot(name: string): boolean;
    }
}

export const BaseErrorHandlerMixin: {
    methods: {
        onError(error: any): void;
    }
}

export const TempusDominusDatePicker: ExtendedVue<Vue, {
    id: string;
    dp: any | null;
    elem: JQuery<HTMLElement> | null;
}, {
    updateDateValue(timestamp: number | null): void;
    onChange(event: any): void;
    registerEvents(): void;
}, {}, {
    value: number;
    config: any;
}, {}>;

export const BaseParamBase: ExtendedVue<Vue, {
    warnings: any;
    errors: any;
}, {
    checkValidity(): true;
    addError(id: string, message: string): void;
    removeError(id: string): void;
    addWarning(id: string, message: string): void;
    removeWarning(id: string): void;
}, {
    paramType: string[];
    hasErrors: boolean;
}, {
    title: string;
    isPrimary: boolean;
    value: string;
    showErrors: boolean;
}, {}>;

export const BaseParamDate: ExtendedVue<Vue, {
    editTimestamp: number | null;
}, {
    onChange(timestamp: number): void;
    checkValidity(): Promise<boolean>;
    addError(id: string, message: string): void;
    removeError(id: string): void;
    addWarning(id: string, message: string): void;
    removeWarning(id: string): void;
}, {
    formattedTimestamp: string;
}, {
    title: string;
    timestamp: number;
    inEdit: boolean;
}, {}>;

export const BaseInputDate: ExtendedVue<Vue, {
    editTimestamp: number | null;
}, {}, {}, {
    timestamp: number;
    inEdit: boolean;
}, {}>;

export const BaseParamInput: ExtendedVue<Vue, {
    editValue: string | null;
    id: string;
}, {
    checkValidity(): Promise<boolean>;
    addError(id: string, message: string): void;
    removeError(id: string): void;
    addWarning(id: string, message: string): void;
    removeWarning(id: string): void;
}, {}, {
    title: string;
    value: string | number;
    inEdit: boolean;
}, {}>;

export const BaseInput: ExtendedVue<Vue, {
    editValue: string | null;
    hasFocus: boolean
}, {
    checkValidity(): true;
    addError(id: string, message: string): void;
    removeError(id: string): void;
    addWarning(id: string, message: string): void;
    removeWarning(id: string): void;
    setHasFocus(value: boolean): void;
}, {}, {
    value: string | number;
    inEdit: boolean;
}, {}>;

export const BaseInputReadOnly: ExtendedVue<Vue, {
    editValue: string | null;
}, {}, {}, {
    value: string | number;
    inEdit: boolean;
}, {}>;

export const BaseParamFile: ExtendedVue<Vue, {
    editValue: File;
    id: string;
}, {
    checkValidity(): Promise<boolean>;
    addError(id: string, message: string): void;
    removeError(id: string): void;
    addWarning(id: string, message: string): void;
    removeWarning(id: string): void;
}, any, {
    title: string;
    value: string;
    inEdit: boolean;
}, {}>;

export const BaseInputFile: ExtendedVue<Vue, {
    editValue: string | null;
}, {}, {}, {
    value: string | number;
    inEdit: boolean;
}, {}>;

export const BaseParamList: ExtendedVue<Vue, {
    editValue?: string | number | null | undefined;
    id: string;
    domOpts: {
        value: any;
        text: any;
    }[] | null;
}, {
    genDomOpts(): void;
    checkValidity(): q.Promise<boolean>;
    addError(id: string, message: string): void;
    removeError(id: string): void;
    addWarning(id: string, message: string): void;
    removeWarning(id: string): void;
}, {
    valueText: string;
    editValueText: string;
}, {
    title: string;
    value: string | number;
    options: unknown[];
    inEdit: boolean;
}, {}>;

export const BaseSelect: ExtendedVue<Vue, {
    editValue: string | null;
}, {}, {}, {
    value: string | number;
    inEdit: boolean;
}, {}>;

export const BaseParamReadonly: ExtendedVue<Vue, {
    editValue?: string | null | undefined;
    id: string;
}, {
    filterAttrs(): Record<string, string>;
    checkValidity(): Promise<boolean>;
    addError(id: string, message: string): void;
    removeError(id: string): void;
    addWarning(id: string, message: string): void;
    removeWarning(id: string): void;
}, {}, {
    title: string;
    value: string | number;
    inEdit: boolean;
}, {}>;


export const BaseLED: ExtendedVue<Vue, {}, {}, {
    actualValue: string;
}, {
    blink: boolean;
    label: string | undefined;
    value: string | boolean;
}, {}>;

export const BaseParamLED: ExtendedVue<Vue, {}, {}, {
    actualValue: string;
}, {
    title: string;
    blink: boolean;
    label: string | undefined;
    value: string | boolean;
}, {}>;

export const BaseToggle: ExtendedVue<Vue, {
    editValue: boolean | null;
    id: string;
    hasEditRequest: boolean;
}, {
    toggle(): void;
}, {}, {}, {
    value: boolean;
    label: string;
    disabled: boolean;
    inEdit: boolean;
}, {}>;

export const BaseRadio: ExtendedVue<Vue, {
    editValue: boolean | null;
    id: string;
    hasEditRequest: boolean;
}, {
    toggle(): void;
}, {}, {}, {
    name: string;
    value: boolean;
    label: string;
    disabled: boolean;
    inEdit: boolean;
}, {}>;

export const BaseParamToggle: ExtendedVue<Vue, {
    editValue: boolean | null;
    id: string;
}, {
    onEdit(newValue: boolean): void;
    checkValidity(): Promise<boolean>;
    addError(id: string, message: string): void;
    removeError(id: string): void;
    addWarning(id: string, message: string): void;
    removeWarning(id: string): void;
}, {}, {
    title: string;
    value: boolean;
    inEdit: boolean;
}, {}>;

export class BaseMatchMedia {
    constructor(query: string, cb: (match: boolean) => any);
    close(): void;
}
export namespace BaseMatchMedia {
    const SM: string;
    const MD: string;
    const LG: string;
    const XL: string;
    const XXL: string;
}

declare class BaseLoggerClass extends EventEmitter {
    error(error: any): void;
    warning(warning: any): void;
    getErrors(): Array<BaseLogger.Error>;
    clear(): void;
}
export const BaseLogger: BaseLoggerClass;

export namespace BaseLogger {
    interface Error {
    id: string,
    type: 'error' | 'warning',
    message: string,
    error: any,
    timestamp: number
    }
}

export const BaseErrorReport: ExtendedVue<Vue, {
    errors: BaseLogger.Error[];
}, {
    toggle(): void;
}, {
    showKeyHints: boolean;
}, {}, {}>;

export const BaseErrorAlert: ExtendedVue<Vue, {
    errors: BaseLogger.Error[];
}, {}, {}, {
    timeout: number;
    maxErrors: number;
}, {}>;

export const BaseNavBar: ExtendedVue<Vue,
    { errorsExist: boolean },
    {
    setErrorExist(): void;
    toggleErrors(event?: Event): void;
    openKeyboardShortcutsModal(event?: Event): void;
    toggleKeyboardShortcuts(event?: Event): void;
    }, {
    isAdmin: boolean;
    showKeyHints: boolean;
    navRoutes: any[];
    username: string;
    }, {
    title: string;
    version: string;
    showRoutes: boolean;
    }, {}>;

export const BaseNavBarItem: ExtendedVue<Vue, {}, {},
    { active: boolean; },
    { path: string; isDisabled: boolean }, {}>;

export const BaseSideBar: ExtendedVue<Vue,
    { absolute: boolean, hidden: boolean },
    {
    toggle(): void;
    }, {
    isAdmin: boolean;
    navRoutes: any[];
    }, {
    showRoutes: boolean;
    }, {}>;

export const BaseSideBarItem: ExtendedVue<Vue, {}, {},
    { active: boolean; },
    {
    path: string;
    isActive: boolean;
    isDisabled: boolean;
    }, {}>;

export const BaseLineChart : ExtendedVue<Vue,
    {},
    {
    add(value: any|any[]): void,
    redraw(): void,
    render(): void,

    }, {},
    {
    data: any[],
    xAxis: string, yAxis: string|string[]|{[name: string]: string},
    xAxisLabel: string, yAxisLabel: string, maxData: number, smooth: boolean,
    isTimestamp: boolean
    }, {}>;

export const BaseWebSite: ExtendedVue<Vue, {}, {},
    { hasRouter: boolean },
    { title: string, version: string }, {}>;

export const BaseWebApp: ExtendedVue<Vue, {}, {},
    { hasRouter: boolean, showKeyHints: boolean },
    { title: string, version: string }, {}>;

export interface UiState {
    showKeyHints: boolean,
    darkMode: boolean,
    muteAlerts: boolean
}

export interface RouteState {
    path: string,
    params: { [name: string]: string },
    query: { [name: string]: string }
}

export interface StoreState {
    user: { username: string, [key: string]: any } | null,
    route: RouteState|null,
    ui: UiState
}

export function createStore(router?: Router): VuexStore<StoreState>;
export function getStore(): VuexStore<StoreState>|undefined;
export function destroyStore(): void;
export interface UserSource {
    setStore(store: VuexStore<StoreState>): void;
    fetch(): void;
    store: VuexStore<StoreState>;
}

export class BaseIntervalSource {
    constructor(store: VuexStore<StoreState>, namespace?: string, interval?: number)
    start(interval?: number): void
    stop(): void
    destroy(): void
    tick(): void

    static register(store: VuexStore<StoreState>, namespace: string, interval?: number): void
    static plugin(namespace: string, interval?: number): VuexPlugin<any>
}
export interface BaseIntervalState {
    count: number
}

export const sources: { user?: BaseVue.UserSource };
export const storeOptions: {
    state: { user: any },
    getters: { username: any, canEdit: any, canAdmin: any },
    mutations: { user: any },
    modules: { ui: any },
    plugins: any
};
export interface StoreSources {
    user?: BaseVue.UserSource,
    // [key: string]: any
}

export const store: VuexStore<StoreState>;

export const butils: {
    isEmpty(value: any): boolean;
    isNil(value: any): boolean;
    currentUrl(): string;
    setCurrentUrl(path: string): void;
    publicPath(path: string): string,
    format(source: string, repl: any, keep?: boolean): string;
    /* handles seconds or milliseconds timestamp */
    getDateTime(timestamp: number, options?: { zone?: string }): luxon.DateTime;
}

export default BaseVue;

/* extend vuex Store with our sources */
declare module "vuex" {
  interface Store<S> {
    sources: BaseVue.StoreSources
  }
}
