// @ts-check

/* properly load bootstrap and jquery */
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import * as bootstrap from "bootstrap"; // jshint ignore:line
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import $ from "jquery"; // jshint ignore:line

/* eslint-disable-next-line @typescript-eslint/no-unused-vars */ // @ts-ignore
import(/* webpackChunkName: "base" */ "./scss/_base.scss"); // jshint ignore:line
import filters from "./filters";
import utils from "./utils";
import { get } from "lodash";
import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

// Mixins
export { default as BaseKeyboardEventMixin, _globalListener } from "./mixins/BaseKeyboardEventMixin";
export { default as BaseKeepFocusMixin } from "./mixins/BaseKeepFocusMixin";
export { default as BaseHasSlotMixin } from "./mixins/BaseHasSlotMixin";
export { default as BaseHasErrorHandlerMixin } from "./mixins/BaseErrorHandlerMixin";
export { default as BaseChangeReloadMixin } from "./mixins/BaseChangeReloadMixin";

// Plugins
export { default as CodeMirrorEditor } from "./plugins/CodeMirrorEditor.vue";
export { default as MarkdownViewer } from "./plugins/MarkdownViewer.vue";
export { default as TempusDominusDatePicker } from "./plugins/TempusDominusDatePicker.vue";

// Base
import * as components from "./components";
export * from "./components";
export { default as BaseAnimation } from "./BaseAnimation/BaseAnimation";
export { default as BaseLogger } from "./Logger/BaseLogger";
export { default as BaseMatchMedia } from "./BaseMatchMedia";

export { createStore, sources, storeOptions, getStore, destroyStore } from "./store";
import { getStore, sources } from "./store";
import UserSource from "./store/sources/UserSource";
export { default as BaseIntervalSource } from "./store/sources/BaseIntervalSource";

export { utils as butils } from "./utils";

const BaseVue = {
  /**
   * @param {Vue.VueConstructor<Vue>} Vue
   * @param {{ auth?: boolean }} [options]
   */
  install(Vue, options) {
    // eslint-disable-next-line guard-for-in
    for (const prop in components) {
      /** @type {string|undefined} */ // @ts-ignore
      const name = components[prop].options.name;
      if (name) {
        // @ts-ignore
        Vue.component(name, components[prop]);
      }
      else {
        console.warn("[base-vue]: component name not set on: ", prop);
      }
    }

    filters.install(Vue);
    utils.install(Vue);

    if (get(options, "auth", false)) {
      sources.user = new UserSource(getStore());
    }
  }
};

export default BaseVue;
