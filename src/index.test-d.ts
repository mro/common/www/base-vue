import { expectType } from "tsd";
import { BaseCard, BaseErrorAlert, BaseErrorReport, BaseEventSource,
  BaseExpiryIcon, BaseInput, BaseLogger, BaseNavBar, BaseParamInput, BaseSSVG,
  BaseWebSite, CodeMirrorEditor } from ".";

(async function() {

  const card = new BaseCard();
  expectType<string>(card.title);
  expectType<string>(card.$butils.format("test{toto}", { toto: 42 }));

  const paramInput = new BaseParamInput();
  expectType<string | null>(paramInput.editValue);

  const Input = new BaseInput();
  expectType<string | null>(Input.editValue);

  const expiryIcon = new BaseExpiryIcon();
  expectType<number>(expiryIcon.start);

  const cmEditor = new CodeMirrorEditor();
  expectType<string>(cmEditor.content);
  expectType<boolean>(cmEditor.showMarkdownTools);

  BaseLogger.error(new Error("test"));
  BaseLogger.warning("test");
  BaseLogger.clear();

  const errorAlert = new BaseErrorAlert();
  expectType<Array<BaseLogger.Error>>(errorAlert.errors);

  const errorReport = new BaseErrorReport();
  expectType<Array<BaseLogger.Error>>(errorReport.errors);
  errorReport.toggle();

  const navBar = new BaseNavBar();
  expectType<string>(navBar.title);
  navBar.toggleErrors();

  const webSite = new BaseWebSite();
  expectType<string>(webSite.title);

  const ssvg = new BaseSSVG();
  ssvg.updateState({ test: 42 });

  const ev = new BaseEventSource({ propsData: { src: "/sse" } });
  ev.release();
}());
