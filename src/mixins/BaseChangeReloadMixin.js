// @ts-check
import { forOwn, isNil } from "lodash";
import { mixinMaker } from "../utils";

const RELOAD_INTERVAL = 15 * 60 * 1000; // Check every 15 mins if the page should be reloaded


/**
 * @typedef {{
 *   reloadInterval?: number,
 * }} BaseChangeReloadMixinOptions
 */

const BaseChangeReloadMixin = (/** @type {BaseChangeReloadMixinOptions}*/options) => mixinMaker({
  /**
  Supported options:
  {
    reloadInterval: number (def 900 000 (15min)) // The delay between two check requests
  }
  */

  /**
   * @returns {{
  *    intervalHandler: NodeJS.Timeout | undefined,
  *    initialETag: string | undefined,
  *    initialDate: Date | undefined,
  *    checkETag: boolean
  * }}
   */
  data() {
    return {
      intervalHandler: undefined,
      initialETag: undefined,
      initialDate: undefined,
      checkETag: true // if false, fallback on check Date
    };
  },
  async mounted() {
    try {
      // Trigger reload check only if the file is hosted on a server
      if (!window.location.href.includes("http")) {
        console.warn("Website is not hosted on a server. Reload on source change disabled.");
        return;
      }

      // Check for supported reload method (ETage / Date)
      const response = await this.fetch(window.location.href, { method: "HEAD" });

      // Empty => not supported (no comparison availlable)
      this.initialETag = response.getResponseHeader("ETag");

      // Empty => not supported (will always return current date)
      this.initialDate = response.getResponseHeader("Last-Modified");

      // If no server method availlable, cancel the reloading check
      if (isNil(this.initialETag) && isNil(this.initialDate)) {
        console.warn("Neither 'ETag' or 'Last-Modified' methods are supported by the server. Reload on source change disabled.");
        return;
      }

      // If Etag is not supported by the server, fallback on Date check
      if (isNil(this.initialETag)) {
        console.info("'ETag not supported by the server, fallback on Date check");
        this.checkETag = false;
      }

      // Initialize the source reload loop
      this.intervalHandler = setInterval(
        async () => { await this.checkSource(); },
        options.reloadInterval ?? RELOAD_INTERVAL);
    }
    catch (error) {
      console.error(error);
    }
  },
  beforeDestroy() {
    clearInterval(this.intervalHandler);
  },
  methods: {
    /**
     * Custom fetch to prevent the ETag issue on firfox
     * @param {string} url
     * @param {{ method: string, headers?: object }} config
     * @returns {Promise<any>} request promise
     */
    fetch(url, config) {
      return new Promise(function(resolve, reject) {
        const xhr = new XMLHttpRequest();
        xhr.open(config.method, url);

        forOwn(config.headers,
          (value, name) => xhr.setRequestHeader(name, value));
        xhr.onload = () => resolve(xhr);
        xhr.onerror = (error) => reject(error);

        xhr.send();
      });
    },
    /**
     * @returns {Promise<void>}
     */
    async checkSource() {
      const STATUS_OK = 200;

      const headers = {};

      if (this.checkETag) {
        // Compare with the ETag fetched at page load
        headers["If-None-Match"] = this.initialETag;
      }
      else {
        // Compare with last modified date stored at the beginning
        headers["If-Modified-Since"] = this.initialDate;
      }

      // Retrieve information from the sources of the page to see if it should be reloaded
      const response = await this.fetch(window.location.href, { method: "HEAD", headers });

      // Check if the document was modified since load
      if (response.status === STATUS_OK) {

        // To avoid inconsistencies between browsers (return OK but different ETag)
        if (this.checkETag && this.initialETag === response.getResponseHeader("ETag")) {
          return;
        }

        window.location.reload(); // We refresh the page
      }
    }
  }
});

export default BaseChangeReloadMixin;
