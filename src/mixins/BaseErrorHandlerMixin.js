// @ts-check
import { has } from "lodash";

import logger from "../Logger/BaseLogger";
import { mixinMaker } from "../utils";

const component = mixinMaker({
  methods: {
    /**
     * @param {Error|any} error
     */
    onError(error) {
      if (has(this, [ "$listeners", "error" ])) {
        this.$emit("error", error);
      }
      else {
        logger.error(error);
      }
    }
  }
});
export default component;
