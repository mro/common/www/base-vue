// @ts-check

import { mixinMaker } from "../utils";

const component = mixinMaker({
  /**
   * @returns {{ hasFocus: boolean }}
   */
  data() {
    return { hasFocus: false };
  },
  methods: {
    /**
     * @param {boolean} value
     */
    setHasFocus(value) {
      this.hasFocus = value;
    }
  }
});
export default component;
