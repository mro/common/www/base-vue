// @ts-check

import { mixinMaker } from "../utils";

const component = mixinMaker({
  methods: {
    /**
     * @param {string} name
     * @param {boolean} [scoped=true] check for scoped slots
     * @return {boolean}
     */
    hasSlot: function(name, scoped = true) {
      return !!this.$slots[name] || (scoped && !!this.$scopedSlots[name]);
    }
  }
});
export default component;
