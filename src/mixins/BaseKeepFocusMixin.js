// @ts-check

import { mixinMaker } from "../utils";

/**
 * @param {Vue|Element} el
 * @returns {HTMLElement}
 */
function getDomElement(el) {
  // This will handle also the case when it's a Vue component
  // (for example in BaseParamDate)
  // @ts-ignore this check is correct.
  return (el && el.$el) ? el.$el : el;
}

const options = {
  _hasFocus: false
};

const component = mixinMaker({
  ...options,
  methods: {
    /**
     * @param  {string|null} [ref]
     */
    focusBubble(ref) {
      for (let elt = /** @type {HTMLElement|null} */ (getDomElement(ref ? /** @type {Element} */(this.$refs[ref]) : this)); elt; elt = elt.parentElement) {
        elt.focus();
        if (elt === document.activeElement) { break; }
      }
    },
    /**
     * @param {string|null} [ref]
     * @param {boolean} [bubble]
     */
    keepFocusOnNextTick(ref, bubble = false) {
      if (getDomElement(ref ? /** @type {Element} */(this.$refs[ref]) : this) ===
          document.activeElement) {
        this.setFocusOnNextTick(ref, bubble);
      }
    },

    /**
     * @param {string|null} [ref]
     * @param {boolean} [bubble]
     */
    setFocusOnNextTick(ref, bubble = true) {
      this.$nextTick(() => {
        if (bubble) {
          this.focusBubble(ref ?? null);
        }
        else {
          const el = getDomElement(ref ? /** @type {Element} */(this.$refs[ref]) : this);
          if (el) {
            /** @type HTMLElement */(el).focus();
          }
        }
      });
    },
    /**
     * @param {string|null} [ref]
     */
    saveFocusState(ref) {
      /** @type {typeof options} */(this.$options)._hasFocus = ((ref ? this.$refs[ref] : this) ===
        document.activeElement);
    },
    /**
     * @param {string|null} [ref]
     */
    restoreFocusState(ref) {
      if (/** @type {typeof options} */(this.$options)._hasFocus) {
        /** @type HTMLElement */(ref ? this.$refs[ref] : this.$el).focus();
      }
    }
  }
});
export default component;
