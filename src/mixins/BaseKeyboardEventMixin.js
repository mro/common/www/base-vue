/* eslint max-lines: ["error", 400] */
// @ts-check

import { bindAll, forEach, get, isArray, isEmpty, last, reduce, remove, set,
  split, union } from "lodash";

import { createStore } from "../store";
import { mixinMaker } from "../utils";

const KeyMap = {
  Enter: "enter",
  Escape: "esc",
  Shift: "shift",
  " ": "space",
  ArrowLeft: "left",
  ArrowRight: "right",
  ArrowUp: "up",
  ArrowDown: "down",
  Meta: "meta",
  Control: "ctrl",
  Alt: "alt",
  "+": "plus",
  "-": "minus"
};

const Mod = {
  ctrl: 0x01,
  alt: 0x02,
  shift: 0x04,
  meta: 0x08
};

/**
 * @param {Event?} event
 * @returns {boolean}
 */
function isInput(event) {
  if (event && event.target) {
    const tagName = /** @type string */(get(event.target, "tagName", ""));
    if (tagName === "INPUT" || tagName === "SELECT" || tagName === "TEXTAREA") {
      return !get(event.target, "readOnly", false);
    }
    else {
      return get(event.target, "isContentEditable", false);
    }
  }
  return false;
}

/**
 * @typedef {{[key: string]: function}} ListenerCallbacks
 */

/** @type KeyboardEventHandler */
export let _globalListener;

export class KeyboardEventHandler {
  /**
   * @param {Element | null} target
   * @param {boolean} checkOnInputs
   */
  constructor(target = null, checkOnInputs/*: boolean */ = true) {
    /** @type ListenerCallbacks */
    this.listeners = {};
    /** @type Element | Document */
    this.target = target || window.document;
    /** @type boolean */
    this.checkOnInputs = checkOnInputs;
    /** @type boolean */
    this._keyupAttached = false;
    /** @type boolean */
    this._keydownAttached = false;
    bindAll(this, [ "onKeyEvent" ]);
  }

  /**
   * @return {void}
   */
  release() {
    this.target.removeEventListener("keyup", /** @type EventListener */(this.onKeyEvent));
    this.target.removeEventListener("keydown", /** @type EventListener */(this.onKeyEvent));
    this._keyupAttached = false;
    this._keydownAttached = false;
  }

  /**
   * @return {void}
   */
  releaseIfEmpty() {
    if (isEmpty(this.listeners)) {
      this.release();
    }
  }

  /**
   * @param {string} key
   * @returns {{keyCode: string, modMask: number, eventType: string}}
   */
  // Doesn't make much sense having it here but it's useful for unittest
  parseKey(key /*: string */) {
    // Example key: ctrl-shift-s-keyup
    // last keyup/keydown is optional. Default is keyup
    const parts = split(key, "-");
    let eType = "keyup";
    if (last(parts) === "keyup" || last(parts) === "keydown") {
      // @ts-ignore undefined can't happen
      eType = last(parts);
      // remove eventType from parts
      parts.pop();
    }
    // Extract the last part as key
    // @ts-ignore undefined can't happen
    key = parts.pop();
    // Transform mods in a mask
    return {
      keyCode: key,
      // @ts-ignore
      modMask: reduce(parts, (ret, m) => (Mod[m] | ret), 0),
      eventType: eType
    };
  }

  /**
   * @param {string} keyCode
   * @param {number} mod
   * @param {string} eType
   * @returns {string}
   */
  genKeyHash(keyCode/*: string */, mod /*: number */, eType /*: string */) {
    return `${keyCode}-${mod}-${eType}`;
  }

  /**
   * @param {boolean} value
   * @return {void}
   */
  setCheckOnInput(value /*: boolean */) {
    this.checkOnInputs = value;
  }

  /**
   * @param {string} key
   * @returns {any}
   */
  getCallBacks(key /*: string */) {
    // return empty array if there are no callbacks
    return get(this.listeners, key, []);
  }

  /**
   * @param {string} key
   * @param {function} cb
   */
  addCallBack(key/*: string */, cb /*: function */) {
    // Attach event listener if there is the first time
    const { keyCode, modMask, eventType } = this.parseKey(key);
    const keyHash = this.genKeyHash(keyCode, modMask, eventType);
    if (eventType === "keyup" && !this._keyupAttached) {
      this.target.addEventListener("keyup", /** @type EventListener */(this.onKeyEvent));
      this._keyupAttached = true;
    }
    else if (eventType === "keydown" && !this._keydownAttached) {
      this.target.addEventListener("keydown", /** @type EventListener */(this.onKeyEvent));
      this._keydownAttached = true;
    }

    // If the same cb already exist for the same keyHash, the union
    // function will not duplicate it.
    set(this.listeners, keyHash,
      union(this.getCallBacks(keyHash), [ cb ]));
  }

  /**
     * @param {string} key
     * @param {function} cb
     */
  removeCallBack(key /*: string */, cb /*: function */) {
    const { keyCode, modMask, eventType } = this.parseKey(key);
    const keyHash = this.genKeyHash(keyCode, modMask, eventType);
    remove(this.getCallBacks(keyHash), function(/** @type {function} */callback) {
      return callback === cb;
    });
  }

  /**
   * @param {KeyboardEvent} event
   */
  onKeyEvent(event /*: KeyboardEvent */) {
    if (!event.key) { return; }
    /** @type string*/
    const keyCode = get(KeyMap, event.key, event.key.toLowerCase());
    const input = isInput(event);
    // Create mod mask
    let mod = 0;
    if (event.altKey) { mod |= Mod.alt; }
    if (event.ctrlKey) { mod |= Mod.ctrl; }
    if (event.metaKey) { mod |= Mod.meta; }
    if (this.checkOnInputs && this.checkInput(input, keyCode, mod)) { return; }
    if (event.shiftKey) { mod |= Mod.shift; }

    const keyHash = this.genKeyHash(keyCode, mod, event.type);

    forEach(this.getCallBacks(keyHash), (/** @type {function} */ cb) => {
      // NB: It will stop the cb chain if cb returns false!!
      return cb(event);
    });
  }

  /**
   * @param {boolean} isInput
   * @param {string} key
   * @param {number} modMask
   * @returns {boolean}
   */
  checkInput(isInput /*: boolean */, key /*: string */, modMask /*: number */) {
    /* on inputs only fire events when it has a modifier,
         with an exception for escape */
    return isInput && !((modMask !== 0) || (key === "esc"));
  }
}

/**
 * @typedef {{
 *   local?: boolean,
 *   checkOnInputs?: boolean
 * }} KeyboardEventMixinOptions
 *
 * @typedef {{
 *   k: string,
 *   cb: function
 * }} KeyEventCB
 *
 * @typedef {{
 *    _keyboardCBs: Array<KeyEventCB>
 * }} Options
 */

const BaseKeyboardEventMixin = (/** @type {KeyboardEventMixinOptions} */options) => mixinMaker({
  /**
  Supported options:
  {
    local: boolean (def false) // It will instantiate a local Keyboard event handler
                               // and not a global one
    checkOnInputs: boolean (def true) // It will check if focus is on input
                                      // and prevent of executing the event callback
  }
   */
  /**
   * @returns {{ _keyHandler: KeyboardEventHandler|undefined }}
   */
  data() {
    return {
      /* eslint-disable vue/no-reserved-keys */
      _keyHandler: undefined
    };
  },
  computed: {
    /** @return {boolean} */
    showKeyHints() { return get(createStore(), [ "state", "ui", "showKeyHints" ], false); }
  },
  mounted() {
    const local = get(options, "local", false);
    const checkOnInputs = get(options, "checkOnInputs", true);
    if (local) {
      this._keyHandler = new KeyboardEventHandler(this.$el, checkOnInputs);
    }
    else {
      if (!_globalListener) {
        _globalListener = new KeyboardEventHandler(null, checkOnInputs);
      }
      this._keyHandler = _globalListener;
    }
    // '_keyboardCBs' is used internally. Cast with extended fields
    /** @type {Options} */(this.$options)._keyboardCBs = [];
  },
  beforeDestroy() {
    if (!this._keyHandler) {
      return;
    }
    // Remove all the CBs registered to this component
    forEach(/** @type {Options} */(this.$options)._keyboardCBs, (obj) => {
      // @ts-ignore it is initialized
      this._keyHandler.removeCallBack(obj.k, obj.cb);
    });
    this._keyHandler.releaseIfEmpty();
  },
  methods: {
    /**
     * @param {string | string[]} key
     * @param {function} cb
     */
    onKey(key, cb) {
      const keyboardCBs = /** @type {Options} */(this.$options)._keyboardCBs;

      if (isArray(key)) {
        for (const sKey of key) {
          // @ts-ignore it is initialized
          this._keyHandler.addCallBack(sKey, cb);
          // Save also locally
          keyboardCBs.push({ k: sKey, cb: cb });
        }
      }
      else {
        // @ts-ignore it is initialized
        this._keyHandler.addCallBack(key, cb);
        // Save also locally
        keyboardCBs.push({ k: key, cb: cb });
      }

    },
    /**
     * @param {string} key
     * @param {string?} [signal=null]
     */
    onKeyEmit(key, signal = null) {
      this.onKey(key, (/** @type {KeyboardEvent} */event) => this.$emit(signal || key, event));
    },
    /**
     * @param {string} key
     * @param {function?} cb
     */
    removeKey(key, cb) {
      const keyboardCBs = /** @type {Options} */(this.$options)._keyboardCBs;

      if (keyboardCBs.length > 0) {
        const removedCbs = remove(keyboardCBs, (obj) => {
          // if cb not provided it will remove all the callbacks for that key
          return obj.k === key &&
            (cb ? obj.cb === cb : true);
        });
        forEach(removedCbs, (obj) => {
          // @ts-ignore it is initialized
          this._keyHandler.removeCallBack(obj.k, obj.cb);
        });
      }
    },
    /**
     * @param {boolean} value
     */
    setCheckOnInput(value) {
      // @ts-ignore it is initialized
      this._keyHandler.setCheckOnInput(value);
    }
  }
});

export default BaseKeyboardEventMixin;
