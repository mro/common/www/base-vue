// @ts-check

import { invoke } from "lodash";
import { mixinMaker } from "../utils";

/**
 * @typedef {import("vue").ComponentOptionsMixin} ComponentOptionsMixin
 */

const resizeObserver = new ResizeObserver(
  function(entries) {
    entries.forEach((e) => {
      // get size (width and heigth)
      // see https://developer.mozilla.org/en-US/docs/Web/API/ResizeObserverEntry
      let width, height;

      if (e.contentBoxSize) { // using latest properties
        width = e.contentBoxSize?.[0]?.inlineSize ??
          // @ts-ignore: https://developer.mozilla.org/en-US/docs/Web/API/ResizeObserverSize/inlineSize
          e.contentBoxSize?.inlineSize; // for old version of Firefox
        height = e.contentBoxSize?.[0]?.blockSize ??
          // @ts-ignore: https://developer.mozilla.org/en-US/docs/Web/API/ResizeObserverSize/blockSize
          e.contentBoxSize?.blockSize; // ditto
      }
      else {
        width = e.contentRect?.width;
        height = e.contentRect?.height;
      }
      invoke(e.target, [ "__vue__", "onResize" ], width, height);
    });
  }
);

const component = mixinMaker({
  mounted() {
    resizeObserver.observe(this.$el);
  },
  beforeDestroy() {
    resizeObserver.unobserve(this.$el);
  }
});
export default component;
