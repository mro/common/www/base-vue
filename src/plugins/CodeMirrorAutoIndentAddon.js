// @ts-nocheck
// Copyright (C) 2017 by Joël Porquet <joel.porquet@gmail.com>
// Distributed under an MIT license: https://github.com/joel-porquet/CodeMirror-markdown-list-autoindent/blob/master/LICENSE

"use strict";

import CodeMirror from "codemirror";

var cmPos = CodeMirror.Pos;
var listTokenRE = /^(\s*)(>[> ]*|[*+-] \[[x ]\]|[*+-]|(\d+)[.)])(\s*)$/;

/**
 * @param {CodeMirror.Position} pos
 * @param {CodeMirror} cm
 */
function matchListToken(pos, cm) {
  /* Get some info about the current state */
  var eolState = cm.getStateAfter(pos.line);
  var inList = eolState.list !== false;
  var inQuote = eolState.quote !== 0;

  /* Get the line from the start to where the cursor currently is */
  var lineStart = cm.getRange(cmPos(pos.line, 0), pos);

  /* Matches the beginning of the list line with the list token RE */
  var match = listTokenRE.exec(lineStart);

  /* Not being in a list, or being in a list but not right after the list
   * token, are both not considered a match */
  if ((!inList && !inQuote) || !match) { return false; }
  else { return true; }
}

CodeMirror.commands.autoIndentMarkdownList = function(/** @type CodeMirror */ cm) {
  if (cm.getOption("disableInput")) { return CodeMirror.Pass; }
  var ranges = cm.listSelections();
  for (var i = 0; i < ranges.length; i++) {
    var pos = ranges[i].head;

    if (!ranges[i].empty() || !matchListToken(pos, cm)) {
      /* If no match, call regular Tab handler */
      cm.execCommand("defaultTab");
      return undefined;
    }

    /* Select the whole list line and indent it by one unit */
    cm.indentLine(pos.line, "add");
  }
  return undefined;
};

CodeMirror.commands.autoUnindentMarkdownList = function(/** @type CodeMirror */ cm) {
  if (cm.getOption("disableInput")) { return CodeMirror.Pass; }
  var ranges = cm.listSelections();
  for (var i = 0; i < ranges.length; i++) {
    var pos = ranges[i].head;

    if (!ranges[i].empty() || !matchListToken(pos, cm)) {
      /* If no match, call regular Shift-Tab handler */
      cm.execCommand("indentAuto");
      // return;
    }

    /* Select the whole list line and unindent it by one unit */
    cm.indentLine(pos.line, "subtract");
  }
  return undefined;
};
