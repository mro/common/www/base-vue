/* eslint-disable max-lines */
/* eslint max-lines: ["error", 400] */
// @ts-check

import Vue from "vue";
import CodeMirror from "codemirror";
import "codemirror/addon/edit/continuelist";
import "./CodeMirrorAutoIndentAddon";
/* eslint-disable-next-line @typescript-eslint/no-unused-vars */ // @ts-ignore
import PugMod from "codemirror/mode/pug/pug.js"; // jshint ignore:line
/* eslint-disable-next-line @typescript-eslint/no-unused-vars */ // @ts-ignore
import MarkdownMod from "codemirror/mode/markdown/markdown"; // jshint ignore:line
import { BaseStoreMixin } from "../store/utils";
import BaseResizeObserverMixin from "../mixins/BaseResizeObserverMixin";
import { assign, bindAll, clone, cloneDeep, first,
  includes, isEmpty, set, split, throttle } from "lodash";

const managedModes = [ "pug", "markdown", "javascript" ];
const managedEvents = [
  "scroll", "changes", "beforeChange", "cursorActivity",
  "keyHandled", "inputRead", "electricInput", "beforeSelectionChange",
  "viewportChange", "swapDoc", "gutterClick", "gutterContextMenu",
  "focus", "blur", "refresh", "optionChange", "scrollCursorIntoView",
  "update"
];

/**
 * @typedef {{ textarea: HTMLTextAreaElement }} Refs
 */
const component = /** @type {V.Constructor<any, Refs>} */ (Vue).extend({
  name: "CodeMirrorEditor",
  mixins: [ BaseResizeObserverMixin, BaseStoreMixin ],
  props: {
    value: { type: String, default: "" },
    name: { type: String, default: "codemirror" },
    maxLength: { type: Number, default: -1 },
    placeholder: { type: String, default: "" },
    markdownTools: { type: Boolean, default: true },
    options: { type: Object, default: () => ({}) },
    events: { type: Array, default: () => ([]) },
    managedModes: { type: Array, default: () => managedModes }
  },
  /**
   * @return {{
   *   content: string,
   *   codemirror: CodeMirror.EditorFromTextArea | null
   *   cminstance: CodeMirror.EditorFromTextArea | null
   *   cmOptions: CodeMirror.EditorConfiguration
   * }}
   */
  data() {
    return {
      content: "", codemirror: null, cminstance: null,
      cmOptions: { mode: "markdown" } // Default mode
    };
  },
  computed: {
    /**
     * @returns {boolean}
     */
    showMarkdownTools() {
      return this.cmOptions.mode === "markdown" && this.markdownTools;
    },
    /**
     * @return {boolean}
     */
    darkMode() { return this.getBaseState([ "ui", "darkMode" ]); },
    /**
     * @returns {number}
     */
    valueLength() {
      return this.cminstance?.getValue().length ?? 0;
    }
  },
  watch: {
    options: {
      deep: true,
      /**
       * @param {any} options
       */
      handler(options) {
        // eslint-disable-next-line guard-for-in
        for (const key in options) {
          set(this.cmOptions, key, options[key]);
          // @ts-ignore key is well formed
          if (this.cminstance) { this.cminstance.setOption(key, options[key]); }
        }
      }
    },
    /**
     * @param {string} newVal
     */
    value(newVal) {
      this.handlerCodeChange(newVal);
    },
    darkMode: function(mode) {
      if (this.codemirror) {
        this.codemirror.setOption("theme", mode ? "material-darker" : "default");
      }
    }
  },
  mounted() {
    this.initialize();
    bindAll(this, [ "bold", "italic", "list", "link", "undo", "redo" ]);

    this.onResize = throttle(this.onResize, 250 /* ms */);
  },
  beforeDestroy() {
    if (this.cminstance) { this.cminstance.toTextArea(); }
  },
  methods: {
    /**
     * @param {number} width
     * @param {number} height
     */
    onResize(width, height) { // start auto-refresh
      if (width > 0 && height > 0) {

        // @ts-ignore
        const display = this.cminstance?.display;
        // eslint-disable-next-line eqeqeq
        if (display.lastWrapHeight == display.wrapper.clientHeight) {
          return;
        }

        this.refresh();
      }
    },
    /**
     * @brief CodeMirror Management
     */
    initialize() {
      // Only markdown or pug mode at the moment. Default is markdown
      /** @type CodeMirror.EditorConfiguration */
      const cmOptions = assign(this.cmOptions, this.options);
      // If markdown mode, add extrakeys and addons
      if (cmOptions.mode === "markdown") {
        assign(cmOptions, {
          extraKeys: {
            "Ctrl-B": this.bold, "Cmd-B": this.bold,
            "Ctrl-I": this.italic, "Cmd-I": this.italic,
            "Ctrl-L": this.list, "Cmd-L": this.list,
            "Ctrl-K": this.link, "Cmd-K": this.link,
            "Ctrl-Z": this.undo, "Cmd-Z": this.undo,
            "Shift-Ctrl-Z": this.redo, "Shift-Cmd-Z": this.redo,
            Enter: "newlineAndIndentContinueMarkdownList",
            Tab: "autoIndentMarkdownList",
            "Shift-Tab": "autoUnindentMarkdownList" }
        });
      }

      if (!includes(this.managedModes, cmOptions.mode)) {
        this.$refs.textarea.innerHTML = `Mode ${cmOptions.mode} not supported...`;
        return;
      }
      cmOptions.theme = this.darkMode ? "material-darker" : "default";
      this.codemirror = CodeMirror.fromTextArea(this.$refs.textarea, cmOptions);
      this.cminstance = this.codemirror;
      this.cmOptions = cmOptions;
      this.cminstance.setValue(this.value || this.content);
      this.cminstance.on("beforeChange", this.enforceMaxLength);
      this.cminstance.on("change", (/** @type CodeMirror.Editor */cm) => {
        this.content = cm.getValue();
        if (this.$emit) {
          this.$emit("input", this.content);
        }
      });
      /** @type any */
      const tmpEvents = {};
      clone(managedEvents)
      // @ts-ignore
      .concat(this.events)
      .filter((/** @type string */ e) => (!tmpEvents[e] && (tmpEvents[e] = true)))
      .forEach((event) => {
        if (!this.cminstance) { return; }
        // @ts-ignore event is inside managedEvents
        this.cminstance.on(event, (/** @type any[] */...args) => {
          this.$emit(event, ...args);
        });
      });
      this.$emit("ready", this.codemirror);
      // prevents funky dynamic rendering
      this.refresh();
    },
    refresh() {
      this.$nextTick(() => {
        if (!this.cminstance) { return; }
        this.cminstance.refresh();
      });
    },
    focus() {
      if (!this.cminstance) { return; }
      this.cminstance.focus();
    },
    /**
     * @returns { CodeMirror.Editor | null }
     */
    get() {
      return this.cminstance;
    },
    /**
     * @param {CodeMirror.Editor} cm
     * @param {any} change
     */
    enforceMaxLength(cm, change) {
      if (this.maxLength > 0 && change.update) {
        const separator = cm.getDoc().lineSeparator();
        var str = change.text.join(separator);
        var delta = str.length -
          (cm.indexFromPos(change.to) - cm.indexFromPos(change.from));
        if (delta <= 0) { return true; }
        delta = cm.getValue().length + delta - this.maxLength;
        if (delta > 0) {
          str = str.substr(0, str.length - delta);
          change.update(change.from, change.to, str.split(separator));
        }
      }
      return undefined;
    },
    /**
     * @param {string} newVal
     */
    handlerCodeChange(newVal) {
      if (!this.cminstance) { return; }
      const cmValue = this.cminstance.getValue();
      if (newVal !== cmValue) {
        /** @type CodeMirror.ScrollInfo */
        const scrollInfo = this.cminstance.getScrollInfo();
        this.cminstance.setValue(newVal);
        this.content = newVal;
        this.cminstance.scrollTo(scrollInfo.left, scrollInfo.top);
      }
    },
    getForwardSelection() {
      if (!this.cminstance) { return null; }
      const range = cloneDeep(first(this.cminstance.listSelections()));
      if (!range) { return null; }
      if (range.anchor.line > range.head.line ||
        (range.anchor.line === range.head.line &&
          range.anchor.ch > range.head.ch)) {
        const tmp = range.head;
        range.head = range.anchor;
        range.anchor = tmp;
      }
      return range;
    },
    /**
     * @brief Toolbar for Markdown mode
     */
    heading() {
      if (!this.cminstance) { return; }
      const withSpace = this.cminstance.getSelection() ===
        this.cminstance.getLine(this.cminstance.getCursor().line);
      this.makeListWith("#", withSpace);
    },
    bold() {
      this.wrapSelectionWith("**");
    },
    italic() {
      this.wrapSelectionWith("_");
    },
    strikethrough() {
      this.wrapSelectionWith("~~");
    },
    quote() {
      this.makeListWith(">");
    },
    code() {
      if (!this.cminstance) { return; }
      const selected = this.cminstance.getSelection();
      const nLines = split(selected, "\n").length;
      if (nLines <= 1) {
        this.wrapSelectionWith("`");
      }
      else {
        const range = this.getForwardSelection();
        if (!range) { return; }
        const elStart = range.anchor.ch === 0 ? "```\n" : "\n```\n";
        const lastLine = clone(this.cminstance.getLine(range.head.line));
        const elEnd = range.head.ch === lastLine.length ? "\n```" : "\n```\n";
        this.wrapSelectionWith(elStart, elEnd);
      }
    },
    link() {
      if (!this.cminstance) { return; }
      const initialCursor = clone(this.cminstance.getCursor("to"));
      const selected = this.cminstance.getSelection(); // It may be empty
      // Advance and set the cursor at the beginning of "url" text
      const startCursor = {
        ch: initialCursor.ch + 3 /* for []( */,
        line: initialCursor.line
      };
      const endCursor = {
        ch: startCursor.ch + 3, // "url"
        line: startCursor.line
      };
      this.cminstance.replaceSelection(`[${selected}](url)`);
      this.cminstance.focus();
      this.cminstance.setSelection(startCursor, endCursor);
    },
    list() {
      this.makeListWith("-");
    },
    taskList() {
      this.makeListWith("- [ ]");
    },
    table() {
      if (!this.cminstance) { return; }
      const tableStr =
          "\n| header | header |\n" +
          "| ------ | ------ |\n" +
          "| cell | cell |\n" +
          "| cell | cell |\n";
      const cursor = this.cminstance.getCursor();
      this.cminstance.replaceRange(tableStr, cursor, cursor);
    },
    numberedList() {
      if (!this.cminstance) { return; }
      if (!this.cminstance.somethingSelected()) {
        this.makeListWith("1.");
        return;
      }
      const selected = this.cminstance.getSelection();
      const lines = split(selected, "\n");
      let listed = "";
      let number = 1;
      for (const line of lines) {
        if (isEmpty(line)) { continue; }
        listed += `${number++}. ${line}\n`;
      }
      this.cminstance.replaceSelection(`${listed}`);
    },
    indent() {
      if (!this.cminstance) { return; }
      this.indentLines("add");
      this.cminstance.focus();
    },
    outdent() {
      if (!this.cminstance) { return; }
      this.indentLines("subtract");
      this.cminstance.focus();
    },
    /**
     * @param {"add" | "subtract" } mode
     */
    indentLines(mode) {
      if (!this.cminstance) { return; }
      if (mode !== "add" && mode !== "subtract") { return; }
      const range = this.getForwardSelection();
      if (!range) { return; }
      for (let line = range.anchor.line; line <= range.head.line; line++) {
        this.cminstance.indentLine(line, mode);
      }
    },
    undo() {
      if (!this.cminstance) { return; }
      this.cminstance.undo();
    },
    redo() {
      if (!this.cminstance) { return; }
      this.cminstance.redo();
    },
    /**
     * @param {string} elementStart
     * @param {string?=} elementEnd
     */
    wrapSelectionWith(elementStart, elementEnd = undefined) {
      if (!this.cminstance) { return; }
      if (!elementEnd) {
        elementEnd = elementStart;
      }
      if (!this.cminstance.somethingSelected()) {
        // Add element start and end and move cursor in the middle
        const initialCursor = this.cminstance.getCursor();
        this.cminstance.replaceSelection(`${elementStart}${elementEnd}`);
        // Advance and set the cursor
        initialCursor.ch += elementStart.length;
        this.cminstance.setCursor(initialCursor);
      }
      else {
        const selection = first(this.cminstance.listSelections());
        const selected = this.cminstance.getSelection();
        if (!selection) { return; }
        this.cminstance.replaceRange(`${elementStart}${selected}${elementEnd}`,
          selection.anchor, selection.head, selected);
      }
      this.cminstance.focus();
    },
    /**
     * @param {string} elementList
     * @param {boolean} withSpace
     */
    makeListWith(elementList, withSpace = true) {
      if (!this.cminstance) { return; }
      if (!this.cminstance.somethingSelected()) {
        // Add element list and move cursor at the end
        const initialCursor = this.cminstance.getCursor();
        const line = this.cminstance.getLine(initialCursor.line);
        this.cminstance.replaceRange(`${elementList}${withSpace ? " " : ""}${line}`,
          { line: initialCursor.line, ch: 0 },
          // @ts-ignore: ch is set to last position automatically
          { line: initialCursor.line }, line); // This replace the whole line
        // Advance and set the cursor
        initialCursor.ch += elementList.length + 1;
        this.cminstance.setCursor(initialCursor);
      }
      else {
        const selected = this.cminstance.getSelection();
        const lines = split(selected, "\n");
        let listed = "";
        for (const line of lines) {
          if (isEmpty(line)) { continue; }
          listed += `${elementList}${withSpace ? " " : ""}${line}\n`;
        }
        this.cminstance.replaceSelection(`${listed}`);
      }
      this.cminstance.focus();
    }
  }
});
export default component;
