// @ts-check
import MarkdownIt from "markdown-it";
// @ts-ignore
import { full as MarkdownItEmoji } from "markdown-it-emoji";
import MarkdownItTasklists from "markdown-it-task-lists";
import MarkdownItLinkAttributes from "markdown-it-link-attributes";

import { assign, set } from "lodash";
import Vue from "vue";

/**
 * @typedef {{
 *  mdContainer: HTMLDivElement
 * }} Refs
 */

const component = /** @type {V.Constructor<any, Refs>} */ (Vue).extend({
  name: "MarkdownViewer",
  props: {
    value: { type: [ String ], default: "" },
    options: { type: Object, default: () => ({}) }
  },
  /**
   * @return {{
    *   mdOptions: MarkdownIt.Options,
    *   mdinstance: MarkdownIt
    * }}
    */
  data() {
    /** @type MarkdownIt.Options */
    const mdOptions = assign({}, { linkify: true },
      /** @type MarkdownIt.Options */(this.options));
    return {
      mdOptions,
      mdinstance: new MarkdownIt(mdOptions)
      .use(MarkdownItEmoji)
      .use(MarkdownItTasklists, {
        enabled: true,
        label: true,
        labelAfter: true
      })
      .use(MarkdownItLinkAttributes, {
        attrs: {
          target: "_blank",
          rel: "noopener"
        }
      })
    };
  },
  watch: {
    options: {
      deep: true,
      /**
       * @param {any} options
       */
      handler(options) {
        // eslint-disable-next-line guard-for-in
        for (const key in options) {
          set(this.mdOptions, key, options[key]);
        }
        this.mdinstance.set(this.mdOptions);
      }
    },
    value: {
      immediate: true,
      /**
       * @param {string} val
       */
      handler(val) {
        this.$nextTick(() => {
          // this is async, do ensure that things haven't been destroyed
          if (this.$refs && this.$refs.mdContainer && this.mdinstance) {
            this.$refs.mdContainer.innerHTML = this.mdinstance.render(val);
          }
        });
      }
    }
  },
  mounted() {
    // Beautify output of tables with bootstrap classes
    // eslint-disable-next-line camelcase
    this.mdinstance.renderer.rules.table_open = function() {
      return '<table class="table table-striped">\n';
    };
  }
});
export default component;
