// @ts-check
import $ from "jquery";
import moment from "moment";
import { forEach, isNil } from "lodash";
import { genId } from "../utils";
import Vue from "vue";

/**
 * @type {Promise<void>}
 */
const initTempusDominus = (async function() {
  // karma loads test files asynchronuously, and we're in an async function,
  // let's be cautious
  if (!("jQuery" in window)) {
    // @ts-ignore of course doesn't exist. we are creating it.
    window.jQuery = $;
  }
  if (!("moment" in window)) {
    // @ts-ignore of course doesn't exist. we are creating it.
    window.moment = moment;
  }
  await import("tempusdominus-bootstrap");
  // Override Font Awesome icons on the widget
  // @ts-ignore window.jQuery exist now.
  $.extend(true, window.jQuery.fn.datetimepicker.Constructor.Default, {
    icons: {
      time: "fa fa-clock",
      today: "fa fa-calendar-check"
    }
  });
}());

/**
 * We are using 'tempusdominus-bootstrap' instead of the original
 * 'tempusdominus-bootstrap-4' because the last one is not maintained
 * since more than 2 years. Instead the fork is maintained and fixed by
 * the community.
 */

const component = /** @type {V.Constructor<any, any>} */(Vue).extend({
  name: "TempusDominusDatePicker",
  props: {
    value: {
      default: null,
      type: Number,
      /**
       * At the moment it accepts only unix timestamp.
       * We can improve in order to accept also formatted data strings,
       * Date objects and moment objects
       */
      validator(value) {
        return value === null || typeof value === "number";
      }
    },
    // https://tempusdominus.github.io/bootstrap-4/Options/
    config: {
      type: Object,
      default: () => ({
        format: "YYYY-MM-DD HH:mm:ss ZZ",
        buttons: {
          showToday: true,
          showClear: true,
          showClose: false
        },
        keepInvalid: false,
        useCurrent: false
      })
    }
  },
  /**
   * @returns {{
   *   id: string,
   *   dp: any | null,
   *   elem: JQuery<HTMLElement> | null
   * }}
   */
  data() {
    return {
      id: genId(),
      // Tempus Dominus data control
      dp: null,
      // Jquery element
      elem: null
    };
  },
  watch: {
    /**
     * @brief Listen to change from outside of component and update DOM
     * @param {number | null} newValue
     */
    value(newValue /*: ?number*/) {
      this.updateDateValue(newValue);
    },
    /**
     * @brief Watch for any change in options and set them
     */
    config: {
      deep: true,
      /**
       * @param {any} newConfig
       */
      handler(newConfig /*: {} */) {
        if (this.dp) {
          this.dp.options(newConfig);
        }
      }
    }
  },
  async mounted() {
    // Return early if date-picker is already loaded
    if (this.dp) { return; }
    // Init deps
    await initTempusDominus;
    // Save element
    // @ts-ignore I assure you window.jQuery exist now.
    this.elem = window.jQuery(this.$el);
    if (!this.elem) { return; }
    // This trick helps with keepFocus, otherwise the plugin complains
    this.elem.addClass("datetimepicker-input");
    // Init date-picker
    // @ts-ignore: datetimepicker is added by tempus plugin
    this.elem.datetimepicker(this.config);
    // Set attrs again. Plugin is removing some of them.
    forEach(this.$attrs, (value, key) => {
      if (this.elem) { this.elem.attr(key, value); }
    });
    // Store data control
    this.dp = this.elem.data("datetimepicker");
    // Set initial value
    this.updateDateValue(this.value);
    // If the input has the focus, show the widget!
    if (this.$el === document.activeElement) {
      this.dp.show();
    }
    // Watch for changes
    this.elem.on("change.datetimepicker", this.onChange);
    // Register remaining events
    this.registerEvents();
  },
  beforeDestroy() {
    if (this.dp) {
      this.dp.destroy();
      this.dp = null;
    }
    if (this.elem) {
      this.elem.off("change.datetimepicker");
      this.elem = null;
    }
  },
  methods: {
    /**
     * @param {number?} timestamp
     */
    updateDateValue(timestamp) {
      if (this.dp) {
        this.dp.date(isNil(timestamp) ? null : moment.unix(timestamp));
      }
    },
    /**
     * @biref Update v-model upon change triggered by date-picker itself
     * @param {any} event
     */
    onChange(event /*: any */) {
      // In case of invalid date, we have event.date === false.
      // Let's trigger input event in order to allow
      // users doing custom check of the field
      if (event.date === false) {
        this.$emit("input", null);
      }
      else if (moment.isMoment(event.date)) {
        const timestamp = /** @type moment.Moment */(event.date).unix();
        if (timestamp !== this.value) {
          this.$emit("input", timestamp);
        }
      }
    },
    /**
     * @brief Emit all available events from Tempus Dominus
     *   https://tempusdominus.github.io/bootstrap-4/Events/
     */
    registerEvents() {
      if (!this.elem) { return; }
      const events = [ "hide", "show", "change", "error", "update" ];
      const eventNameSpace = "datetimepicker";

      events.forEach((name) => {
        // @ts-ignore: it's not null here
        this.elem.on(`${name}.${eventNameSpace}`, (/** @type any[] */...args) => {
          this.$emit(`${name}`, ...args);
        });
      });
    }
  }
});
export default component;
