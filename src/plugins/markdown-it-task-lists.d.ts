

declare module "markdown-it-task-lists" {
    import MarkdownIt = require('markdown-it');
    declare namespace MarkdownItTasklists {
        interface Config {
            enabled: boolean,
            label: boolean,
            labelAfter: boolean
        }
    }
    
    declare function MarkdownItTasklists(md: MarkdownIt, config: MarkdownItTasklists.Config): void;

    export = MarkdownItTasklists;
}







