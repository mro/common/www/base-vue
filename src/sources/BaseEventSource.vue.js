// @ts-check
import { forEach, hasIn, invoke } from "lodash";
import Vue from "vue";

import ErrorHandlerMixin from "../mixins/BaseErrorHandlerMixin";
import d from "debug";

const debug = d("base:es");

/**
 * @typedef {{
 *    source: EventSource|null,
 *    listeners: { [key: string]: (e: MessageEvent) => void }|null
 * }} Opts
 */

const component = /** @type {V.Constructor<Opts, any>} */(Vue).extend({
  name: "BaseEventSource",
  mixins: [ ErrorHandlerMixin ],
  props: {
    src: { type: String, default: null },
    json: { type: Boolean, default: false }
  },
  /** @return {{ message: any }} */
  data() {
    return { message: null };
  },
  watch: {
    src: {
      immediate: true,
      handler() {
        this.release();
        this.connect();
      }
    }
  },
  beforeDestroy() {
    this.release();
  },
  methods: {
    release() {
      if (this.$options.source) {
        debug("closing EventSource(%s)", this.src);
        forEach(this.$options.listeners, // @ts-ignore
          // eslint-disable-next-line @stylistic/js/max-len
          (listener, name) => this.$options.source.removeEventListener(name, listener));
        this.$options.source.close();
        this.$options.source = null;
        this.$options.listeners = null;
      }
    },
    connect() {
      if (this.$options.source || !this.src) { return; }
      debug("opening EventSource(%s)", this.src);
      this.$options.source = new EventSource(this.src);
      this.$options.listeners = {};
      forEach(this.$listeners, (l, name) => {
        if (name === "message" || name === "error") { return; }
        const cb = (/** @type {MessageEvent} */ e) => this.onEvent(name, e);
        // @ts-ignore
        this.$options.listeners[name] = cb;
        // @ts-ignore
        this.$options.source.addEventListener(name, cb);
      });
      this.$options.source.onmessage = this.onMessage;
      this.$options.source.onerror = (/** @type {any} */ err) => {
        debug("error: %o", err);
        if (hasIn(err, "message")) {
          this.onError(err);
        }
        else if (hasIn(err, "data")) {
          this.onError(new Error(err.data));
        }
        else {
          this.onError(new Error("[Eventsource]: failed to connect to " + this.src));
        }
      };
    },
    /**
     * @param {string} type
     * @param {MessageEvent} msg
     */
    onEvent(type, msg) {
      if (this.json) {
        try {
          this.$emit(type, JSON.parse(msg.data));
        }
        catch (e) {
          this.onError(e);
        }
      }
      else {
        this.$emit(type, msg);
      }
    },
    /**
     * @param {MessageEvent} msg
     */
    onMessage(msg) {
      if (this.json) {
        try {
          this.message = JSON.parse(msg.data);
          this.$emit("message", this.message);
        }
        catch (e) {
          this.onError(e);
        }
      }
      else {
        this.message = msg.data;
        this.$emit("message", msg);
      }
    }
  },
  /**
   * @return {Vue.VNode}
   */
  render() {
    /* render-less component, renders its default slot */
    return invoke(this.$scopedSlots, "default", { message: this.message });
  }
});
export default component;
