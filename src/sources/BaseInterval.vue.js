// @ts-check
import { get, invoke } from "lodash";
import Vue from "vue";
import d from "debug";

import { BaseStoreMixin } from "../store/utils";
import BaseIntervalSource from "../store/sources/BaseIntervalSource";
import logger from "../Logger/BaseLogger";

const debug = d("base:interval");

/**
 * @typedef {{ source: BaseIntervalSource|null }} Opts
 */

const component = /** @type {V.Constructor<Opts, any>} */(Vue).extend({
  name: "BaseInterval",
  mixins: [ BaseStoreMixin ],
  props: {
    interval: { type: Number, default: 1000 },
    store: { type: String, default: null }
  },
  /** @return {{ localCount: number }} */
  data() {
    return { localCount: 0 };
  },
  computed: {
    /**
     * @return {number}
     */
    count() {
      return get(this.getBaseStore()?.state, this.store?.split("/"))?.count ?? this.localCount;
    }
  },
  watch: {
    interval: "restart",
    store: function(_, old) {
      // @ts-ignore
      this.getBaseStore()?.sources[old]?.destroy();
      this.restart();
    }
  },
  mounted() { this.restart(); },
  beforeDestroy() { this.stop(); },
  methods: {
    stop() {
      this.$options.source?.removeEventListener("tick", this.onTick);
      this.$options.source?.destroy();
      this.$options.source = null;
    },
    start() {
      if (this.$options.source) { return; }
      const store = this.getBaseStore();
      if (store && this.store) {
        try {
          BaseIntervalSource.register(store, this.store, this.interval);
        }
        catch (e) { logger.error(`failed to create source: ${e}`); }
        // @ts-ignore
        this.$options.source = store.sources?.[this.store];
      }
      else {
        this.$options.source = new BaseIntervalSource(undefined, undefined,
          this.interval);
      }
      this.$options.source?.addEventListener("tick", this.onTick);
    },
    restart() {
      this.stop();
      this.start();
    },
    onTick() {
      ++this.localCount;
      debug("tick %i", this.count);
      this.$emit("tick", this.count);
    }
  },
  /**
   * @return {Vue.VNode}
   */
  render() {
    /* render-less component, renders its default slot */
    return invoke(this.$scopedSlots, "default", { count: this.count });
  }
});
export default component;
