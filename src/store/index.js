// @ts-check

import { get, includes, set, some } from "lodash";
import Vuex from "vuex";
import Vue from "vue";
import createPersistedState from "vuex-persistedstate";

import ui from "./modules/ui";

Vue.use(Vuex);

/**
 * @typedef {import('./sources/UserSource').default} UserSource
 */

/** @type {V.StoreOptions<BaseVue.StoreState>} */
export const storeOptions = {
  // @ts-ignore route and ui are added later
  state: { user: null },
  getters: {
    /** @param {BaseVue.StoreState} state */
    username: (state) => {
      return get(state, "user.sub", null);
    },
    /** @param {BaseVue.StoreState} state */
    canEdit: (state) => {
      const userRoles = get(state, [ "user", "cern_roles" ]);
      return some([ "editor", "admin" ], (r) => includes(userRoles, r));
    },
    /** @param {BaseVue.StoreState} state */
    canAdmin: (state) => {
      const userRoles = get(state, [ "user", "cern_roles" ]);
      return includes(userRoles, "admin");
    }
  },
  mutations: {
    /**
     * @param {BaseVue.StoreState} state
     * @param {BaseVue.StoreState["user"]} user
     */
    user(state, user) {
      state.user = user;
    }
  },
  modules: { ui },
  plugins: [
    createPersistedState({ paths: [ "ui" ] }),
    /** @param {V.Store<any>} store */
    function(store) {
      if (sources.user) {
        set(store, [ "sources", "user" ], sources.user);
        sources.user.setStore(store);
      }
    }
  ]
};

/** @type {V.Store<BaseVue.StoreState>|undefined} */
var _storeInstance;

/**
 * @return {V.Store<BaseVue.StoreState>}
 */
export function createStore() {
  if (!_storeInstance) {
    _storeInstance = new Vuex.Store(storeOptions);
  }
  return _storeInstance;
}
export default createStore;

export function getStore() { return _storeInstance; }

/**
 * @brief mainly for test purpose
 */
export function destroyStore() {
  _storeInstance = undefined;
}

// UserSource is added dynamically when loading BaseVue
/** @type {{ user?: UserSource }} */
export const sources = {};
