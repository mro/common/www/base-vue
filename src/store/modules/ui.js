// @ts-check
import Vue from "vue";
import { assign } from "lodash";

// @ts-ignore
import dark from "!file-loader?name=dark.css!sass-loader!../../scss/_dark.scss";
// @ts-ignore
import light from "!file-loader?name=light.css!sass-loader!../../scss/_light.scss";

const Themes = {
  LIGHT: light,
  DARK: dark
};

/**
 * @param {boolean} isDarkMode
 */
const getThemeString = (isDarkMode) => (isDarkMode ?
  Themes.DARK : Themes.LIGHT);

/**
 * @param {string} theme
 * @returns
 */
const setDocumentTheme = (theme) => {
  const old = document.getElementById("theme");
  const filename = theme?.substr(theme.lastIndexOf("/") + 1);
  if (old?.getAttribute("href")?.endsWith(filename)) { return; }

  const newTheme = document.createElement("link");
  newTheme.setAttribute("href", theme);
  newTheme.setAttribute("rel", "stylesheet");
  newTheme.onload = () => {
    document.getElementById("theme")?.remove();
    newTheme.id = "theme";
  };
  document.head.append(newTheme);
};

/** @type {V.Module<BaseVue.UiState>} */
const m = {
  namespaced: true,
  state: () => ({
    showKeyHints: false,
    darkMode: false,
    muteAlerts: false
  }),
  mutations: {
    /**
     * @param  {BaseVue.UiState} state
     * @param  {string} name
     */
    remove(state, name) {
      Vue.delete(state, name);
    },
    update: assign
  },
  actions: {
    /** @param {V.ActionContext<BaseVue.StoreState>} ctx */
    toggleTheme(ctx) {
      ctx.commit("update", { darkMode: !ctx.state.darkMode });
      ctx.dispatch("applyTheme");
    },
    /** @param {V.ActionContext<BaseVue.StoreState>} ctx */
    applyTheme(ctx) {
      const themeStr = getThemeString(ctx.state.darkMode);
      setDocumentTheme(themeStr);
    }
  }
};
export default m;
