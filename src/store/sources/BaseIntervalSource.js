// @ts-check
import { set } from "lodash";

/**
 * @brief ticker source updates the store every "interval"
 */
export default class BaseIntervalSource extends EventTarget {
  /**
   * @param {V.Store<BaseVue.StoreState>} [store]
   * @param {string} [namespace] store namespace
   * @param {number} [interval] interval in milliseconds
   */
  constructor(store, namespace, interval) {
    super();
    this.store = store ?? null;
    this.namespace = namespace;
    /** @type {number} */
    this.interval = 0;
    /** @type {NodeJS.Timeout|null} */
    this._timer = null;
    this._evt = new CustomEvent("tick");
    this.start(interval);
  }

  /**
   * @brief start or restart the timer
   * @param  {number} [interval] interval in milliseconds
   */
  start(interval) {
    if (this._timer) {
      clearInterval(this._timer);
    }
    this.interval = interval ?? 0;
    this._commit("reset");
    this._timer = this.interval ?
      setInterval(() => this.tick(), interval) : null;
  }

  /**
   * @brief stop timer
   */
  stop() { this.start(0); }

  /**
   * @brief release resources
   */
  destroy() {
    const store = this.store;
    this.store = null; // prevent any store interaction
    this.stop();

    if (store && this.namespace) {
      // @ts-ignore
      delete store.sources[this.namespace];
      store.unregisterModule(this.namespace.split("/"));
    }
  }

  /**
   * commit in the store
   * @param  {string} method
   * @param  {any} [arg]
   */
  _commit(method, arg) {
    this.store?.commit(this.namespace ? `${this.namespace}/${method}` : method,
      arg);
  }

  tick() {
    this._commit("tick");
    this.dispatchEvent(this._evt);
  }

  /**
   * @brief remove plugin
   * @param {V.Store<BaseVue.StoreState>} store
   * @param {string} namespace
   * @param {number} [interval]
   */
  static register(store, namespace, interval) {
    // @ts-ignore
    store.sources?.[namespace]?.destroy();
    store.registerModule(namespace.split("/"), {
      namespaced: true,
      state: { count: 0 },
      mutations: {
        reset(state) { state.count = 0; },
        tick(state) { state.count += 1; }
      }
    });
    set(store, [ "sources", namespace ], new BaseIntervalSource(store, namespace, interval));
  }

  /**
   * @brief ticker plugin register module and source
   * @details this is mainly there as an example implementation
   * @param {string} namespace
   * @param {number} [interval]
   * @return {(store: V.Store<BaseVue.StoreState>) => void}
   */
  static plugin(namespace, interval) {
    return function(store) {
      return BaseIntervalSource.register(store, namespace, interval);
    };
  }
}
