// @ts-check
import logger from "../../Logger/BaseLogger";
import axios from "axios";
import { currentUrl } from "../../utils";
import { invoke } from "lodash";

export default class UserSource {
  /**
   * @param {V.Store<BaseVue.StoreState>} [store]
   */
  constructor(store) {
    this.store = store;
    this.fetch();
  }

  /**
   * @param {V.Store<BaseVue.StoreState>} [store]
   */
  setStore(store) {
    this.store = store;
    if (this.store) {
      this.store.commit("user", null);
    }
    this.fetch();
  }

  fetch() {
    if (!this.store) { return; }
    this.prom = axios.get(currentUrl() + "/auth/me")
    .then(
      (ret) => invoke(this.store, "commit", "user", ret.data),
      () => logger.error("User not authenticated"));
  }

  destroy() {}
}
