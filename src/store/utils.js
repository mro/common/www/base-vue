// @ts-check
import { get } from "lodash";
import { createStore, getStore } from "./index";
import { sync } from "vuex-router-sync";
import d from "debug";
import { mixinMaker } from "../utils";

const debug = d("base:store");


export const BaseStoreMixin = mixinMaker({
  beforeCreate() {
    const baseStore = createStore();
    if (!baseStore?.state?.route && this.$router) {
      debug("registering route in store");
      sync(baseStore, this.$router);
    }
  },
  methods: {
    /**
     * @return {V.Store<BaseVue.StoreState>|undefined}
     */
    getBaseStore() {
      return getStore();
    },
    /**
     * @param  {string|string[]} path
     * @return {any}
     */
    getBaseState(path) {
      const state = getStore()?.state;
      return get(state, path);
    },
    /**
     * @param  {string|string[]} path
     * @return {any}
     */
    getBaseGetter(path) {
      const getters = getStore()?.getters;
      return get(getters, path);
    },
    /**
     * @param  {string} path
     * @param  {any} value
     * @return {any}
     */
    commitBase(path, value) {
      return getStore()?.commit?.(path, value);
    },
    /**
     * @param  {string} path
     * @return {any}
     */
    dispatchBase(path) {
      return getStore()?.dispatch?.(path);
    }
  }
});
