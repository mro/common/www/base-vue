// @ts-check

import { get, isEmpty, isNil, random, replace } from "lodash";
import { DateTime } from "luxon";
let id = 0;

/**
 * @returns {string}
 */
export function genId() {
  return "b-" + (++id);
}

/** @type {string} */
var _currentUrl = window.location.origin + window.location.pathname;

/**
 * @return {string}
 */
export function currentUrl() {
  return _currentUrl;
}

/**
 * @param {string} url
 */
export function setCurrentUrl(url) {
  _currentUrl = url;
}

/**
 * @brief get webpack public path ('/dist')
 * @param  {string} url
 * @return {string}
 */
export function publicPath(url) {
  /* eslint-disable-next-line camelcase */ /* @ts-ignore */
  if (typeof __webpack_public_path__ !== "undefined") {
    /* eslint-disable-next-line camelcase */ /* @ts-ignore */
    return __webpack_public_path__ + url;
  }
  return url;
}

const templateRegex = /{([^\}]*)}/g;
/**
 * @brief compile a string template
 * @details can't use "template" from lodash
 * (with { interpolate: /{([^\}]*)}/g }), since it uses eval.
 * @details uses "{word}" for parts to be replaced
 * @param  {string} source format string
 * @param  {{ [key:string]: any }} repl replacement object
 * @param  {boolean} keep whereas to keep unmatched braces
 * @return {string}
 */
export function format(source, repl, keep = false) {
  return replace(source, templateRegex, function(match, key) {
    return get(repl, key, keep ? match : "");
  });
}

/**
 * @param {number} timestamp
 * @param {luxon.DateTimeOptions} [options]
 * @return {DateTime}
 */
export function getDateTime(timestamp, options = undefined) {
  if (timestamp > 32503676400) { /* year 3000 --> ms timestamp */
    return DateTime.fromMillis(timestamp, options);
  }
  else {
    return DateTime.fromSeconds(timestamp, options);
  }
}

export const utils = {
  get, isEmpty, isNil, currentUrl, setCurrentUrl, publicPath, format, random,
  getDateTime
};

/**
 * @param {Vue.VueConstructor<any>} Vue
 */
function installUtils(Vue) {
  // @ts-ignore
  Vue.prototype.$butils = utils;
}

export default {
  install: installUtils
};

/**
 * @brief function used to automatically and correctly infer types in mixins
 * @template V, Data, Methods, Computed, PropNames, SetupBindings, Mixin, Extends
 * @type {V.mixinMaker<V, Data, Methods, Computed, PropNames, SetupBindings, Mixin, Extends>}}
 */
export function mixinMaker(m) { return /** @type {any} */ (m); }
