/* must be done early */
import debug from "debug";
// @ts-ignore
debug.useColors = () => false;
// @ts-ignore: injected by webpack
if (typeof DEBUG !== "undefined") {
  // @ts-ignore: injected by webpack
  localStorage["debug"] = DEBUG;
}

const
  { before } = require("mocha"),
  server = require("@cern/karma-server-side"),
  chai = require("chai"),
  dirtyChai = require("dirty-chai");

import BaseVue from "../src";
import Vue from "vue";

chai.use(dirtyChai);

before(function() {
  Vue.use(BaseVue);
  return server.run(function() {
    // add a guard to not add one event listener per test
    if (!this.karmaInit) {
      // $FlowIgnore
      this.karmaInit = true;
      /* do not accept unhandledRejection */
      process.on("unhandledRejection", function(reason) {
        console.log("UnhandledRejection:", reason);
        throw reason;
      });
    }
  });
});
