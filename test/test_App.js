// @ts-check

import { afterEach, describe, it } from "mocha";
import { expect } from "chai";
import { mount } from "@vue/test-utils";

import { BaseCard } from "../src";
import { createRouter, waitForWrapper } from "./utils";

describe("App", function() {
  /** @type {Tests.Wrapper} */
  var wrapper;

  afterEach(function() {
    if (wrapper) {
      wrapper.destroy();
      /* @ts-ignore */
      wrapper = null;
    }
  });

  it("can mount a WebSite without router", async () => {
    wrapper = mount({ template: '<BaseWebSite><BaseCard><li key="1">content</li></BaseCard></BaseWebSite>' });
    await waitForWrapper(
      () => wrapper.findComponent({ name: "BaseCard" }));
  });

  it("can mount a WebSite with router", async () => {
    const router = createRouter("/", {
      routes: [ { name: "Home", path: "/", component: BaseCard } ] });

    wrapper = mount({ template: "<BaseWebSite />" }, { router });
    // wait for router to settle
    await waitForWrapper(
      () => wrapper.findComponent({ name: "BaseCard" }));

    const items = await waitForWrapper(
      () => wrapper.findAllComponents({ name: "BaseNavBarItem" }));
    expect(items).to.have.length(1);
    // @ts-ignore
    expect(items.at(0).vm.active).to.equal(true);
  });

  it("can mount a WebApp without router", async () => {
    wrapper = mount({ template: '<BaseWebApp><BaseCard><li key="1">content</li></BaseCard></BaseWebApp>' });
    await waitForWrapper(
      () => wrapper.findComponent({ name: "BaseCard" }));
  });

  it("can mount a WebApp with router", async () => {
    const router = createRouter("/", {
      routes: [ { name: "Home", path: "/", component: BaseCard } ] });

    wrapper = mount({ template: "<BaseWebApp />" }, { router });
    // wait for router to settle
    await waitForWrapper(
      () => wrapper.findComponent({ name: "BaseCard" }));

    const items = await waitForWrapper(
      () => wrapper.findAllComponents({ name: "BaseSideBarItem" }));
    expect(items).to.have.length(1);
    expect(items.at(0).vm).to.have.property("active", true);
  });
});
