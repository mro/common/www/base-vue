// @ts-check

import {
  wait,
  waitFor
} from "./utils";

import _ from "lodash";
import { afterEach, describe, it } from "mocha";

import { expect } from "chai";
import { mount } from "@vue/test-utils";

describe("BaseAnimationBlock", function() {
  /** @type {Tests.Wrapper} */
  var wrapper;

  afterEach(function() {
    if (wrapper) {
      wrapper.destroy();
      /* @ts-ignore */
      wrapper = null;
    }
  });

  it("can delay item removal", async function() {
    wrapper = mount({
      data: () => ({ expand: true }),
      template: `<BaseCollapsible :expand='expand'>
        <template v-slot:default='c'>
          <BaseAnimationBlock anim="delay" :appear='false' mode='' :options='{ outDelay: 0.3 }'>
            <div v-if="c.isExpanded">body={{ c.isExpanded }}</div>
          </BaseAnimationBlock>
        </template>
      </BaseCollapsible>`
    }, { stubs: { "transition-group": false, transition: false } });

    await waitFor(() => _.includes(wrapper.text(), "body=true"));
    wrapper.setData({ expand: false });
    await wait(50);
    expect(wrapper.text()).to.contain("body=true",
      "body should still be visible");
    await waitFor(() => (wrapper.text() === ""),
      "body should have been removed");
  });

  it("can animate a component", async function() {
    /* ensures animation engine is working */
    wrapper = mount({
      data: () => ({ expand: true }),
      template: `
        <BaseAnimationBlock anim="fade" :appear='true' :options='{ duration: 0.3 }'>
          <div style="opacity: 0;">body</div>
        </BaseAnimationBlock>`
    }, { stubs: { "transition-group": false, transition: false } });
    await waitFor(() => _.includes(wrapper.text(), "body"));
    await waitFor(() => /** @type {HTMLElement} */(wrapper.element).style.opacity === "1");
  });
});
