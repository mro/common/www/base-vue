// @ts-check

import {
  stubs,
  waitFor } from "./utils";

import { afterEach, describe, it } from "mocha";
import { expect } from "chai";
import { mount } from "@vue/test-utils";

import { BaseCard as Card } from "../src";

describe("BaseCard", function() {
  /** @type {Tests.Wrapper} */
  var wrapper;

  afterEach(function() {
    if (wrapper) {
      wrapper.destroy();
      /* @ts-ignore */
      wrapper = null;
    }
  });

  it("can mount a Card", async function() {
    wrapper = mount(Card, { propsData: { title: "test title" }, stubs });

    await waitFor(() => wrapper.find(".card-header").exists());
    const header = wrapper.find(".card-header");
    expect(header.text()).to.equal("test title");
    wrapper.setProps({ title: "" });
    await waitFor(() => !wrapper.find(".card-header").exists());
  });

  it("can toggle footer", async function() {
    wrapper = mount({
      components: { Card },
      template: `<Card>
          <template v-if="footer" v-slot:footer>footer content</template>
        </Card>`,
      data: () => ({ footer: true })
    }, { stubs });

    await waitFor(() => wrapper.find(".card-footer").exists());
    expect(wrapper.find(".card-footer").text()).to.equal("footer content");
    wrapper.setData({ footer: false });
    await waitFor(() => !wrapper.find(".card-footer").exists(), undefined, 1500);
    wrapper.setData({ footer: true });
    await waitFor(() => wrapper.find(".card-footer").exists());
  });

  it("can display content", async function() {
    wrapper = mount({
      components: { Card },
      template: `<Card>
          <li v-if="body" key="0">some content</li>
        </Card>`,
      data: () => ({ body: true })
    }, { stubs });

    await waitFor(() => wrapper.find("ul > li").exists());
    expect(wrapper.find("ul > li").text()).to.equal("some content");
    wrapper.vm.body = false;
    await waitFor(() => !wrapper.find("ul > li").exists());
    wrapper.vm.body = true;
    await waitFor(() => wrapper.find("ul > li").exists());
  });
});
