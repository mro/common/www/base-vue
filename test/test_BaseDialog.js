// @ts-check

import "./karma_index";
import { waitFor, waitForValue, waitForWrapper } from "./utils";
import { expect } from "chai";
import { afterEach, describe, it } from "mocha";
import { mount } from "@vue/test-utils";

import { BaseDialog } from "../src";

describe("BaseDialog", function() {
  /** @type {Tests.Wrapper} */
  let wrapper;

  afterEach(function() {
    if (wrapper) {
      wrapper.destroy();
      // @ts-ignore
      wrapper = null;
    }
  });

  it("can use a dialog", async function() {
    wrapper = mount(BaseDialog, { propsData: { title: "test dialog" } });

    var prom = wrapper.vm.request();
    var button = await waitForWrapper(
      () => wrapper.findAll("button").filter((b) => b.text() === "Ok"));
    button.trigger("click");
    expect(await prom).to.equal(true);

    prom = wrapper.vm.request();
    button = await waitForWrapper(
      () => wrapper.findAll("button").filter((b) => b.text() === "Cancel"));
    button.trigger("click");
    expect(await prom).to.equal(false);

    prom = wrapper.vm.request();
    wrapper.destroy();
    // @ts-ignore
    wrapper = null;
    await prom.then(
      () => { throw new Error("should fail"); },
      (/** @type {Error} */ err) => expect(err.message).to.equal("destroyed"));
  });

  it("fires an event when shown and when hidden", async function() {
    wrapper = mount(BaseDialog, { propsData: { title: "test dialog" } });

    // open dialog (hidden = false)
    let prom = waitFor(() => wrapper.emitted("hidden"),
      "Failed to catch the first 'hidden' event");
    wrapper.vm.request();

    expect(await prom).to.have.property("length", 1);
    expect(wrapper.emitted("hidden")?.[0]).to.be.deep.equal([ false ]);

    // close dialog (hidden = true)
    var button = await waitForWrapper(
      () => wrapper.findAll("button").filter((b) => b.text() === "Ok"));

    prom = waitForValue(() => wrapper.emitted("hidden")?.length, 2,
      "Failed to catch the second 'hidden' event");
    button.trigger("click");

    await prom;
    expect(wrapper.emitted("hidden")?.[1]).to.be.deep.equal([ true ]);
  });

  it("has keyboard navigation when using default buttons", async function() {
    wrapper = mount(BaseDialog, { propsData: { title: "test dialog" } });

    var prom = wrapper.vm.request();
    await waitForWrapper(
      () => wrapper.findAll("button").filter((b) => b.text() === "Ok"));
    wrapper.trigger("keyup", { key: "enter" });
    expect(await prom).to.equal(true);

    prom = wrapper.vm.request();
    await waitForWrapper(
      () => wrapper.findAll("button").filter((b) => b.text() === "Cancel"));
    wrapper.trigger("keyup", { key: "esc" });
    expect(await prom).to.equal(false);
  });

  it("disables keyboard navigation when not using default buttons", async function() {
    wrapper = mount({
      template: "<BaseDialog ref='dialog'><template #footer='d'><button class='btn btn-danger' @click='d.resolve(\"ok\")'>Ok</button></template></BaseDialog>"
    });

    var prom = wrapper.vm.$refs.dialog.request();
    var button = await waitForWrapper(
      () => wrapper.findAll("button").filter((b) => b.text() === "Ok"));
    wrapper.trigger("keyup", { key: "esc" });
    button.trigger("click");
    expect(await prom).to.equal("ok");
  });
});
