// @ts-check

import { waitFor } from "./utils";

import { isNumber, toNumber } from "lodash";
import { afterEach, beforeEach, describe, it } from "mocha";
import { mount } from "@vue/test-utils";
import server from "@cern/karma-server-side";

describe("BaseEventSource", function() {
  /** @type {Tests.Wrapper} */
  var wrapper;
  /** @type {{ addr: { port: number } }} */
  var env = { addr: { port: 0 } };

  beforeEach(function() {
    // @ts-ignore
    return server.run(function() {
      const sse = serverRequire("./example/server/express-sse");
      const cors = serverRequire("cors");
      this.app = serverRequire("express")();

      // @ts-ignore
      this.app.get("/sse", cors(), sse, (req, res) => {
        if (res.sse) {
          var counter = 0;
          const interval = setInterval(
            () => res.sse((++counter).toString()), 100);
          res.on("close", () => clearInterval(interval));
        }
        else {
          res.send();
        }
      });
      return new Promise((resolve) => {
        this.server = this.app.listen(0, () => resolve(this.server.address()));
      });
    })
    .then((/** @type {{ port: number }}} */ addr) => { env.addr = addr; });
  });

  afterEach(function() {
    return server.run(function() {
      // @ts-ignore
      this.server.close();
      this.app = null;
      this.server = null;
    });
  });

  afterEach(function() {
    if (wrapper) {
      wrapper.destroy();
      /* @ts-ignore */
      wrapper = null;
    }
  });

  it("can connect", async function() {
    wrapper = mount({
      template: `<BaseEventSource src='http://localhost:${env.addr.port}/sse'>
        <template v-slot:default='es'>{{ es.message }}</template>
      </BaseEventSource>`
    });

    await waitFor(() => (toNumber(wrapper.text()) > 1));
  });

  it("can use events", async function() {
    wrapper = mount({
      props: { src: { type: String, default: `http://localhost:${env.addr.port}/sse` } },
      data: () => { return { message: null, error: null }; },
      template: `<BaseEventSource @message='message=$event.data' @error='error=$event' :src='src'>
        <template v-slot:default='es'>{{ es.message }}</template>
      </BaseEventSource>`
    });

    await waitFor(() => (toNumber(wrapper.text()) > 1));
    await waitFor(() => (toNumber(wrapper.vm.message) > 1));

    wrapper.setProps({ src: "invalid" });
    await waitFor(() => (wrapper.vm.error !== null));
  });

  it("can parse message events as json", async function() {
    wrapper = mount({
      props: { src: { type: String, default: `http://localhost:${env.addr.port}/sse` } },
      data: () => { return { message: null }; },
      template: `<BaseEventSource @message='message=$event' :json='true' :src='src'>
        <template v-slot:default='es'>{{ es.message }}</template>
      </BaseEventSource>`
    });

    await waitFor(() => isNumber(wrapper.vm.message));
  });

  it("may have no content", async function() {
    wrapper = mount({
      props: { src: { type: String, default: `http://localhost:${env.addr.port}/sse` } },
      data: () => { return { message: null }; },
      template: "<BaseEventSource @message='message=$event' :json='true' :src='src'/>"
    });

    await waitFor(() => (toNumber(wrapper.vm.message) > 1));
  });
});
