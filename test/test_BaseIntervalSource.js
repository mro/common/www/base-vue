// @ts-check

import { expect } from "chai";
import { describe, it } from "mocha";
import { BaseIntervalSource } from "../src";
import Vuex from "vuex";

import { waitFor } from "./utils";

/**
 * @typedef {{ ticker: BaseIntervalSource }} Sources
 */

/*
 * to properly register sources in an application, use:
 * declare module "@cern/base-vue" {
 *   declare interface StoreState {
 *     areaMap: { [name: string]: string }
 *   }
 *   declare interface StoreSources {
 *     sampleExchanger: import('./sources/SampleExchanger').default
 *   }
 * }
 */

describe("BaseIntervalSource", function() {
  it("can create a BaseIntervalSource", async function() {
    /** @type {V.Store<{ ticker: BaseVue.BaseIntervalState }>} */
    const store = new Vuex.Store({
      plugins: [ BaseIntervalSource.plugin("ticker", 50) ]
    });
    expect(store.hasModule("ticker")).to.equal(true);

    await waitFor(() => store.state.ticker?.count >= 2, "counter not ticking");
    /** @type {BaseIntervalSource} */
    const ticker = /** @type {Sources} */ (store.sources).ticker;
    expect(ticker).to.exist();

    ticker.stop();
    await waitFor(() => (store.state.ticker?.count === 0), "counter not reset");
    ticker.destroy();
    expect(store.hasModule("ticker")).to.equal(false);
  });
});
