// @ts-check
import { after, before, describe, it } from "mocha";
import { expect } from "chai";

import _ from "lodash";
import Vue from "vue";
import { mount } from "@vue/test-utils";
import { default as BaseKeyboardEventMixin, KeyboardEventHandler,
  _globalListener } from "../src/mixins/BaseKeyboardEventMixin";

/**
 * @param  {any} refObj
 * @param  {any} keyCode
 * @param  {any} modMask
 * @param  {any} eventType
 */
function checkKeyParsing(refObj, keyCode, modMask, eventType) {
  return refObj.keyCode === keyCode &&
    refObj.modMask === modMask &&
    refObj.eventType === eventType;
}

describe("KeyboardEventHandler", () => {

  it("Initial state and basic setter and getter with default constructor", () => {
    const keh = new KeyboardEventHandler();
    expect(_.isEmpty(keh)).to.equal(false);
    expect(_.isEmpty(keh.listeners)).to.equal(true);
    expect(keh.target).to.equal(window.document);
    expect(keh._keyupAttached).to.equal(false);
    expect(keh._keydownAttached).to.equal(false);
    expect(keh.checkOnInputs).to.equal(true);
    keh.setCheckOnInput(false);
    expect(keh.checkOnInputs).to.equal(false);
  });

  it("Test parameters on constructor", () => {
    const fakeTarget = { fake: true };
    const keh = new KeyboardEventHandler(/** @type {any} */ (fakeTarget), false);
    expect(keh.target).to.equal(fakeTarget);
    expect(keh.checkOnInputs).to.equal(false);
    keh.setCheckOnInput(true);
    expect(keh.checkOnInputs).to.equal(true);
  });

  it("Test parseKey and genKeyHash", () => {
    const keh = new KeyboardEventHandler();
    let op = keh.parseKey("e");
    expect(checkKeyParsing(op, "e", 0, "keyup")).to.equal(true);
    op = keh.parseKey("e-keyup");
    expect(checkKeyParsing(op, "e", 0, "keyup")).to.equal(true);
    op = keh.parseKey("shift-e-keyup");
    expect(checkKeyParsing(op, "e", 4, "keyup")).to.equal(true);
    op = keh.parseKey("shift-e-keydown");
    expect(checkKeyParsing(op, "e", 4, "keydown")).to.equal(true);
    op = keh.parseKey("ctrl-alt-w");
    expect(checkKeyParsing(op, "w", 3, "keyup")).to.equal(true);
    op = keh.parseKey("ctrl-alt-w-keydown");
    expect(checkKeyParsing(op, "w", 3, "keydown")).to.equal(true);
    op = keh.parseKey("meta");
    expect(checkKeyParsing(op, "meta", 0, "keyup")).to.equal(true);
    op = keh.parseKey("shift-up");
    expect(checkKeyParsing(op, "up", 4, "keyup")).to.equal(true);
    op = keh.parseKey("shift-down");
    expect(checkKeyParsing(op, "down", 4, "keyup")).to.equal(true);
    // I would say one test is enough for a simple concat.
    expect(keh.genKeyHash(op.keyCode, op.modMask, op.eventType)).to.equal("down-4-keyup");
  });

  it("Test getCallBacks, addCallBack, removeCallBack", () => {
    const keh = new KeyboardEventHandler();
    const fakeCb = () => true;
    const fakeCb2 = () => false;

    expect(_.isEmpty(keh.listeners)).to.equal(true);
    const key = "e";
    const { keyCode, modMask, eventType } = keh.parseKey(key);
    const keyHash = keh.genKeyHash(keyCode, modMask, eventType);
    expect(_.isEmpty(keh.getCallBacks(keyHash))).to.equal(true);
    keh.addCallBack(key, fakeCb);
    expect(_.isEmpty(keh.getCallBacks(keyHash))).to.equal(false);
    expect(keh.getCallBacks(keyHash).length).to.equal(1);

    // Should not delete the cb
    keh.removeCallBack(key, fakeCb2);
    expect(keh.getCallBacks(keyHash).length).to.equal(1);

    // Should delete the cb
    keh.removeCallBack(key, fakeCb);
    expect(keh.getCallBacks(keyHash).length).to.equal(0);

    // Should add only one cb
    keh.addCallBack(key, fakeCb);
    keh.addCallBack(key, fakeCb);
    expect(keh.getCallBacks(keyHash).length).to.equal(1);

    // Should add the cb if cb is different
    keh.addCallBack(key, fakeCb2);
    expect(keh.getCallBacks(keyHash).length).to.equal(2);
  });


});


describe("BaseKeyboardEventMixin", () => {
  /** @type {Tests.Wrapper} */
  var wrapper;

  before(function() {
    wrapper = mount(Vue.extend({
      mixins: [ BaseKeyboardEventMixin({}) ],
      /** @return {{ testValue: number }} */
      data() {
        return {
          testValue: 0
        };
      },
      mounted() {
        // remember: keyup or keydown are optional but default is keyup!
        this.onKey("e", this.fireEventE.bind(this)); // key = e-0-keyup
        this.onKey("e-keyup", this.fireEventE2.bind(this)); // key = e-0-keyup
        this.onKey("e", this.fireEventE3.bind(this)); // key = e-0-keyup
        this.onKey("shift-e", this.fireEventShiftE.bind(this)); // key = e-4-keyup

        // test to check if multiple cbs are removed with a single key
        // it seems cbs are the same but actually the are not.
        this.onKey("w", () => true);
        this.onKey("w", () => true);
        this.onKey("w", () => true);
        this.removeKey("w");
      },
      methods: {
        fireEventE() {
          this.testValue = "fireEvent-E";
        },
        fireEventE2() {
          this.testValue = "fireEvent-E2";
          return false;
        },
        fireEventE3() {
          this.testValue = "fireEvent-E3";
        },
        fireEventShiftE() {
          this.testValue = "fireEvent-ShiftE";
        }
      },
      template: "<div><input /></div>"
    }));
  });

  after(function() {
    if (wrapper) {
      wrapper.destroy();
    }
    // @ts-ignore
    wrapper = null;
  });

  it("passes the sanity check and creates a wrapper", () => {
    expect(!!wrapper.vm).to.be.equal(true);
  });

  it("check everything is registered on the GlobalListener", () => {
    /* $FlowIgnore: _globalListener is initialized */
    expect(_globalListener.getCallBacks("e-0-keyup").length).to.equal(3);
    /* $FlowIgnore: _globalListener is initialized */
    expect(_globalListener.getCallBacks("e-4-keyup").length).to.equal(1);
  });

  it("Check multiple callbacks are removed when cb is not passed by parameter", () => {
    /* $FlowIgnore: _globalListener is initialized */
    expect(_globalListener.getCallBacks("w-0-keyup").length).to.equal(0);
  });

  it("Check callback chain works", () => {
    document.dispatchEvent(new KeyboardEvent("keyup", { key: "e" }));
    expect(wrapper.vm.testValue).to.equal("fireEvent-E2"); // fireEventE3 is ignored because of return false
  });

  it("Check combo key shift + key", () => {
    document.dispatchEvent(new KeyboardEvent("keyup", { key: "e", shiftKey: true }));
    expect(wrapper.vm.testValue).to.equal("fireEvent-ShiftE");
  });

  // This MUST be the last test
  it("Check callbacks are removed after destroy", () => {
    wrapper.destroy();
    /* $FlowIgnore: _globalListener is initialized */
    expect(_globalListener.getCallBacks("e-0-keyup").length).to.equal(0);
    /* $FlowIgnore: _globalListener is initialized */
    expect(_globalListener.getCallBacks("e-4-keyup").length).to.equal(0);
  });
});
