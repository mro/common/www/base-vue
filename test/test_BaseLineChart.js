// @ts-check

import { stubs, waitFor, waitForValue } from "./utils";

import { times } from "lodash";
import { afterEach, describe, it } from "mocha";
import { mount } from "@vue/test-utils";

import { BaseLineChart } from "../src";

describe("BaseLineChart", function() {
  /** @type {Tests.Wrapper} */
  var wrapper;

  afterEach(function() {
    if (wrapper) {
      wrapper.destroy();
      /* @ts-ignore */
      wrapper = null;
    }
  });

  it("can mount", async function() {
    wrapper = mount(BaseLineChart,
      { propsData: { data: [ 1, 2, 3, 4 ] }, stubs, attachTo: document.body });

    await waitFor(() => wrapper.find(".b-line-chart").exists());
    await waitForValue(
      () => wrapper.findAll("g.chart-body circle.dot").length,
      4, "graph points not found");

    wrapper.vm.add(5);
    await waitForValue(
      () => wrapper.findAll("g.chart-body circle.dot").length,
      5, "graph points not added");
  });

  it("can mount a multi-graph", async function() {
    wrapper = mount(BaseLineChart,
      { propsData: {
        data: [
          { key: 0, value1: 1, value2: 2 },
          { key: 1, value1: 2, value2: 3 },
          { key: 2, value1: 4, value2: 5 }
        ],
        xAxis: "key", yAxis: [ "value1", "value2" ],
        smooth: false, isTimestamp: true
      }, stubs, attachTo: document.body });

    await waitFor(() => wrapper.find(".b-line-chart").exists());
    await waitForValue(
      () => wrapper.findAll("g.chart-body circle.dot").length,
      6, "graph points not found");

    wrapper.vm.add({ key: 3, value1: 6, value3: 7 });
    await waitForValue(
      () => wrapper.findAll("g.chart-body circle.dot").length,
      8, "graph points not added");
  });

  it("can limit its content", async function() {
    wrapper = mount(BaseLineChart,
      { propsData: { maxData: 10, smooth: false }, stubs,
        attachTo: document.body });

    await waitFor(() => wrapper.find(".b-line-chart").exists());
    wrapper.vm.add({ key: 0, value: 1 });
    await waitForValue(
      () => wrapper.findAll("g.chart-body circle.dot").length,
      1, "graph points not found");

    wrapper.vm.add(times(20, (n) => ({ key: n + 1, value: 42 })));
    await waitForValue(
      () => wrapper.findAll("g.chart-body circle.dot").length,
      10, "wrong points count");
  });
});
