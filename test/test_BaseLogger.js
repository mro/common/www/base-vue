// @ts-check

import { afterEach, describe, it } from "mocha";
import { expect } from "chai";
import { createLocalVue, mount } from "@vue/test-utils";
import { isElementVisible } from "./utils.js";
import { createStore } from "../src/store/index.js";
import Vue from "vue";

import {
  default as BaseVue,
  BaseErrorAlert as ErrorAlert,
  BaseErrorReport as ErrorReport,
  BaseLogger as logger } from "../src";
import { waitForValue } from "./utils";

describe("BaseLogger", function() {
  /** @type {Tests.Wrapper} */
  var wrapper;

  afterEach(function() {
    if (wrapper) {
      wrapper.destroy();
      /* @ts-ignore */
      wrapper = null;
    }
  });

  it("can log errors", function() {
    logger.clear();
    expect(logger.getErrors()).to.have.length(0);
    logger.error("test");
    logger.error(new Error("another"));
    logger.warning("test");
    expect(logger.getErrors()).to.have.length(3);
    logger.clear();
    expect(logger.getErrors()).to.have.length(0);
  });

  it("displays alerts on error", async function() {
    logger.clear();
    const localVue = createLocalVue();
    localVue.use(BaseVue);
    wrapper = mount(ErrorAlert, { localVue });

    logger.error("test");
    logger.error(new Error("another"));
    await waitForValue(() => wrapper.findAll(".alert").length, 2);
  });

  it("can mute alerts", async function() {
    createStore().commit("ui/update", { muteAlerts: false });
    const localVue = createLocalVue();
    localVue.use(BaseVue);
    wrapper = mount(Vue.extend({
      name: "LoggerTest",
      components: { ErrorAlert, ErrorReport },
      template: `<div>
        <ErrorAlert/>
        <ErrorReport/>
      </div>`
    }), {
      localVue,
      attachTo: document.body,
      stubs: { "transition-group": false, transition: false }
    });


    logger.clear();
    logger.error("test error");
    logger.warning("test warning");

    await waitForValue(() => wrapper.findAll(".alert").length, 2, "no alerts found");

    const alertMuteBtn = wrapper.find(".b-alert-container .x-mute-btn");
    let alert = wrapper.find(".alert");
    await waitForValue(() => isElementVisible(alert), true, "should be visible");

    // mute alerts (from Error Alert component)
    await alertMuteBtn.trigger("click");
    await waitForValue(() => isElementVisible(alert), false, "should be hidden");

    // unmute alerts (from Error Alert component)
    await alertMuteBtn.trigger("click");

    logger.clear();
    logger.error("test error");
    logger.warning("test warning");

    await waitForValue(() => wrapper.findAll(".alert").length, 2);

    const reportMuteBtn = wrapper.find(".b-error-report .x-mute-btn");
    alert = wrapper.find(".alert");

    await waitForValue(() => isElementVisible(alert), true, "should be visible again");

    // mute alerts (from Error Report component)
    await reportMuteBtn.trigger("click");

    await waitForValue(() => isElementVisible(alert), false, "should be hidden again");
  });

  it("displays an error report", async function() {
    logger.clear();

    const localVue = createLocalVue();
    localVue.use(BaseVue);
    wrapper = mount(ErrorReport, { localVue });

    logger.error("test");
    logger.error(new Error("another"));
    await waitForValue(() => wrapper.findAll(".text-danger").length, 2);
  });

  it("it saves errors on localStorage", async function() {
    logger.clear();
    expect(logger.getErrors()).to.have.length(0);
    logger.error("test");
    logger.error("test2");
    logger.warning("test");
    expect(logger.getErrors()).to.have.length(3);

    expect(logger.getErrors()) // @ts-ignore internal property
    .to.deep.equal(JSON.parse(localStorage.getItem(logger._field) || "[]"));
    logger.clear();
    expect(logger.getErrors()) // @ts-ignore internal property
    .to.deep.equal(JSON.parse(localStorage.getItem(logger._field) || "[]"));
  });

  it("displays report with errors loaded from localStorage", async function() {
    logger.clear();
    logger.error("test");
    logger.error("test2");
    logger.error("test3");

    const localVue = createLocalVue();
    localVue.use(BaseVue);
    wrapper = mount(ErrorReport, { localVue });

    await waitForValue(() => wrapper.findAll(".text-danger").length, 3);

    // Clear Errors
    wrapper.find(".modal-footer .x-clean-btn").trigger("click");
    await waitForValue(() => wrapper.findAll(".text-danger").length, 0);
    expect(logger.getErrors()) // @ts-ignore internal property
    .to.deep.equal(JSON.parse(localStorage.getItem(logger._field) || "[]"));
  });
});
