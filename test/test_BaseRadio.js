// @ts-check

import { afterEach, describe, it } from "mocha";
import { expect } from "chai";
import { mount } from "@vue/test-utils";

import { BaseRadio } from "../src";
import { stubs, waitForValue } from "./utils";

describe("BaseRadio", function() {
  /** @type {Tests.Wrapper} */
  var wrapper;

  afterEach(function() {
    if (wrapper) {
      wrapper.destroy();
      /* @ts-ignore */
      wrapper = null;
    }
  });

  it("can mount a BaseRadio", async function() {
    wrapper = mount(BaseRadio, {
      propsData: { label: "Test Param", name: "group1", value: false },
      stubs
    });

    expect(wrapper.vm.editValue).to.equal(false);
    wrapper.setProps({ inEdit: true });

    await wrapper.vm.$nextTick();
    wrapper.find("input").trigger("click");
    expect(wrapper.vm.editValue).to.equal(true);
    wrapper.setProps({ inEdit: false, value: true });
    await wrapper.vm.$nextTick();
    expect(wrapper.find("input")).to.have.nested.property("element.checked", true);
  });

  it("notifies about changes", async function() {
    wrapper = mount(BaseRadio, {
      propsData: { value: false, name: "group1" },
      stubs
    });

    wrapper.setProps({ inEdit: true });
    await wrapper.vm.$nextTick();
    wrapper.find("input").trigger("click");
    await wrapper.vm.$nextTick();
    expect(wrapper.emitted("edit")).to.deep.equal([ [ false ], [ true ] ]);
  });

  it("supports groups", async function() {
    wrapper = mount({
      template: `<div>
        <BaseRadio ref='toggle1' :inEdit='inEdit' v-model='toggle1' name='group' />
        <BaseRadio ref='toggle2' :inEdit='inEdit' v-model='toggle2' name='group' />
      </div>`,
      props: { inEdit: { type: Boolean, default: false } },
      data() { return { toggle1: false, toggle2: true }; }
    }, { stubs });

    wrapper.setProps({ inEdit: true });
    await wrapper.vm.$nextTick();
    wrapper.findComponent({ ref: "toggle1" }).find("input").trigger("click");
    expect(wrapper.vm.$refs.toggle1).ok();

    await waitForValue(() => wrapper.vm.toggle1, true,
      "failed to set toggle1");
    expect(wrapper.findComponent({ ref: "toggle1" }).find("input").element)
    .to.have.property("checked", true, "grouped input not modified");

    await waitForValue(() => wrapper.vm.toggle2, false,
      "grouped radio not modified");

    wrapper.vm.$refs.toggle2.editValue = true;
    await waitForValue(() => wrapper.vm.toggle1, false,
      "grouped radio not modified (2)");
    expect(wrapper.findComponent({ ref: "toggle1" }).find("input").element)
    .to.have.property("checked", false, "grouped input not modified (2)");
  });
});
