// @ts-check

import { waitFor, waitForValue, waitForWrapper } from "./utils";

import { invoke } from "lodash";
import { afterEach, describe, it } from "mocha";
import { expect } from "chai";
import { mount } from "@vue/test-utils";

import { butils } from "../src";

/**
 * @typedef {typeof import('../src/BaseSSVG.vue').default} BaseSSVG
 */

describe("BaseSSVG", function() {
  /** @type {Tests.Wrapper} */
  var wrapper;

  afterEach(function() {
    if (wrapper) {
      wrapper.destroy();
      /* @ts-ignore */
      wrapper = null;
    }
  });

  it("can mount a SSVG", async function() {
    wrapper = mount({
      template: `<BaseSSVG>
        <state>
          <property name="test" type="number" min="0" max="100">
            <relation query-selector='text' from="0" to="1" />
          </property>
        </state>
        <text>some text</text>
        </BaseSSVG>`
    });

    const ssvg = /** @type {Tests.Wrapper<V.Instance<BaseSSVG>>} */ (await waitForWrapper(
      () => wrapper.findComponent({ name: "BaseSSVG" })));
    await waitForValue(() => ssvg.find("text").text(), "0");
    ssvg.vm.updateState({ test: 100 });
    await waitForValue(() => ssvg.find("text").text(), "1");
    expect(ssvg.vm.getState()).to.deep.equal({ test: 100 });

    ssvg.vm.reload();
    await waitForValue(() => ssvg.find("text").text(), "0");
  });

  it("can bind SSVG", async function() {
    wrapper = mount({
      props: { data: { type: Object, default: null } },
      template: `<BaseSSVG :state='data'>
        <state>
          <property name="test" type="number" min="0" max="100">
            <relation query-selector='text' from="0" to="1" />
          </property>
        </state>
        <text>some text</text>
        </BaseSSVG>`
    });

    const ssvg = /** @type {Tests.Wrapper<V.Instance<BaseSSVG>>} */ (await waitForWrapper(
      () => wrapper.findComponent({ name: "BaseSSVG" })));
    await waitForValue(() => ssvg.find("text").text(), "0");
    wrapper.setProps({ data: { test: 100 } });
    await waitForValue(() => ssvg.find("text").text(), "1");
    expect(ssvg.vm.getState()).to.deep.equal({ test: 100 });
  });

  it("can load an external SSVG", async function() {
    wrapper = mount({
      template: `<BaseSSVG src='${butils.publicPath("img/ssvg-logo.svg")}'><rect /></BaseSSVG>`
    }, { attachTo: document.body });

    const ssvg = /** @type {Tests.Wrapper<V.Instance<BaseSSVG>>} */ (await waitForWrapper(
      () => wrapper.findComponent({ name: "BaseSSVG" })));

    const star = await waitFor(
      () => invoke(ssvg.find("iframe"), [ "element", "contentDocument", "querySelector" ], ".ssvg-star"));
    await waitForValue(() => star.getAttribute("style"),
      "transform: scale(1) rotateX(0deg) rotateZ(0deg) translateZ(0px) translateY(0px);");
    ssvg.vm.updateState({ range: 2 });

    await waitForValue(() => star.getAttribute("style"),
      "transform: scale(0.8) rotateX(60deg) rotateZ(-10deg) translateZ(-20px) translateY(-340px);");
  });
});
