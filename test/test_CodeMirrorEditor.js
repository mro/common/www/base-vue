// @ts-check
/* eslint-disable max-lines */
import _ from "lodash";
import { afterEach, beforeEach, describe, it } from "mocha";
import { expect } from "chai";
import { mount } from "@vue/test-utils";

import { CodeMirrorEditor } from "../src";
import { isElementVisible, waitFor } from "./utils";

/**
 * @details It works for characters A-Z and a-z only
 * @param  {string} char
 * @return {number}
 */
function getKeyCode(char) {
  var keyCode = char.charCodeAt(0);
  if (keyCode > 90) { // 90 is keyCode for 'z'
    return keyCode - 32;
  }
  return keyCode;
}

/**
 * @param  {CodeMirror.Editor} cm
 * @param  {string} txt
 */
function setTextAndSelect(cm, txt) {
  expect(_.isNil(cm)).to.be.false();
  expect(_.isNil(txt)).to.be.false();
  cm.setValue(txt);
  expect(cm.getValue()).to.equal(txt);
  cm.setSelection({ line: 0, ch: 0 }, { line: 0, ch: txt.length });
}

/**
 * @param  {CodeMirror.Editor} cm
 * @param  {string} txt
 * @param  {Tests.Wrapper} button
 */
function selectAndClick(cm, txt, button) {
  expect(_.isNil(button)).to.be.false();
  setTextAndSelect(cm, txt);
  button.trigger("click");
}

describe("CodeMirrorEditor", function() {
  /** @type {Tests.Wrapper} */
  let wrapper;
  /** @type {CodeMirror.Editor} */
  let cm;
  const txt = "example";

  beforeEach(function() {
    wrapper = mount(CodeMirrorEditor, {
      propsData: { value: "" }
    });
    cm = wrapper.vm.get();
  });

  afterEach(function() {
    if (wrapper) {
      wrapper.destroy();
      /* @ts-ignore */
      wrapper = null;
    }
  });

  it("can mount a CodeMirrorEditor (markdown by default)", async function() {
    expect(wrapper.vm.cminstance.getValue()).to.equal("");
    cm.setValue("Test editor");
    expect(wrapper.vm.cminstance.getValue()).to.equal("Test editor");
    expect(wrapper.vm.cminstance.getOption("mode")).to.equal("markdown");
    wrapper.setProps({ value: "**Test editor**" });
    await wrapper.vm.$nextTick();
    expect(wrapper.vm.cminstance.getValue()).to.equal("**Test editor**");

    // Test markdown tools is visible
    await waitFor(() => isElementVisible(wrapper.findComponent({ ref: "toolbar" }).element));
    expect(wrapper.vm.showMarkdownTools).to.be.equal(true);

    // Move to Pug mode
    wrapper.setProps({ options: { mode: "pug" } });
    await waitFor(() => !isElementVisible(wrapper.findComponent({ ref: "toolbar" }).element));
    expect(wrapper.vm.showMarkdownTools).to.be.equal(false);
  });

  it("CodeMirror maxLength", async function() {
    wrapper = mount(CodeMirrorEditor, {
      propsData: { value: "", maxLength: 2 }
    });
    cm = wrapper.vm.get();
    const button = wrapper.find(".fa-heading");
    button.trigger("click");
    button.trigger("click");
    button.trigger("click");
    expect(cm.getValue()).to.equal("##"); // and not ###
  });

  it("Markdown Tools - Header", async function() {
    const button = wrapper.find(".fa-heading");

    // With no selection
    button.trigger("click");
    expect(cm.getValue()).to.equal("# ");

    // With text selected
    selectAndClick(cm, txt, button);
    expect(cm.getValue()).to.equal(`# ${txt}\n`);
  });

  it("Markdown Tools - Bold", async function() {
    const button = wrapper.find(".fa-bold");

    // With no selection
    button.trigger("click");
    expect(cm.getValue()).to.equal("****");

    // With text selected
    selectAndClick(cm, txt, button);
    expect(cm.getValue()).to.equal(`**${txt}**`);

    // Test keyboard shortcut (CTRL-B)
    setTextAndSelect(cm, txt);
    cm.focus();
    cm.getInputField().dispatchEvent(new KeyboardEvent("keydown",
      { key: "b", ctrlKey: true, keyCode: getKeyCode("b") }));
    expect(cm.getValue()).to.equal(`**${txt}**`);
  });

  it("Markdown Tools - Italic", async function() {
    const button = wrapper.find(".fa-italic");

    // With no selection
    button.trigger("click");
    expect(cm.getValue()).to.equal("__");

    // With text selected
    selectAndClick(cm, txt, button);
    expect(cm.getValue()).to.equal(`_${txt}_`);

    // Test keyboard shortcut (CTRL-I)
    setTextAndSelect(cm, txt);
    cm.getInputField().dispatchEvent(new KeyboardEvent("keydown",
      { key: "i", ctrlKey: true, keyCode: getKeyCode("i") }));
    expect(cm.getValue()).to.equal(`_${txt}_`);
  });

  it("Markdown Tools - Strikethrough", async function() {
    const button = wrapper.find(".fa-strikethrough");

    // With no selection
    button.trigger("click");
    expect(cm.getValue()).to.equal("~~~~");

    // With text selected
    selectAndClick(cm, txt, button);
    expect(cm.getValue()).to.equal(`~~${txt}~~`);
  });

  it("Markdown Tools - Unordered List", async function() {
    const button = wrapper.find(".fa-list");
    const buttonIndent = wrapper.find(".fa-indent");
    const buttonOutdent = wrapper.find(".fa-outdent");

    // With no selection
    button.trigger("click");
    expect(cm.getValue()).to.equal("- ");

    // Indent
    buttonIndent.trigger("click");
    expect(cm.getValue()).to.equal("  - ");
    // Outdent
    buttonOutdent.trigger("click");
    expect(cm.getValue()).to.equal("- ");
    // Indent Via Addon
    cm.execCommand("autoIndentMarkdownList");
    expect(cm.getValue()).to.equal("  - ");
    // Outdent Via Addon
    cm.execCommand("autoUnindentMarkdownList");
    expect(cm.getValue()).to.equal("- ");

    // With text selected
    selectAndClick(cm, txt, button);
    expect(cm.getValue()).to.equal(`- ${txt}\n`);

    // Test keyboard shortcut (CTRL-L)
    setTextAndSelect(cm, txt);
    cm.getInputField().dispatchEvent(new KeyboardEvent("keydown",
      { key: "l", ctrlKey: true, keyCode: getKeyCode("l") }));
    expect(cm.getValue()).to.equal(`- ${txt}\n`);
  });

  it("Markdown Tools - Numbered List", async function() {
    const button = wrapper.find(".fa-list-ol");
    const buttonIndent = wrapper.find(".fa-indent");
    const buttonOutdent = wrapper.find(".fa-outdent");

    // With no selection
    button.trigger("click");
    expect(cm.getValue()).to.equal("1. ");

    // Indent
    buttonIndent.trigger("click");
    expect(cm.getValue()).to.equal("  1. ");
    // Outdent
    buttonOutdent.trigger("click");
    expect(cm.getValue()).to.equal("1. ");

    // With text selected
    selectAndClick(cm, txt, button);
    expect(cm.getValue()).to.equal(`1. ${txt}\n`);
  });

  it("Markdown Tools - Task List", async function() {
    const button = wrapper.find(".fa-tasks");
    const buttonIndent = wrapper.find(".fa-indent");
    const buttonOutdent = wrapper.find(".fa-outdent");

    // With no selection
    button.trigger("click");
    expect(cm.getValue()).to.equal("- [ ] ");

    // Indent
    buttonIndent.trigger("click");
    expect(cm.getValue()).to.equal("  - [ ] ");
    // Outdent
    buttonOutdent.trigger("click");
    expect(cm.getValue()).to.equal("- [ ] ");

    // With text selected
    selectAndClick(cm, txt, button);
    expect(cm.getValue()).to.equal(`- [ ] ${txt}\n`);
  });

  it("Markdown Tools - Table", async function() {
    const button = wrapper.find(".fa-table");

    // With no selection
    button.trigger("click");
    expect(cm.getValue()).to.equal(
      "\n| header | header |\n" +
        "| ------ | ------ |\n" +
        "| cell | cell |\n" +
        "| cell | cell |\n");
  });

  it("Markdown Tools - Quote", async function() {
    const button = wrapper.find(".fa-quote-right");

    // With no selection
    button.trigger("click");
    expect(cm.getValue()).to.equal("> ");

    // With text selected
    selectAndClick(cm, txt, button);
    expect(cm.getValue()).to.equal(`> ${txt}\n`);
  });

  it("Markdown Tools - Code", async function() {
    const button = wrapper.find(".fa-code");

    // With no selection
    button.trigger("click");
    expect(cm.getValue()).to.equal("``");

    // With text selected (Single Line)
    selectAndClick(cm, txt, button);
    expect(cm.getValue()).to.equal(`\`${txt}\``);

    // With text selected (Multiple Lines)
    cm.setValue(`${txt}\n${txt}`);
    cm.setSelection({ line: 0, ch: 0 }, { line: 1, ch: txt.length });
    button.trigger("click");
    expect(cm.getValue()).to.equal(`\`\`\`\n${txt}\n${txt}\n\`\`\``);
  });

  it("Markdown Tools - Link", async function() {
    const button = wrapper.find(".fa-link");

    // With no selection
    button.trigger("click");
    expect(cm.getValue()).to.equal("[](url)");

    // With text selected
    selectAndClick(cm, txt, button);
    expect(cm.getValue()).to.equal(`[${txt}](url)`);

    // Keyboad shortcut (Ctrl+K)
    setTextAndSelect(cm, txt);
    cm.getInputField().dispatchEvent(new KeyboardEvent("keydown",
      { key: "k", ctrlKey: true, keyCode: getKeyCode("k") }));
    expect(cm.getValue()).to.equal(`[${txt}](url)`);
  });

  it("Markdown Tools - Undo and Redo", async function() {
    const buttonUndo = wrapper.find(".fa-undo");
    const buttonRedo = wrapper.find(".fa-redo");

    // It change the text and save previous state to ''
    cm.replaceRange(txt, { line: 0, ch: 0 }, { line: 0, ch: 0 }, "");
    expect(cm.getValue()).to.equal(txt);

    // With Keyboard shortcut
    // Undo (Ctrl+Z)
    cm.focus();
    cm.getInputField().dispatchEvent(new KeyboardEvent("keydown",
      { key: "z", ctrlKey: true, keyCode: getKeyCode("z") }));
    // await waitFor(() => cm.getValue() === '');
    expect(cm.getValue()).to.equal("");
    // Redo (Ctrl+Shift+Z)
    cm.focus();
    cm.getInputField().dispatchEvent(new KeyboardEvent("keydown",
      { key: "z", ctrlKey: true, shiftKey: true, keyCode: getKeyCode("z") }));
    // await waitFor(() => cm.getValue() === txt);
    expect(cm.getValue()).to.equal(txt);

    // Press undo. It should go back to ''
    buttonUndo.trigger("click");
    expect(cm.getValue()).to.equal("");
    // Press redo. It should put txt again
    buttonRedo.trigger("click");
    expect(cm.getValue()).to.equal(txt);
  });
});
