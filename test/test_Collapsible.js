// @ts-check

import {
  isElementVisible,
  stubs,
  waitFor,
  waitForValue
} from "./utils";

import { includes, invoke } from "lodash";
import { afterEach, describe, it } from "mocha";

import { expect } from "chai";
import { mount } from "@vue/test-utils";

import { BaseCollapsible as Collapsible } from "../src";

describe("BaseCollapsible", function() {
  /** @type {Tests.Wrapper} */
  var wrapper;

  afterEach(function() {
    if (wrapper) {
      wrapper.destroy();
      /* @ts-ignore */
      wrapper = null;
    }
  });

  it("can mount a Collapsible", async function() {
    wrapper = mount(Collapsible, { propsData: { title: "test title" }, stubs });

    await waitFor(() => wrapper.find("h5").exists());
    const header = wrapper.find("h5");
    expect(header.text()).to.equal("test title");
    expect(wrapper.vm.isExpanded).to.be.false();
  });

  it("can toggle content using props", async function() {
    wrapper = mount(Collapsible, {
      propsData: { expand: true },
      slots: {
        default: "body content",
        header: "header title"
      }, stubs, attachTo: document.body });

    await waitFor(() => isElementVisible(wrapper.findComponent({ ref: "content" }).element));
    expect(wrapper.findComponent({ ref: "header" }).text()).to.equal("header title");
    const content = wrapper.findComponent({ ref: "content" });
    expect(content.text()).to.equal("body content");
    wrapper.setProps({ expand: false });
    await waitFor(() => !isElementVisible(content.element));
    wrapper.setProps({ expand: true });
    await waitFor(() => isElementVisible(content.element));
  });

  it("can toggle content using method", async function() {
    wrapper = mount(Collapsible, {
      propsData: { expand: true },
      slots: {
        default: "body content",
        header: "header title"
      }, stubs, attachTo: document.body });

    await waitFor(() => isElementVisible(wrapper.findComponent({ ref: "content" }).element));
    expect(wrapper.findComponent({ ref: "header" }).text()).to.equal("header title");
    const content = wrapper.findComponent({ ref: "content" });
    expect(content.text()).to.equal("body content");
    invoke(wrapper, [ "vm", "show" ], false);
    await waitFor(() => !isElementVisible(content.element));
    invoke(wrapper, [ "vm", "show" ], true);
    await waitFor(() => isElementVisible(content.element));
  });

  it("can toggle content by clicking", async function() {
    wrapper = mount(Collapsible, {
      slots: {
        default: "body content",
        header: "header title"
      }, stubs, attachTo: document.body });

    await waitFor(() => !isElementVisible(wrapper.findComponent({ ref: "content" }).element));
    const content = wrapper.findComponent({ ref: "content" });
    wrapper.find("i").trigger("click");
    await waitFor(() => isElementVisible(content.element));
    wrapper.find("i").trigger("click");
    await waitFor(() => !isElementVisible(content.element));
  });

  it("can bind Collapsible state", async function() {
    wrapper = mount({
      components: { Collapsible },
      data: () => ({ expand: false }),
      template: `
        <Collapsible :expand='expand' @update:expand='expand = $event'>
          <template v-slot:header='c'>title={{ c.isExpanded }}</template>
          <template v-slot:default='c'>body={{ c.isExpanded }}</template>
        </Collapsible>`
    }, stubs);

    await waitFor(() => includes(wrapper.text(), "title=false"));
    await waitFor(() => includes(wrapper.text(), "body=false"));
    wrapper.setData({ expand: true });
    await waitFor(() => includes(wrapper.text(), "title=true"));
    await waitFor(() => includes(wrapper.text(), "body=true"));
    wrapper.find("i").trigger("click");
    await waitForValue(() => wrapper.vm.expand, false);
  });
});
