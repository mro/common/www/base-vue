// @ts-check

import { after, before, describe, it } from "mocha";
import { expect } from "chai";
import { waitFor } from "./utils";
import { result } from "lodash";

import { createLocalVue, mount } from "@vue/test-utils";
import { default as BaseVue } from "../src";

const testCases = [
  // bigNumber(value, suffix, options) = result
  // value, suffix, options, result
  [ undefined, undefined, undefined, "-" ],
  [ "0", undefined, undefined, "0" ],
  [ 0, undefined, undefined, "0" ],
  [ 1, " suffix", undefined, "1 suffix" ],
  [ 1.12345, undefined, undefined, "1.123" ],
  [ 1.12345, undefined, { precision: 2 }, "1.12" ],
  [ 0.12345, undefined, undefined, "1.235e-1" ],
  [ 10101, undefined, undefined, "1.010e+4" ],
  [ 1.12, undefined, { precision: 3, fixed: true }, "1.120" ],
  [ 1.12, undefined, { precision: 3, fixed: false }, "1.12" ],
  [ 1.12, "x", { precision: 3 }, "1.12x" ]
];

describe("Filter bigNumber", () => {
  var localVue;
  /** @type {Tests.Wrapper} */
  var wrapper;

  before(function() {
    localVue = createLocalVue();

    // BaseVue plugin sets the filters
    localVue.use(BaseVue);

    wrapper = mount({
      template: "<div>{{ value | b-bigNumber(suffix, options) }}</div>",
      data: () => ({ value: undefined, suffix: undefined, options: undefined })
    }, { localVue });
  });

  after(function() {
    wrapper.destroy();
    // @ts-ignore
    wrapper = null;
    localVue = null;
  });

  it("passes the sanity check and creates a wrapper", () => {
    expect(!!wrapper.vm).to.be.true();
  });

  testCases.forEach((testC) => {

    it(`b-bigNumber(${result(testC, 0)}) = ${testC[3]}`, () => {
      wrapper.setData({
        value: result(testC, 0),
        suffix: result(testC, 1),
        options: result(testC, 2)
      });
      return waitFor(() => (wrapper.find("div").text() === testC[3]))
      .catch((err) => {
        /* make it fancier */
        expect(wrapper.find("div").text()).to.equal(testC[3]);
        throw err;
      });
    });
  });
});
