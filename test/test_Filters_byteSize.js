// @ts-check

import { toString } from "lodash";
import { byteSize } from "../src/filters";

import { after, before, describe, it } from "mocha";
import { expect } from "chai";
import { waitFor } from "./utils";

import { createLocalVue, mount } from "@vue/test-utils";
import { default as BaseVue, BaseCard as Card } from "../src";

const testCases = [
  // byteSize(bytes, decimal) = result
  // bytes, decimals, result
  [ undefined, undefined, "#NaN" ],
  [ "", undefined, "" ],
  [ "string", undefined, "string" ],
  [ {}, undefined, "#NaN" ],
  [ [], undefined, "#NaN" ],
  [ "0", undefined, "0 Bytes" ],
  [ 0, undefined, "0 Bytes" ],
  [ 1, undefined, "1 Bytes" ],
  [ 1023, undefined, "1023 Bytes" ],
  [ "1023", undefined, "1023 Bytes" ],
  [ 1024, undefined, "1 KiB" ],
  [ 2024, undefined, "1.98 KiB" ],
  [ 2024, 0, "2 KiB" ],
  [ 1048576, undefined, "1 MiB" ],
  [ 1058576, undefined, "1.01 MiB" ],
  [ 1064976, 3, "1.016 MiB" ],
  [ 2024, "2", "1.98 KiB" ]
];

/**
 * @param  {any[]} test
 * @return {string}
 */
function testCaseStr(test) {
  return `byteSize(${toString(test[0])}, ${toString(test[1])}) = ${test[2]}`;
}

describe("Filter ByteSize", () => {
  testCases.forEach((testC) => {
    it(testCaseStr(testC), () => {
      /* @ts-ignore: testing NaN error cases */
      expect(byteSize(testC[0], testC[1])).to.equal(testC[2]);
    });
  });
});

describe("Filter ByteSize in BaseCard component", () => {
  var localVue;
  /** @type {Tests.Wrapper} */
  var wrapper;

  before(function() {
    localVue = createLocalVue();

    // BaseVue plugin sets the filters
    localVue.use(BaseVue);

    wrapper = mount({
      components: { Card },
      template: `<Card>
          <template v-if="footer" v-slot:footer>{{byteValue | b-byteSize(byteSizeDecimals)}}</template>
        </Card>`,
      data: () => ({
        footer: true,
        byteValue:
        undefined,
        byteSizeDecimals: undefined
      })
    }, {
      localVue,
      propsData: {},
      mocks: {}
    });
  });

  after(function() {
    wrapper.destroy();
    // @ts-ignore
    wrapper = null;
    localVue = null;
  });

  it("passes the sanity check and creates a wrapper", () => {
    expect(!!wrapper.vm).to.be.true();
  });

  testCases.forEach((testC) => {
    it(testCaseStr(testC), () => {
      wrapper.setData({ byteValue: testC[0], byteSizeDecimals: testC[1] });
      return waitFor(() => (wrapper.find(".card-footer").text() === testC[2]));
    });
  });
});
