// @ts-check

import { DateTime } from "luxon";
import { after, before, describe, it } from "mocha";
import { waitForValue } from "./utils";

import { createLocalVue, mount } from "@vue/test-utils";
import { default as BaseVue } from "../src";

const testCases = [
  [ undefined, "" ],
  // we have to cope with the browser's timezone since this string is localised
  [ 1591800644542, DateTime.fromMillis(1591800644542)
  .toLocaleString(DateTime.DATETIME_SHORT_WITH_SECONDS) ],
  [ 1591800644, DateTime.fromSeconds(1591800644)
  .toLocaleString(DateTime.DATETIME_SHORT_WITH_SECONDS) ], // detect unix style
  [ {}, "" ],
  [ [], "" ]
];

describe("Filter timestamp as dateTime", () => {
  /** @type {Vue.VueConstructor<Vue>} */
  var localVue;
  /** @type {Tests.Wrapper} */
  var wrapper;

  before(function() {
    localVue = createLocalVue();

    // BaseVue plugin sets the filters
    localVue.use(BaseVue);
  });

  after(function() {
    wrapper?.destroy();
    // @ts-ignore
    wrapper = null;
    // @ts-ignore
    localVue = null;
  });

  testCases.forEach((testC) => {
    it(`b-dateTime(${testC[0]}) = ${testC[1]}`, () => {
      wrapper = mount({
        template: "<div>{{ value | b-dateTime }}</div>",
        data: () => ({ value: undefined })
      }, { localVue });

      wrapper.setData({ value: testC[0] });
      return waitForValue(() => (wrapper.find("div").text()), testC[1]);
    });
  });

  it("can display a date in specific timezone", async function() {
    wrapper = mount({
      template: "<div>{{ value | b-dateTime(undefined, { zone: 'Brazil/East', locale: 'en' }) }}</div>",
      data: () => ({ value: undefined })
    }, { localVue });

    wrapper.setData({ value: 1591800644 });
    return waitForValue(() => (wrapper.find("div").text()), "6/10/2020, 11:50:44 AM");
  });
});
