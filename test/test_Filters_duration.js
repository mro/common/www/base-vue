// @ts-check

import { after, before, describe, it } from "mocha";
import { expect } from "chai";
import { waitFor } from "./utils";
import { result } from "lodash";

import { createLocalVue, mount } from "@vue/test-utils";
import { default as BaseVue } from "../src";

const testCases = [
  // duration(ms, units, options) = result
  // ms, units, options, result
  [ undefined, undefined, undefined, "" ],
  [ null, undefined, undefined, "" ],
  [ 0, undefined, undefined, "" ],
  [ {}, undefined, undefined, "" ],
  [ [], undefined, undefined, "" ],
  [ 123456789, undefined, undefined, "123456789 milliseconds" ],
  [ (65 * 60 * 60 * 1000), [ "day", "hour" ], undefined, "2 days, 17 hours" ],
  [
    () => 5340000, [ "days", "hours", "minutes", "seconds" ],
    { hideNulls: false, unitDisplay: "narrow" }, "0d, 1h, 29m, 0s"
  ]
];

describe("Filter milliseconds as duration", () => {
  var localVue;
  /** @type {Tests.Wrapper} */
  var wrapper;

  before(function() {
    localVue = createLocalVue();

    // BaseVue plugin sets the filters
    localVue.use(BaseVue);

    wrapper = mount({
      template: "<div>{{ value | b-duration(units, options) }}</div>",
      data: () => ({ value: undefined, units: undefined, options: undefined })
    }, { localVue });
  });

  after(function() {
    wrapper.destroy();
    // @ts-ignore
    wrapper = null;
    localVue = null;
  });

  it("passes the sanity check and creates a wrapper", () => {
    expect(!!wrapper.vm).to.be.true();
  });

  testCases.forEach((testC) => {

    it(`b-duration(${result(testC, 0)}) = ${testC[3]}`, () => {
      wrapper.setData({
        value: result(testC, 0),
        units: result(testC, 1),
        options: result(testC, 2)
      });
      return waitFor(() => (wrapper.find("div").text() === testC[3]))
      .catch((err) => {
        /* make it fancier */
        expect(wrapper.find("div").text()).to.equal(testC[3]);
        throw err;
      });
    });
  });
});
