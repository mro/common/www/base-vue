// @ts-check

import { toString } from "lodash";
import { DateTime } from "luxon";
import { after, before, describe, it } from "mocha";
import { waitForValue } from "./utils";

import { createLocalVue, mount } from "@vue/test-utils";
import { default as BaseVue, BaseCard as Card } from "../src";

const testCases = [
  // byteSize(bytes, decimal) = result
  // bytes, decimals, result
  [ undefined, undefined, "" ],
  // we have to cope with the browser's timezone since this string is localised
  [ 1591800644542, undefined, DateTime.fromMillis(1591800644542)
  .set({ millisecond: 0 }).toISO({ suppressMilliseconds: true }) ],
  [ 1591800644, undefined, DateTime.fromSeconds(1591800644)
  .toISO({ suppressMilliseconds: true }) ], // detect unix style
  [ 1591800644542, "yyyy-MM-dd", "2020-06-10" ],
  [ {}, undefined, "" ],
  [ [], undefined, "" ]
];

describe("Filter timestamp using b-luxon", () => {
  /** @type {Vue.VueConstructor<Vue>} */
  var localVue;
  /** @type {Tests.Wrapper} */
  var wrapper;

  before(function() {
    localVue = createLocalVue();

    // BaseVue plugin sets the filters
    localVue.use(BaseVue);
  });

  after(function() {
    wrapper?.destroy();
    // @ts-ignore
    wrapper = null;
    // @ts-ignore
    localVue = null;
  });

  testCases.forEach((testC) => {
    it(`b-luxon(${toString(testC[0])}) = ${testC[2]}`, async () => {
      wrapper = mount({
        components: { Card },
        template: `<Card>
            <template v-if="footer" v-slot:footer>{{value | b-luxon(format)}}</template>
          </Card>`,
        data: () => ({ footer: true, value: undefined, format: undefined })
      }, { localVue });

      wrapper.setData({ value: testC[0], format: testC[1] });
      return waitForValue(() => (wrapper.find(".card-footer").text()), testC[2]);
    });
  });

  it("can display a date in specific timezone", async function() {
    wrapper = mount({
      components: { Card },
      template: `<Card>
          <template v-if="footer" v-slot:footer>{{value | b-luxon(format, { zone: 'Brazil/East', locale: 'fr' })}}</template>
        </Card>`,
      data: () => ({ footer: true, value: undefined, format: undefined })
    }, { localVue });
    wrapper.setData({ value: 1591800644 });
    return waitForValue(() => (wrapper.find(".card-footer").text()), "2020-06-10T11:50:44-03:00");
  });
});
