// @ts-check

import { DateTime } from "luxon";
import { after, before, describe, it } from "mocha";
import { expect } from "chai";
import { waitFor } from "./utils";
import { result } from "lodash";

import { createLocalVue, mount } from "@vue/test-utils";
import { default as BaseVue } from "../src";

const testCases = [
  [ undefined, "" ],
  // we have to cope with the browser's timezone since this string is localised
  [ 1591800644542, DateTime.fromMillis(1591800644542).toRelative() ],
  [ 1591800644, DateTime.fromSeconds(1591800644).toRelative() ], // detect unix style
  [ () => DateTime.now().minus({ months: 5 }).toSeconds(), "5 months ago" ],
  [ () => DateTime.now().plus({ hours: 2, seconds: 5 }).toSeconds(), "in 2 hours" ],
  [ {}, "" ],
  [ [], "" ]
];

describe("Filter timestamp as relative dateTime", () => {
  var localVue;
  /** @type {Tests.Wrapper} */
  var wrapper;

  before(function() {
    localVue = createLocalVue();

    // BaseVue plugin sets the filters
    localVue.use(BaseVue);

    wrapper = mount({
      template: "<div>{{ value | b-relativeDateTime }}</div>",
      data: () => ({ value: undefined })
    }, { localVue });
  });

  after(function() {
    wrapper.destroy();
    // @ts-ignore
    wrapper = null;
    localVue = null;
  });

  it("passes the sanity check and creates a wrapper", () => {
    expect(!!wrapper.vm).to.be.true();
  });

  testCases.forEach((testC) => {

    it(`b-relativeDateTime(${result(testC, 0)}) = ${testC[1]}`, () => {
      wrapper.setData({ value: result(testC, 0) });
      return waitFor(() => (wrapper.find("div").text() === testC[1]))
      .catch((err) => {
        /* make it fancier */
        expect(wrapper.find("div").text()).to.equal(testC[1]);
        throw err;
      });
    });
  });
});
