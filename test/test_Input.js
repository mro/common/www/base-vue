// @ts-check

import { afterEach, describe, it } from "mocha";
import { expect } from "chai";
import { mount } from "@vue/test-utils";

import { BaseInput as Input } from "../src";
import { stubs } from "./utils";

describe("Input", function() {
  /** @type {Tests.Wrapper} */
  var wrapper;

  afterEach(function() {
    if (wrapper) {
      wrapper.destroy();
      /* @ts-ignore */
      wrapper = null;
    }
  });

  it("can mount an Input", async function() {
    wrapper = mount(Input, {
      propsData: { value: 42 },
      stubs
    });

    expect(wrapper.vm.editValue).to.equal("42");
    wrapper.setProps({ inEdit: true });

    await wrapper.vm.$nextTick();
    wrapper.find("input").setValue("44");
    expect(wrapper.vm.editValue).to.equal("44");
    wrapper.setProps({ inEdit: false });
    await wrapper.vm.$nextTick();
    expect(wrapper.find("input").element).to.have.property("value", "42");
  });

  it("updates unmodified editValues", async function() {
    wrapper = mount(Input, {
      propsData: {},
      stubs
    });

    expect(wrapper.vm.editValue).to.be.null();

    wrapper.setProps({ value: 42 });
    await wrapper.vm.$nextTick();
    expect(wrapper.vm.editValue).to.equal("42");

    wrapper.setProps({ value: 43 });
    await wrapper.vm.$nextTick();
    expect(wrapper.vm.editValue).to.equal("43");
    wrapper.setData({ editValue: "42" });

    wrapper.setProps({ value: 44 });
    await wrapper.vm.$nextTick();
    expect(wrapper.vm.editValue).to.equal("42");
  });

  it("notifies about changes", async function() {
    wrapper = mount(Input, {
      propsData: { value: 42 },
      stubs
    });

    wrapper.setProps({ inEdit: true });
    await wrapper.vm.$nextTick();
    wrapper.find("input").setValue("44");
    await wrapper.vm.$nextTick();
    expect(wrapper.emitted("edit")).to.deep.equal([ [ "42" ], [ "42" ], [ "44" ] ]);
  });
});
