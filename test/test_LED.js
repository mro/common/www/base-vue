// @ts-check

import { afterEach, describe, it } from "mocha";
import { expect } from "chai";
import { mount } from "@vue/test-utils";
import { stubs, waitForValue } from "./utils";

import { BaseLED as LED } from "../src";

describe("LED", function() {
  /** @type {Tests.Wrapper} */
  var wrapper;

  afterEach(function() {
    if (wrapper) {
      wrapper.destroy();
      /* @ts-ignore */
      wrapper = null;
    }
  });

  it("can mount a LED", async function() {
    wrapper = mount(LED, {
      propsData: { value: false },
      stubs
    });

    expect(wrapper.find(".b-led"), "LED does not exist").to.exist();

    expect(wrapper.vm.actualValue, "actual value should be false")
    .to.equal("transparent");

    expect(wrapper.find(".b-led").element.style.backgroundColor)
    .equals("transparent");

    wrapper.setProps({ value: true });

    await waitForValue(() => wrapper.vm.actualValue.includes("yellow"), true,
      "LED should have yellow color by default");

    expect(wrapper.find(".b-led").element.style.backgroundColor)
    .contains("yellow");
  });

  it("allows any CSS colour to be set", async function() {
    wrapper = mount(LED, {
      propsData: { value: true },
      stubs
    });

    expect(wrapper.vm.actualValue,
      "actual value should be 'var(--yellow)' by default")
    .to.equal("var(--yellow)");

    expect(wrapper.find(".b-led").element.style.backgroundColor)
    .equals("var(--yellow)");

    // keyword (e.g. 'red')

    wrapper.setProps({ value: "red" });

    await waitForValue(() => wrapper.vm.actualValue, "red",
      "LED should have 'red' color");

    expect(wrapper.find(".b-led").element.style.backgroundColor)
    .equals("red");

    // #-hexadecimal notation

    wrapper.setProps({ value: "#00ff00" });

    await waitForValue(() => wrapper.vm.actualValue, "#00ff00",
      "LED should have '#00ff00' color");

    expect(wrapper.find(".b-led").element.style.backgroundColor)
    .equals("rgb(0, 255, 0)");

    // RGB notation

    wrapper.setProps({ value: "rgb(0, 0, 255)" });

    await waitForValue(() => wrapper.vm.actualValue, "rgb(0, 0, 255)",
      "LED should have 'rgb(0, 0, 255)' color");

    expect(wrapper.find(".b-led").element.style.backgroundColor)
    .equals("rgb(0, 0, 255)");
  });

  it("can blink", async function() {
    wrapper = mount(LED, {
      attachTo: document.body,
      propsData: { value: true },
      stubs
    });

    expect(wrapper.find(".b-led").element.classList.contains("b-led-blinking"),
      "LED should not blink").to.be.false();

    wrapper.setProps({ blink: true });

    await waitForValue(
      () => wrapper.find(".b-led").element.classList.contains("b-led-blinking"),
      true, "LED should blink");
  });
});
