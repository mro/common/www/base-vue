// @ts-check

import { afterEach, describe, it } from "mocha";
import { expect } from "chai";
import { mount } from "@vue/test-utils";

import { BaseMarkdownViewer } from "../src";
import { isElementVisible, waitFor } from "./utils";

describe("MarkdownViewer", function() {
  /** @type {Tests.Wrapper} */
  var wrapper;

  afterEach(function() {
    if (wrapper) {
      wrapper.destroy();
      /* @ts-ignore */
      wrapper = null;
    }
  });

  it("can mount a MarkdownViewer", async function() {
    wrapper = mount(BaseMarkdownViewer, {
      propsData: { value: "# Test editor" }
    });

    expect(wrapper.vm.mdinstance).is.not.undefined();
    // Test markdown h1 is visible
    await waitFor(() => isElementVisible(wrapper.find("h1").element));
    expect(isElementVisible(wrapper.find("h2").element)).to.be.false();

    wrapper.setProps({ value: "## Test editor" });
    await waitFor(() => isElementVisible(wrapper.find("h2").element));
    expect(isElementVisible(wrapper.find("h1").element)).to.be.false();
  });

});
