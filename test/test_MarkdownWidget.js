// @ts-check

import { afterEach, describe, it } from "mocha";
import { expect } from "chai";
import { mount } from "@vue/test-utils";

import { BaseMarkdownWidget as MarkdownWidget } from "../src";
import CodeMirrorEditor from "../src/plugins/CodeMirrorEditor.vue";
import { isElementVisible, stubs, waitForWrapper } from "./utils";

describe("MarkdownWidget", function() {
  /** @type {Tests.Wrapper} */
  var wrapper;

  afterEach(function() {
    if (wrapper) {
      wrapper.destroy();
      /* @ts-ignore */
      wrapper = null;
    }
  });

  it("can mount a MarkdownWidget", async function() {
    wrapper = mount(MarkdownWidget, {
      propsData: { value: "Test Markdown Widget" },
      stubs, attachTo: document.body
    });

    const viewer = await waitForWrapper(() => wrapper.findComponent({ ref: "mdViewer" }));
    expect(viewer.vm).to.have.property("value", "Test Markdown Widget");

    await waitForWrapper(() => wrapper.findComponent(CodeMirrorEditor));
    expect(wrapper.vm.editValue).to.equal("Test Markdown Widget");

    wrapper.setProps({ inEdit: true });
    await wrapper.vm.$nextTick();
    expect(wrapper.vm.showPreview).to.be.false();
    expect(isElementVisible(wrapper.findComponent({ ref: "mdViewerPreview" }).element))
    .to.be.false();
    expect(isElementVisible(wrapper.findComponent(CodeMirrorEditor).element))
    .to.be.true();

    wrapper.setProps({ value: "## Test Header" });
    await wrapper.vm.$nextTick();

    expect(wrapper.vm.editValue).to.equal("## Test Header");

    wrapper.setData({ showPreview: true });
    await wrapper.vm.$nextTick();
    expect(isElementVisible(wrapper.findComponent({ ref: "mdViewerPreview" }).element))
    .to.be.true();
    expect(isElementVisible(wrapper.findComponent(CodeMirrorEditor).element))
    .to.be.false();

    // Back to edit mode using keyboard shortcut (Ctrl-Enter)
    wrapper.trigger("keydown", { key: "enter", ctrlKey: true });
    await wrapper.vm.$nextTick();
    expect(wrapper.vm.showPreview).to.be.false();
    expect(isElementVisible(wrapper.findComponent({ ref: "mdViewerPreview" }).element))
    .to.be.false();
    expect(isElementVisible(wrapper.findComponent(CodeMirrorEditor).element))
    .to.be.true();
  });

  it("updates unmodified editValues", async function() {
    wrapper = mount(MarkdownWidget, {
      propsData: { value: "" },
      stubs
    });

    expect(wrapper.vm.editValue).to.equal("");

    wrapper.setProps({ value: "42" });
    await wrapper.vm.$nextTick();
    expect(wrapper.vm.editValue).to.equal("42");

    wrapper.setProps({ value: "43" });
    await wrapper.vm.$nextTick();
    expect(wrapper.vm.editValue).to.equal("43");
    wrapper.setData({ editValue: "42" });

    wrapper.setProps({ value: "44" });
    await wrapper.vm.$nextTick();
    expect(wrapper.vm.editValue).to.equal("42");
  });

  it("notifies about changes", async function() {
    wrapper = mount(MarkdownWidget, {
      propsData: { value: "42" },
      stubs
    });

    wrapper.setProps({ inEdit: true });
    await wrapper.vm.$nextTick();
    // @ts-ignore
    wrapper.findComponent(CodeMirrorEditor).vm.get().setValue("44");
    await wrapper.vm.$nextTick();
    expect(wrapper.emitted("edit")).to.deep.equal([ [ "42" ], [ "42" ], [ "44" ] ]);
  });
});
