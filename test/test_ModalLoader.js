// @ts-check

import { afterEach, describe, it } from "mocha";
import { mount } from "@vue/test-utils";

import { BaseModalLoader as ModalLoader } from "../src";
import { waitFor } from "./utils";

describe("ModalLoader", function() {
  /** @type {Tests.Wrapper} */
  var wrapper;

  afterEach(function() {
    if (wrapper) {
      wrapper.destroy();
      /* @ts-ignore */
      wrapper = null;
    }
  });

  it("has not content when disabled", async function() {
    wrapper = mount(ModalLoader, {
      propsData: { isLoading: true }
    });

    await waitFor(() => wrapper.find(".fa-spinner").exists());
    wrapper.setProps({ isLoading: false });
    await waitFor(() => !wrapper.find(".fa-spinner").exists());
  });
});
