// @ts-check

import { get } from "lodash";
import { afterEach, describe, it } from "mocha";
import { expect } from "chai";
import { mount } from "@vue/test-utils";

import { BaseCard } from "../src";
import { createRouter, waitForWrapper } from "./utils";

describe("NavBar", function() {
  /** @type {Tests.Wrapper} */
  var wrapper;

  afterEach(function() {
    if (wrapper) {
      wrapper.destroy();
      /* @ts-ignore */
      wrapper = null;
    }
  });

  it("can mount a NavBar", async () => {
    const router = createRouter("/", {
      routes: [
        { name: "Home", path: "/", component: BaseCard },
        { name: "Hidden", path: "/hidden", component: BaseCard,
          meta: { navbar: false } },
        { name: "NotHome", path: "/nothome", component: BaseCard,
          props: { title: "not home" } }
      ]
    });

    wrapper = mount({ template: "<div><BaseNavBar /><RouterView /></div>" },
      { propsData: { title: "myApp", version: "42" }, router });
    // wait for router to settle
    await waitForWrapper(
      () => wrapper.findComponent({ name: "BaseCard" }));

    const items = await waitForWrapper(
      () => wrapper.findAllComponents({ name: "BaseNavBarItem" }));
    expect(items).to.have.length(2);
    expect(items.at(0).text()).to.equal("Home");
    expect(items.at(1).text()).to.equal("NotHome");

    router.push("/nothome");
    // FIXME: for some reason not working
    // items.at(1).trigger('click');
    await waitForWrapper(
      () => wrapper.findAllComponents({ name: "BaseCard" })
      .filter((c) => get(c.vm, "title") === "not home"));
    expect(items.at(1).vm).to.have.property("active", true);
  });
});
