// @ts-check
import _ from "lodash";
import $ from "jquery";
import moment from "moment";

import { afterEach, describe, it } from "mocha";
import { expect } from "chai";
import { mount } from "@vue/test-utils";

import TempusDominusDatePicker from "../src/plugins/TempusDominusDatePicker.vue";
import { BaseParamDate as ParamDate } from "../src";
import { stubs, waitFor } from "./utils";

describe("ParamDate", function() {
  /** @type {Tests.Wrapper} */
  var wrapper;

  afterEach(function() {
    if (wrapper) {
      wrapper.destroy();
      /* @ts-ignore */
      wrapper = null;
    }
  });

  // Test Dates
  // 1593259787 -> 2020-06-27 14:09:47 +0200
  it("can mount a ParamDate", async () => {
    wrapper = mount(ParamDate, {
      propsData: { title: "Test Date Param", timestamp: 1593259787 },
      stubs
    });

    expect(wrapper.find(".card-header").text()).to.equal("Test Date Param");
    expect(wrapper.vm.editTimestamp).to.equal(1593259787);
    expect(wrapper.classes()).to.contain("b-param-primary");
    wrapper.setProps({ inEdit: true });
    await wrapper.vm.$nextTick();
    // @ts-ignore
    expect(wrapper.findComponent(TempusDominusDatePicker).vm.$el.value).contains("2020-06-27");
  });

  it("notifies about changes", async () => {
    const div = document.createElement("div");
    /* $FlowIgnore */
    document.body.appendChild(div);

    wrapper = mount(ParamDate, {
      propsData: { title: "Test Date Param", timestamp: 1593259787, inEdit: true },
      stubs,
      attachTo: div // Needs to attach in order to render datepicker widget
    });

    await wrapper.vm.$nextTick();
    // Click the input in order to open the widget
    // $(wrapper.findComponent(TempusDominusDatePicker).vm.$el).click(); // Somehow click is not working with karma only.
    // @ts-ignore
    wrapper.findComponent(TempusDominusDatePicker).vm.dp.show();
    await waitFor(() => wrapper.find(".bootstrap-datetimepicker-widget").exists());
    // Click on today button
    $(wrapper.find(".fa-calendar-check").element).click();
    await wrapper.vm.$nextTick();
    // Check if is today
    // @ts-ignore
    expect(wrapper.findComponent(TempusDominusDatePicker).vm.dp.date().isSame(moment(), "day")).to.be.true();
    // Check last edit event emitted is with a today timestamp
    const lastTsEmitted = _.last(wrapper.emitted("edit"))[0];
    expect(moment.unix(lastTsEmitted).isSame(moment(), "day")).to.be.true();
    wrapper.setProps({ inEdit: false });
    await wrapper.vm.$nextTick();
    wrapper.setProps({ inEdit: true });
    await wrapper.vm.$nextTick();
    // Value should now be reset to initial timestamp
    // @ts-ignore
    expect(wrapper.findComponent(TempusDominusDatePicker).vm.$el.value).contains("2020-06-27");
  });

  it("notifies about ask for edit", async function() {
    wrapper = mount(ParamDate, {
      propsData: { title: "Test Param", timestamp: 1593259787, inEdit: false },
      listeners: { "edit-request": () => null },
      stubs
    });

    wrapper.vm.setHasFocus(true); // Little hack to fake the focus
    await waitFor(() => wrapper.find(".text-primary").exists());
    wrapper.find(".text-primary").trigger("click");
    expect(wrapper.emitted("edit-request")).to.have.property("length", 1);
  });

  it("check readonly does not trigger widget", async () => {
    wrapper = mount({
      template: `<div style="position: relative;">
        <ParamDate ref="in" :inEdit="true" :timestamp="timestamp" readonly="readonly"></ParamDate>
      </div>`,
      components: { ParamDate },
      props: { timestamp: { type: Number, default: 1593259787 } }
    }, { stubs });

    await wrapper.vm.$nextTick();
    // Click the input in order to open the widget
    const tdEl = wrapper.findComponent(TempusDominusDatePicker).vm.$el;
    $(tdEl).click();
    await wrapper.vm.$nextTick();
    expect(wrapper.find(".bootstrap-datetimepicker-widget").exists()).to.be.false();
    // Check that input has readonly attribute
    expect(tdEl.getAttribute("readonly")).to.equal("readonly");
  });

  it("supports native input validation attributes", async () => {
    wrapper = mount({
      template: `<div style="position: relative;">
        <ParamDate ref="in" :inEdit="true" :timestamp="timestamp" required="1"></ParamDate>
      </div>`,
      components: { ParamDate },
      props: { timestamp: { type: Number, default: null } }
    }, { stubs });

    wrapper.vm.$refs.in.checkValidity();
    /* should warn about empty value */

    await waitFor(() => wrapper.find(".alert-danger").exists());
    wrapper.setProps({ timestamp: 1593259787 });
    wrapper.vm.$refs.in.checkValidity();
    await waitFor(() => !wrapper.find(".alert-danger").exists());
    wrapper.setProps({ timestamp: null });
    wrapper.vm.$refs.in.checkValidity();
    await waitFor(() => wrapper.find(".alert-danger").exists());
  });

  it("supports v-model", async () => {
    wrapper = mount({
      template: `
        <BaseParamDate ref="in" :inEdit="true" v-model='timestamp'></BaseParamDate>
      </div>`,
      components: { ParamDate },
      computed: {
        timestamp: {
          get() { return 12345; },
          // @ts-ignore
          set(value) { this.$options._timestamp = value; }
        }
      }
    }, { stubs });

    await wrapper.vm.$nextTick();
    // edit is emitted as soon as component is mounted
    expect(wrapper.vm.$options._timestamp).to.equal(12345);
  });
});
