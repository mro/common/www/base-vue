// @ts-check

import { afterEach, describe, it } from "mocha";
import { expect } from "chai";
import { mount } from "@vue/test-utils";

import {
  BaseParamBase as ParamBase,
  BaseParamDate as ParamDate,
  BaseParamFile as ParamFile,
  BaseParamInput as ParamInput,
  BaseParamList as ParamList } from "../src";

describe("Param errors and warnings", function() {
  /** @type {Tests.Wrapper} */
  var wrapper;

  afterEach(function() {
    if (wrapper) {
      wrapper.destroy();
      /* @ts-ignore */
      wrapper = null;
    }
  });

  /**
   * @param  {V.Instance<any, any>} klass
   */
  function testErrorAPI(klass) {
    it(klass.options.name + " implements error API", async function() {
      wrapper = mount(klass, {
        propsData: { title: "Test Param", inEdit: true }
      });

      await wrapper.vm.$nextTick();
      wrapper.vm.addError("custom", "This is an error");
      wrapper.vm.addWarning("custom", "This is a warning");
      wrapper.vm.checkValidity();
      await wrapper.vm.$nextTick();
      expect(wrapper.find(".alert-danger").exists()).to.be.true();
      expect(wrapper.find(".alert-warning").exists()).to.be.true();
    });
  }

  testErrorAPI(ParamList);
  testErrorAPI(ParamInput);
  testErrorAPI(ParamBase);
  testErrorAPI(ParamDate);
  testErrorAPI(ParamFile);
});
