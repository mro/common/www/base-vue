// @ts-check

import { afterEach, beforeEach, describe, it } from "mocha";
import { expect } from "chai";
import { mount } from "@vue/test-utils";
import sinon from "sinon";
import { get } from "lodash";

import { BaseParamFile as ParamFile } from "../src";
import { stubs, waitFor } from "./utils";

describe("ParamFile", function() {
  /** @type {Tests.Wrapper} */
  var wrapper;
  /** @type {sinon.SinonSandbox} */
  var sandbox;

  beforeEach(function() {
    sandbox = sinon.createSandbox();
  });

  afterEach(function() {
    if (wrapper) {
      wrapper.destroy();
      /* @ts-ignore */
      wrapper = null;
    }
    sandbox.restore();
  });

  it("can mount a ParamFile", async function() {
    wrapper = mount(ParamFile, {
      propsData: { title: "Test Param", value: "sample.txt" },
      stubs
    });

    expect(wrapper.find(".card-header").text()).to.equal("Test Param");
    expect(wrapper.vm.value).to.equal("sample.txt");
    expect(wrapper.classes()).to.contain("b-param-primary");
    wrapper.setProps({ inEdit: true });

    await wrapper.vm.$nextTick();
    const input = wrapper.find("input");
    // @ts-ignore
    sandbox.stub(input.element, "files").value([ { name: "stub" } ]);
    input.trigger("change");

    expect(wrapper.vm.editName).to.equal("stub");
  });

  it("notifies about ask for edit", async function() {
    wrapper = mount(ParamFile, {
      propsData: { title: "Test Param", value: "sample.txt", inEdit: false },
      listeners: { "edit-request": () => null },
      stubs
    });

    wrapper.vm.setHasFocus(true); // Little hack to fake the focus
    await waitFor(() => wrapper.find(".text-primary").exists());
    wrapper.find(".text-primary").trigger("click");
    expect(get(wrapper.emitted("edit-request"), "length")).to.be.equal(1);
  });
});
