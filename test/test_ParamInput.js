// @ts-check

import { afterEach, describe, it } from "mocha";
import { expect } from "chai";
import { mount } from "@vue/test-utils";
import Vue from "vue";

import { BaseParamInput as ParamInput } from "../src";
import { stubs, waitFor, waitForValue } from "./utils";

describe("ParamInput", function() {
  /** @type {Tests.Wrapper} */
  var wrapper;

  afterEach(function() {
    if (wrapper) {
      wrapper.destroy();
      /* @ts-ignore */
      wrapper = null;
    }
  });

  it("can mount a ParamInput", async function() {
    wrapper = mount(ParamInput, {
      propsData: { title: "Test Param", value: 42 },
      stubs
    });

    expect(wrapper.find(".card-header").text()).to.equal("Test Param");
    expect(wrapper.vm.editValue).to.equal("42");
    expect(wrapper.classes()).to.contain("b-param-primary");
    wrapper.setProps({ inEdit: true });

    await wrapper.vm.$nextTick();
    expect(wrapper.classes()).to.contain("b-param-primary");
    wrapper.find("input").setValue("44");
    expect(wrapper.vm.editValue).to.equal("44");
    wrapper.setProps({ inEdit: false });
    await wrapper.vm.$nextTick();
    expect(wrapper.find("input")).to.have.nested.property("element.value", "42");
  });

  it("updates unmodified editValues", async function() {
    wrapper = mount(ParamInput, {
      propsData: { title: "Test Param" },
      stubs
    });

    expect(wrapper.vm.editValue).to.be.null();

    wrapper.setProps({ value: 42 });
    await waitForValue(() => wrapper.vm.editValue, "42");

    wrapper.setProps({ value: 43 });
    await waitForValue(() => wrapper.vm.editValue, "43");

    wrapper.setData({ editValue: "42" });

    wrapper.setProps({ value: 44 });
    await waitForValue(() => wrapper.vm.editValue, "42");
  });

  it("notifies about changes", async function() {
    wrapper = mount(ParamInput, {
      propsData: { title: "Test Param", value: 42 },
      stubs
    });

    wrapper.setProps({ inEdit: true });
    await wrapper.vm.$nextTick();
    wrapper.find("input").setValue("44");
    await wrapper.vm.$nextTick();
    expect(wrapper.emitted("edit")).to.deep.equal([ [ "42" ], [ "42" ], [ "44" ] ]);
  });

  it("notifies about ask for edit", async function() {
    wrapper = mount(ParamInput, {
      propsData: { title: "Test Param", value: 42, inEdit: false },
      listeners: { "edit-request": () => null },
      stubs
    });

    wrapper.vm.setHasFocus(true); // Little hack to fake the focus
    await waitFor(() => wrapper.find(".text-primary").exists());
    wrapper.find(".text-primary").trigger("click");
    expect(wrapper.emitted("edit-request")).to.have.length(1);
  });

  it("supports native input validation attributes", async function() {
    wrapper = mount({
      template: "<ParamInput ref=\"in\" :inEdit=\"true\" :value=\"value\" required pattern=\"[0-9]*\"></ParamInput>",
      components: { ParamInput },
      props: { value: { type: String, default: "" } }
    }, { stubs });

    wrapper.vm.$refs.in.checkValidity();
    /* should warn about empty value */
    await waitFor(() => wrapper.find(".alert-danger").exists());
    wrapper.setProps({ value: "42" });
    wrapper.vm.$refs.in.checkValidity();
    await waitFor(() => !wrapper.find(".alert-danger").exists());
    wrapper.setProps({ value: "pwet" });
    wrapper.vm.$refs.in.checkValidity();
    await waitFor(() => wrapper.find(".alert-danger").exists());
  });

  it("supports v-model", async function() {
    wrapper = mount(Vue.extend({
      components: { ParamInput },
      props: { inEdit: { type: Boolean, default: false } },
      computed: {
        value: {
          get() { return 42; },
          set(value) { this.$options._value = value; }
        }
      },
      template: "<ParamInput ref=\"in\" :inEdit='inEdit' v-model='value'></ParamInput>"
    }), { stubs });

    wrapper.setProps({ inEdit: true });
    await wrapper.vm.$nextTick();
    wrapper.find("input").setValue("44");
    await wrapper.vm.$nextTick();
    expect(wrapper.vm.$options._value).to.equal("44");
  });
});
