// @ts-check

import { afterEach, describe, it } from "mocha";
import { expect } from "chai";
import { mount } from "@vue/test-utils";

import { BaseParamList as ParamList } from "../src";
import { stubs, waitFor } from "./utils";

describe("ParamList", function() {
  /** @type {Tests.Wrapper} */
  var wrapper;

  afterEach(function() {
    if (wrapper) {
      wrapper.destroy();
      /* @ts-ignore */
      wrapper = null;
    }
  });

  it("can mount a dynamic ParamList", async function() {
    wrapper = mount(ParamList, {
      propsData: {
        title: "Test Param", value: 2,
        options: [ { value: 0 }, { value: 2, text: "SEC" } ]
      }, stubs
    });

    expect(wrapper.find(".card-header").text()).to.equal("Test Param");
    expect(wrapper.find("input"))
    .to.have.nested.property("element.value", "SEC");
    expect(wrapper.vm.editValue).to.equal(2);
    expect(wrapper.vm.valueText).to.equal("SEC");
    expect(wrapper.vm.editValueText).to.equal("SEC");

    wrapper.setProps({ inEdit: true });
    await waitFor(() => wrapper.find("select").exists());
    wrapper.findAll("select > option").at(0).setSelected();
    expect(wrapper.vm.editValue).to.equal(0);
    expect(wrapper.vm.editValueText).to.equal("0");

    wrapper.setProps({ inEdit: false });
    await waitFor(() => !wrapper.find("select").exists());
    expect(wrapper.vm.editValue).to.equal(0);
    expect(wrapper.vm.value).to.equal(2);
    expect(wrapper.find("input"))
    .to.have.nested.property("element.value", "SEC");
  });

  it("can mount a static ParamList", async function() {
    wrapper = mount(ParamList, {
      propsData: { value: "2" },
      slots: {
        options: `
          <option value="0">ZERO</option>
          <option value="2">SEC</option>`,
        title: "Another title"
      }, stubs
    });

    expect(wrapper.find(".card-header").text()).to.equal("Another title");
    expect(wrapper.vm.editValue).to.equal("2");
    expect(wrapper.vm.valueText).to.equal("SEC");
    expect(wrapper.vm.editValueText).to.equal("SEC");

    wrapper.setProps({ inEdit: true });
    await waitFor(() => wrapper.find("select").exists());
    wrapper.findAll("select > option").at(0).setSelected();
    expect(wrapper.vm.editValue).to.equal("0");

    wrapper.setProps({ inEdit: false });
    await waitFor(() => !wrapper.find("select").exists());
    expect(wrapper.vm.editValue).to.equal("0");
    expect(wrapper.vm.value).to.equal("2");
    expect(wrapper.find("input")).to.have.nested.property("element.value", "SEC");
  });

  it("updates unmodified editValues", async function() {
    wrapper = mount(ParamList, {
      propsData: {
        title: "Test Param", value: 2,
        options: [
          { value: 0, text: "ZERO" },
          { value: 1, text: "FIRST" },
          { value: 2, text: "SEC" }
        ]
      }, stubs
    });

    expect(wrapper.vm.editValue).to.equal(2);

    wrapper.setProps({ value: 0 });
    await wrapper.vm.$nextTick();
    expect(wrapper.vm.editValue).to.equal(0);

    wrapper.setProps({ value: 2 });
    await wrapper.vm.$nextTick();
    expect(wrapper.vm.editValue).to.equal(2);
    wrapper.setData({ editValue: 1 });
    wrapper.setProps({ value: 0 });
    await wrapper.vm.$nextTick();
    expect(wrapper.vm.editValue).to.equal(1);
  });

  it("notifies about changes", async function() {
    wrapper = mount(ParamList, {
      propsData: {
        title: "Test Param", value: 2,
        options: [
          { value: 0, text: "ZERO" },
          { value: 1, text: "FIRST" },
          { value: 2, text: "SEC" }
        ]
      }, stubs
    });

    wrapper.setProps({ inEdit: true });
    await waitFor(() => wrapper.find("select").exists());
    wrapper.findAll("select > option").at(0).setSelected();
    await wrapper.vm.$nextTick();
    expect(wrapper.emitted("edit")).to.deep.equal([ [ 2 ], [ 0 ] ]);
  });

  it("notifies about ask for edit", async function() {
    wrapper = mount(ParamList, {
      propsData: { title: "Test Param", options: [
        { value: 0, text: "ZERO" },
        { value: 1, text: "FIRST" },
        { value: 2, text: "SEC" }
      ], inEdit: false },
      listeners: { "edit-request": () => null },
      stubs
    });

    wrapper.vm.setHasFocus(true); // Little hack to fake the focus
    await waitFor(() => wrapper.find(".text-primary").exists());
    wrapper.find(".text-primary").trigger("click");
    expect(wrapper.emitted("edit-request")).to.have.property("length", 1);
  });

  it("supports v-model", async function() {
    wrapper = mount({
      template: `
      <BaseParamList ref="in" :inEdit='inEdit' v-model='value'>
        <template #options>
          <option value='1'>first</option>
          <option value='2'>second</option>
        </template>
      </BaseParamList>`,
      props: { inEdit: { type: Boolean, default: false } },
      computed: {
        value: {
          get() { return "1"; },
          // @ts-ignore
          set(value) { this.$options._value = value; }
        }
      }
    }, { stubs });

    wrapper.setProps({ inEdit: true });
    await waitFor(() => wrapper.find("select").exists());
    wrapper.findAll("select > option").at(1).setSelected();
    await wrapper.vm.$nextTick();
    expect(wrapper.vm.$options._value).to.deep.equal("2");
  });
});
