// @ts-check

import { afterEach, describe, it } from "mocha";
import { expect } from "chai";
import { mount } from "@vue/test-utils";

import {
  BaseParamInput as ParamInput,
  BaseParamReadonly as ParamReadonly } from "../src";
import { stubs, waitFor } from "./utils";

describe("ParamReadonly", function() {
  /** @type {Tests.Wrapper} */
  var wrapper;

  afterEach(function() {
    if (wrapper) {
      wrapper.destroy();
      /* @ts-ignore */
      wrapper = null;
    }
  });

  it("can mount a ParamReadonly", async function() {
    wrapper = mount(ParamReadonly, {
      propsData: { title: "Test Param", value: "42" },
      stubs
    });

    expect(wrapper.find(".card-header").text()).to.equal("Test Param");
    expect(wrapper.vm.value).to.equal("42");
    expect(wrapper.classes()).to.contain("b-param-primary");

    wrapper.setProps({ inEdit: true });
    await wrapper.vm.$nextTick();
    expect(wrapper.vm.$refs["input"].value).to.equal("42");
    expect(wrapper.classes()).to.contain("b-param-secondary");
  });

  it("updates unmodified editValues", async function() {
    wrapper = mount(ParamInput, {
      propsData: { title: "Test Param" },
      stubs
    });

    expect(wrapper.vm.editValue).to.be.null();

    wrapper.setProps({ value: 42 });
    await wrapper.vm.$nextTick();
    expect(wrapper.vm.editValue).to.equal("42");

    wrapper.setProps({ value: 43 });
    await wrapper.vm.$nextTick();
    expect(wrapper.vm.editValue).to.equal("43");
    wrapper.setData({ editValue: "42" });

    wrapper.setProps({ value: 44 });
    await wrapper.vm.$nextTick();
    expect(wrapper.vm.editValue).to.equal("42");
  });

  it("supports native input validation attributes", async function() {
    wrapper = mount({
      template: `<BaseParamReadonly ref="in" :inEdit="true" :value="value"
                                @edit="$refs.in.checkValidity()"
                                required pattern="[0-9]*">
                  </BaseParamReadonly>`,
      props: { value: { type: String, default: "" } }
    }, { stubs });

    wrapper.vm.$refs.in.checkValidity();
    /* should warn about empty value */
    await waitFor(() => wrapper.find(".alert-danger").exists());
    wrapper.setProps({ value: "42" });
    wrapper.vm.$refs.in.checkValidity();
    await waitFor(() => !wrapper.find(".alert-danger").exists());
    wrapper.setProps({ value: "pwet" });
    wrapper.vm.$refs.in.checkValidity();
    await waitFor(() => wrapper.find(".alert-danger").exists());
  });

  it("supports v-model", async function() {
    wrapper = mount({
      template: "<BaseParamReadonly ref=\"in\" :inEdit='true' v-model='value'></BaseParamReadonly>",
      computed: {
        value: {
          get() { return 42; },
          // @ts-ignore
          set(value) { this.$options._value = value; }
        }
      }
    }, { stubs });

    await wrapper.vm.$nextTick();
    wrapper.vm.$refs.in.editValue = "44";
    await wrapper.vm.$nextTick();
    expect(wrapper.vm.$options._value).to.equal("44");
  });
});
