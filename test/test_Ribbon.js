// @ts-check
import { afterEach, describe, it } from "mocha";
import { expect } from "chai";
import { mount } from "@vue/test-utils";

import { BaseRibbon as Ribbon } from "../src";

describe("Ribbon", function() {
  /** @type {Tests.Wrapper} */
  var wrapper;

  afterEach(function() {
    if (wrapper) {
      wrapper.destroy();
      /* @ts-ignore */
      wrapper = null;
    }
  });

  it("can mount a Ribbon (default top-right)", async () => {
    const div = document.createElement("div");
    /* $FlowIgnore */
    document.body.appendChild(div);

    wrapper = mount({
      template: `<div style="position: relative;">
        <Ribbon ref="rib" v-model="text"></Ribbon>
      </div>`,
      components: { Ribbon },
      props: { text: { type: String, default: "testText" } }
    }, { attachTo: div }); // Needs to attach in order to render

    expect(wrapper.find(".b-ribbon").find("span").text()).to.equal("testText");
    expect(wrapper.find(".b-ribbon").classes()).to.contain("b-ribbon-top-right");
  });

  it("can mount a Ribbon with position", async () => {
    const div = document.createElement("div");
    /* $FlowIgnore */
    document.body.appendChild(div);

    wrapper = mount({
      template: `<div style="position: relative;">
        <Ribbon ref="rib" v-model="text" position="top-left"></Ribbon>
      </div>`,
      components: { Ribbon },
      props: { text: { type: String, default: "testText" } }
    }, { attachTo: div }); // Needs to attach in order to render

    expect(wrapper.find(".b-ribbon").find("span").text()).to.equal("testText");
    expect(wrapper.find(".b-ribbon").classes()).to.contain("b-ribbon-top-left");
  });
});
