// @ts-check


/* @ts-ignore *//* eslint-disable-next-line */
import $ from 'jquery'; // jshint ignore:line
import moment from "moment";
import Vue from "vue";

import { afterEach, describe, it } from "mocha";
import { expect } from "chai";
import { mount } from "@vue/test-utils";

import TempusDominusDatePicker from "../src/plugins/TempusDominusDatePicker.vue";

describe("TempusDominusDatePicker", function() {
  /** @type {Tests.Wrapper} */
  let wrapper;

  function defaultMount() {
    return mount(TempusDominusDatePicker, {
      propsData: { value: null }
    });
  }

  afterEach(() => {
    if (wrapper) {
      wrapper.destroy();
      /* @ts-ignore */
      wrapper = null;
    }
  });

  it("watch: updates value runtime", async () => {
    wrapper = defaultMount();

    // it renders input field
    expect(wrapper.element.tagName).to.equal("INPUT");
    const date = moment();
    wrapper.setProps({ value: date.unix() });
    await wrapper.vm.$nextTick();
    expect(wrapper.vm.dp.date().unix()).to.equal(date.unix());
  });

  it("watch: updates configs runtime", async () => {
    wrapper = defaultMount();

    await wrapper.vm.$nextTick();
    wrapper.setProps({ config: { useCurrent: false } });
    await wrapper.vm.$nextTick();
    expect(wrapper.vm.dp.options()).to.have.property("useCurrent", false);
  });

  it("opens datepicker when clicked", async () => {
    const div = document.createElement("div");
    div.id = "root";
    /* $FlowIgnore */
    document.body.appendChild(div);

    wrapper = mount(Vue.extend({
      components: { TempusDominusDatePicker },
      props: { timestamp: { type: Number, default: null } },
      template: `<div style="position: relative;">
        <TempusDominusDatePicker ref="in" v-model="timestamp"></TempusDominusDatePicker>
      </div>`
    }), { attachTo: document.body }); // Needs to attach in order to render datepicker widget

    await wrapper.vm.$nextTick();
    // $(wrapper.vm.$refs.in.$el).click(); // Somehow click is not working with karma only.
    // @ts-ignore
    wrapper.findComponent(TempusDominusDatePicker).vm.dp.show();
    await wrapper.vm.$nextTick();
    expect(wrapper.find(".bootstrap-datetimepicker-widget").exists()).to.be.true();
  });

  it("check the default format", async () => {
    wrapper = defaultMount();

    // 1593259787 -> 2020-06-27 14:09:47 +0200
    wrapper.setProps({ value: 1593259787, config: { format: "YYYY-MM-DD" } });
    await wrapper.vm.$nextTick();
    expect(wrapper.vm.$el.value).to.equal("2020-06-27");
  });

  it("destroy correctly", async () => {
    wrapper = defaultMount();
    await wrapper.vm.$nextTick();
    wrapper.destroy();
    expect(wrapper.vm.dp).to.be.null();
  });
});
