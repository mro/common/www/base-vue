// @ts-check

import { afterEach, describe, it } from "mocha";
import { expect } from "chai";
import { mount } from "@vue/test-utils";

import { BaseToggle as Toggle } from "../src";
import { stubs } from "./utils";

describe("Toggle", function() {
  /** @type {Tests.Wrapper} */
  var wrapper;

  afterEach(function() {
    if (wrapper) {
      wrapper.destroy();
      /* @ts-ignore */
      wrapper = null;
    }
  });

  it("can mount a Toggle", async function() {
    wrapper = mount(Toggle, {
      propsData: { value: false },
      stubs
    });

    expect(wrapper.find(".custom-switch")).to.exist();
    expect(wrapper.vm.editValue).to.equal(false);
    wrapper.setProps({ inEdit: true });

    await wrapper.vm.$nextTick();
    expect(wrapper.vm.editValue).to.equal(false);
    wrapper.find("input").trigger("click");
    expect(wrapper.vm.editValue).to.equal(true);
  });

  it("notifies about changes", async function() {
    wrapper = mount(Toggle, {
      propsData: { value: false },
      stubs
    });

    wrapper.setProps({ inEdit: true });
    await wrapper.vm.$nextTick();
    wrapper.find("input").trigger("click");
    await wrapper.vm.$nextTick();
    expect(wrapper.emitted("edit")).to.deep.equal([ [ false ], [ true ] ]);
  });
});
