// @ts-check

import { toString } from "lodash";

import { after, before, describe, it } from "mocha";
import { expect } from "chai";

import { createLocalVue, mount } from "@vue/test-utils";
import {
  default as BaseVue,
  BaseHasSlotMixin as HasSlotMixin } from "../src";

import { waitFor } from "./utils";

describe("utils", () => {
  /** @type {V.Constructor<any, any, Vue>} */
  var localVue;
  /** @type {Tests.Wrapper} */
  var wrapper;

  before(function() {
    localVue = createLocalVue();

    // BaseVue plugin sets the filters
    localVue.use(BaseVue);
  });

  after(function() {
    if (wrapper) {
      wrapper.destroy();
      // @ts-ignore
      wrapper = null;
    }
    // @ts-ignore
    localVue = null;
  });

  it("can use get", async () => {
    wrapper = mount({
      template: `<div>
        <div class="mounted" />
        <div class="test-value">{{$butils.get(value, 'a', 'default')}}</div>
        <div class="test-attr" :attr="$butils.get(value, 'a', 'default')"></div>
      </div>`,
      data: () => ({ value: undefined })
    }, { localVue });

    /**
     * @param {any} value
     * @param {any} result
     */
    async function testValue(value, result) {
      wrapper.vm.$data.value = value;
      await wrapper.vm.$nextTick();
      expect(wrapper.find(".test-value").element.innerHTML)
      .to.equal(toString(result), `test failed for "${JSON.stringify(value)}"`);
      expect(wrapper.find(".test-attr").element.getAttribute("attr"))
      .to.equal(toString(result), `test failed for "${JSON.stringify(value)}"`);
    }

    await wrapper.vm.$nextTick();
    expect(wrapper.find(".mounted").exists()).to.be.true();
    await testValue({ a: 123 }, 123);
    await testValue({ a: "test" }, "test");
    await testValue({ a: true }, true);
    await testValue({ a: "" }, "");
    await testValue(123, "default");
    await testValue(null, "default");
    await testValue(undefined, "default");
    await testValue({ b: 1 }, "default");
  });

  it("can use isEmpty", async () => {
    wrapper = mount({
      template: `<div>
        <div class="mounted" />
        <div v-if="$butils.isEmpty(value)" class="test-value"></div>
      </div>`,
      data: () => ({ value: undefined })
    }, { localVue });

    /**
     * @param {any} value
     * @param {boolean} result
     */
    async function testValue(value, result) {
      wrapper.vm.$data.value = value;
      await wrapper.vm.$nextTick();
      expect(wrapper.find(".test-value").exists())
      .to.equal(result, `test failed for "${toString(value)}"`);
    }

    await wrapper.vm.$nextTick();
    expect(wrapper.find(".mounted").exists()).to.be.true();
    await testValue([ 17 ], false);
    await testValue([], true);
    await testValue({ test: 42 }, false);
    await testValue({}, true);
    await testValue(null, true);
    await testValue(undefined, true);
    await testValue("test", false);
    await testValue("", true);
  });

  it("can use isNil", async () => {
    wrapper = mount({
      template: `<div>
        <div class="mounted" />
        <div v-if="$butils.isNil(value)" class="test-value"></div>
      </div>`,
      data: () => ({ value: undefined })
    }, { localVue });

    /**
     * @param {any} value
     * @param {any} result
     */
    async function testValue(value, result) {
      wrapper.vm.$data.value = value;
      await wrapper.vm.$nextTick();
      expect(wrapper.find(".test-value").exists())
      .to.equal(result, `test failed for "${toString(value)}"`);
    }

    await wrapper.vm.$nextTick();
    expect(wrapper.find(".mounted").exists()).to.be.true();
    await testValue(null, true);
    await testValue("", false);
    await testValue(undefined, true);
    await testValue(0, false);
  });

  it("can use hasSlot", () => {
    var SlotCompo = {
      template: `<div>
        <slot name="test" />
        <div v-if="hasSlot('test')" class='test-slot'></div>
        <div v-if="hasSlot('test')" class='no-test-slot'></div>
      </div>`,
      mixins: [ HasSlotMixin ]
    };

    wrapper = mount({
      components: { SlotCompo },
      template: "<SlotCompo><template v-slot:test>test</template></SlotCompo>"
    }, { localVue });

    return waitFor(() => wrapper.find(".test-slot").exists());
  });
});
