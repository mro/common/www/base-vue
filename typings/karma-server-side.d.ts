

declare module "karma-server-side" {
  namespace server {
    function run<T>(fun: (...args: any[]) => T): Promise<T>;
  }
  export = server;
}

declare function serverRequire(name: string): any

